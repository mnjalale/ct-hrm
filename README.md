# CT HRMS 

A Performance Appraisal System consisting of a web client and a REST API.

## Client
The client app has been created using Angular 6. 

## Server
The REST API was developed using Node.Js, Express, MySQL, Sequelize amongst other libraries.