import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { LayoutComponent } from './components/shared/layout/layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { P404Component } from './components/error/p404/p404.component';
import { P500Component } from './components/error/p500/p500.component';
import { EmployeesComponent } from './components/employees/employees/employees.component';
import { UpsertEmployeeComponent } from './components/employees/upsert-employee/upsert-employee.component';
import { CreateAppraisalComponent } from './components/performance/create-appraisal/create-appraisal.component';
import { ViewAppraisalsComponent } from './components/performance/view-appraisals/view-appraisals.component';
import { AppraisalDetailsComponent } from './components/performance/appraisal-details/appraisal-details.component';
import { PerformanceTargetDetailsComponent } from './components/performance/performance-target-details/performance-target-details.component';
import { PerformanceTrainingNeedDetailsComponent } from './components/performance/performance-training-need-details/performance-training-need-details.component';
import { PerformanceAdditionalAssignmentDetailsComponent } from './components/performance/performance-additional-assignment-details/performance-additional-assignment-details.component';
import { EmployeeComponent } from './components/employees/employee/employee.component';
import { ProfileComponent } from './components/employees/profile/profile.component';
import { SystemSettingsComponent } from './components/system-settings/system-settings/system-settings.component';
import { MinistryDetailsComponent } from './components/system-settings/ministry-details/ministry-details.component';
import { DepartmentDetailsComponent } from './components/system-settings/department-details/department-details.component';
import { SectionDetailsComponent } from './components/system-settings/section-details/section-details.component';
import { DutyStationDetailsComponent } from './components/system-settings/duty-station-details/duty-station-details.component';
import { AppraisalListComponent } from './components/performance/appraisal-list/appraisal-list.component';
import { EmployeeAppraisalsComponent } from './components/performance/employee-appraisals/employee-appraisals.component';
import { GenderBalanceReportComponent } from './components/reports/employees/gender-balance-report/gender-balance-report.component';


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    P404Component,
    P500Component,
    LayoutComponent,
    DashboardComponent,
    EmployeesComponent,
    UpsertEmployeeComponent,
    CreateAppraisalComponent,
    ViewAppraisalsComponent,
    AppraisalDetailsComponent,
    PerformanceTargetDetailsComponent,
    PerformanceTrainingNeedDetailsComponent,
    PerformanceAdditionalAssignmentDetailsComponent,
    EmployeeComponent,
    ProfileComponent,
    SystemSettingsComponent,
    MinistryDetailsComponent,
    DepartmentDetailsComponent,
    SectionDetailsComponent,
    DutyStationDetailsComponent,
    AppraisalListComponent,
    EmployeeAppraisalsComponent,
    GenderBalanceReportComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
