export class ServerResponseBase {
    public message: string;
    public statusCode: number;
    public errors: Array<string>;
}