import { Employee } from "../employees/Employee";
import { PerformanceAppraisalTarget } from "./PerformanceAppraisalTarget";
import { PerformanceAppraisalTrainingNeed } from "./PerformanceAppraisalTrainingNeed";
import { PerformanceAppraisalAdditionalAssignment } from "./PerformanceAppraisalAdditionalAssignment";

export class PerformanceAppraisal {
    constructor() {
        this.employee = new Employee();
        this.performanceAppraisalTargets = new Array<PerformanceAppraisalTarget>();
        this.performanceAppraisalTrainingNeeds = new Array<PerformanceAppraisalTrainingNeed>();
        this.performanceAppraisalAdditionalAssignments = new Array<PerformanceAppraisalAdditionalAssignment>();
    }

    public id: string;
    public startDate: Date;
    public midYearReviewDate: Date;
    public endDate: Date;
    public appraiseePeriodStartSignature: string;
    public appraiseePeriodStartSignatureDate: Date;
    public supervisorPeriodStartName: string;
    public supervisorPeriodStartSignature: string;
    public supervisorPeriodStartSignatureDate: Date;
    public supervisorMidYearReviewName: string;
    public supervisorMidYearReviewSignature: string;
    public supervisorMidYearReviewSignatureDate: Date;
    public trainingAppraiseeSignature: string;
    public trainingAppraiseeSignatureDate: Date;
    public trainingSupervisorName: string;
    public trainingSupervisorSignature: string;
    public trainingSupervisorSignatureDate: Date;
    public appraiseePeriodEndComments: string;
    public supervisorPeriodEndComments: string;
    public supervisorPeriodEndName: string;
    public supervisorPeriodEndSignature: string;
    public supervisorPeriodEndSignatureDate: Date;
    public recommendedRewardType: string;
    public recommendedOtherInterventions: string;
    public recommendedSanction: string;
    public recommendationMinuteNo: string;
    public recommendationMeetingDate: Date;
    public recommendationChairPersonName: string;
    public recommendationChairPersonSignature: string;
    public recommendationChairPersonSignatureDate: Date;
    public recommendationSecretaryName: string;
    public recommendationSecretarySignature: string;
    public recommendationSecretarySignatureDate: Date;
    public recommendationAuthorizedOfficerApproved: boolean;
    public recommendationAuthorizedOfficerName: string;
    public recommendationAuthorizedOfficerSignature: string;
    public recommendationAuthorizedOfficerSignatureDate: Date;
    public employeeId: string;
    public employee: Employee;
    public performanceAppraisalTargets: PerformanceAppraisalTarget[];
    public performanceAppraisalTrainingNeeds: PerformanceAppraisalTrainingNeed[];
    public performanceAppraisalAdditionalAssignments: PerformanceAppraisalAdditionalAssignment[];
}