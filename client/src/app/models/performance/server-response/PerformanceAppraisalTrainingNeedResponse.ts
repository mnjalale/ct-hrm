import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalTrainingNeed } from "../PerformanceAppraisalTrainingNeed";

export class PerformanceAppraisalTrainingNeedResponse extends ServerResponseBase {
    data: PerformanceAppraisalTrainingNeed;
}