import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalTarget } from "../PerformanceAppraisalTarget";

export class PerformanceAppraisalTargetsResponse extends ServerResponseBase {
    data: PerformanceAppraisalTarget[];
}