import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalTarget } from "../PerformanceAppraisalTarget";

export class PerformanceAppraisalTargetResponse extends ServerResponseBase {
    data: PerformanceAppraisalTarget;
}