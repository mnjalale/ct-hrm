import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalAdditionalAssignment } from "../PerformanceAppraisalAdditionalAssignment";

export class PerformanceAppraisalAdditionalAssignmentResponse extends ServerResponseBase {
    data: PerformanceAppraisalAdditionalAssignment;
}