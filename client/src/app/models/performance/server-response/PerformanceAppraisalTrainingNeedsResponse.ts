import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalTrainingNeed } from "../PerformanceAppraisalTrainingNeed";

export class PerformanceAppraisalTrainingNeedsResponse extends ServerResponseBase {
    data: PerformanceAppraisalTrainingNeed[];
}