import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisal } from "../PerformanceAppraisal";

export class PerformanceAppraisalResponse extends ServerResponseBase {
    data: PerformanceAppraisal;
}