import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisal } from "../PerformanceAppraisal";

export class PerformanceAppraisalsResponse extends ServerResponseBase {
    data: PerformanceAppraisal[];
}