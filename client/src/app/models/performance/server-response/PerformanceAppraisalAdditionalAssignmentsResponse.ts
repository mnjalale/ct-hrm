import { ServerResponseBase } from "../../base/ServerResponseBase";
import { PerformanceAppraisalAdditionalAssignment } from "../PerformanceAppraisalAdditionalAssignment";

export class PerformanceAppraisalAdditionalAssignmentsResponse extends ServerResponseBase {
    data: PerformanceAppraisalAdditionalAssignment[];
}