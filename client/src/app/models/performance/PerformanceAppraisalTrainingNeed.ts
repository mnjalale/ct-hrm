
export class PerformanceAppraisalTrainingNeed {
    public id: string;
    public description: string;
    public performanceAppraisalId: string;
}