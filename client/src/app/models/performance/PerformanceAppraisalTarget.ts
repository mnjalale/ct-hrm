
export class PerformanceAppraisalTarget {
    public id: string;
    public performanceTarget: string;
    public performanceIndicator: string;
    public midYearReviewRemarks: string;
    public updatedPerformanceTarget: string;
    public achievedResult: string;
    public performanceAppraisalScore: number;
    public performanceAppraisalId: string;
}