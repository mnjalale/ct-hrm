
export class PerformanceAppraisalAdditionalAssignment {
    public id: string;
    public description: string;
    public performanceAppraisalId: string;
}