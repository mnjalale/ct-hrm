import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Manager } from "../Manager";

export class ManagerResponse extends ServerResponseBase {
    public data: Array<Manager>;
}