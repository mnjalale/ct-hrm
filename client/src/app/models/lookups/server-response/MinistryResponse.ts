import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Ministry } from "../Ministry";

export class MinistryResponse extends ServerResponseBase {
    public data: Ministry;
}