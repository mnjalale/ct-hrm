import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Section } from "../Section";

export class SectionsResponse extends ServerResponseBase {
    public data: Array<Section>;
}