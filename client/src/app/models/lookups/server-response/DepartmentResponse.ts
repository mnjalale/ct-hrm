import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Department } from "../Department";

export class DepartmentResponse extends ServerResponseBase {
    public data: Department;
}