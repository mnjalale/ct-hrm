import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Section } from "../Section";

export class SectionResponse extends ServerResponseBase {
    public data: Section;
}