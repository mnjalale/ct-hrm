import { ServerResponseBase } from "../../base/ServerResponseBase";
import { DutyStation } from "../DutyStation";

export class DutyStationResponse extends ServerResponseBase {
    public data: DutyStation;
}