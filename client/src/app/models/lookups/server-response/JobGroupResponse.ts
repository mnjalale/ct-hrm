import { ServerResponseBase } from "../../base/ServerResponseBase";
import { JobGroup } from "../JobGroup";

export class JobGroupResponse extends ServerResponseBase {
    public data: Array<JobGroup>;
}