import { ServerResponseBase } from "../../base/ServerResponseBase";
import { DutyStation } from "../DutyStation";

export class DutyStationsResponse extends ServerResponseBase {
    public data: Array<DutyStation>;
}