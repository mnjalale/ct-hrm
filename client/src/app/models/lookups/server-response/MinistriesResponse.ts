import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Ministry } from "../Ministry";

export class MinistriesResponse extends ServerResponseBase {
    public data: Array<Ministry>;
}