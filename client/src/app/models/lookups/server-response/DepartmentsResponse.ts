import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Department } from "../Department";

export class DepartmentsResponse extends ServerResponseBase {
    public data: Array<Department>;
}