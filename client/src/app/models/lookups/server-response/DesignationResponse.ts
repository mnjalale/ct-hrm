import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Designation } from "../Designation";

export class DesignationResponse extends ServerResponseBase {
    public data: Array<Designation>;
}