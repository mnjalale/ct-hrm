import { ServerResponseBase } from "../../base/ServerResponseBase";
import { EmploymentType } from "../EmploymentType";

export class EmploymentTypeResponse extends ServerResponseBase {
    public data: Array<EmploymentType>;
}