export class Manager {
    public id: string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public fullName: string;
}