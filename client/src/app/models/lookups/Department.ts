import { LookupBase } from "../base/LookupBase";

export class Department extends LookupBase {
    public ministryId: string;
}