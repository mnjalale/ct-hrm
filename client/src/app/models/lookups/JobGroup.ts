export class JobGroup {
    public id: string;
    public code: string;
    public alternativeCode: string;
    public description: string;
    public alternativeDescription: string;
}