export class GenderBalanceReportModel {
    constructor() {
        this.male = 0;
        this.female = 0;
    }

    public male: number;
    public female: number;
}