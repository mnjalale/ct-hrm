import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Employee } from "../Employee";

export class EmployeeResponse extends ServerResponseBase {
    data: Employee;
}