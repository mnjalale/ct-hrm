import { ServerResponseBase } from "../../base/ServerResponseBase";
import { Employee } from "../Employee";

export class EmployeesResponse extends ServerResponseBase {
    data: Employee[];
}