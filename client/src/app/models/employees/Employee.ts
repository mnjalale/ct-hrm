import { JobGroup } from "../lookups/JobGroup";
import { Designation } from "../lookups/Designation";
import { EmploymentType } from "../lookups/EmploymentType";

export class Employee {
    constructor() {
        this.jobGroup = new JobGroup();
        this.designation = new Designation();
        this.employmentType = new EmploymentType();
    }

    public id: string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public gender: string;
    public email: string;
    public phoneNumber: string;
    public username: string;
    public jobGroupId: string;
    public jobGroup: JobGroup;
    public designationId: string;
    public ministryId: string;
    public departmentId: string;
    public sectionId: string;
    public dutyStationId: string;
    public designation: Designation;
    public managerId: string;
    public manager: Employee;
    public employmentTypeId: string;
    public employmentType: EmploymentType;
    public isManager: boolean;
    public isAdmin: boolean;
    public password: string;
    public hasActiveAppraisal: boolean;

    // Temporary fields to be replaced
    public personalNo: string;
    public ministryStateDepartment: string;
    public directorateDepartmentDivision: string;
    public sectionUnit: string;
    public dutyStation: string;
}