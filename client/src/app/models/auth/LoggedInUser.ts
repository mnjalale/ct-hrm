import { Roles } from "../../utils/enums/Roles";

export class LoggedInUser {
    public id: string;
    public firstName: string;
    public lastName: string;
    public roles: string[];

    public get isAdmin(): boolean {
        return !!this.roles.find(role => role == Roles.Administrator);
    }
    public get isManager(): boolean {
        return !!this.roles.find(role => role == Roles.Manager);
    }
    public get isEmployee(): boolean {
        return !!this.roles.find(role => role == Roles.Employee);
    }
}