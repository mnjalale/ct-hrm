import { LoggedInUser } from "../LoggedInUser";

export class LoginResponseData {
    public token: string;
    public refreshToken: string;
    public user: LoggedInUser;
}