import { ServerResponseBase } from "../../base/ServerResponseBase";
import { LoginResponseData } from "./LoginResponseData";

export class LoginResponse extends ServerResponseBase {
    public data: LoginResponseData;
}