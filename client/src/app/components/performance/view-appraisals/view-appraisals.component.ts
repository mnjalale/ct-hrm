import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceService } from '../../../services/performance.service';
import { PerformanceAppraisal } from '../../../models/performance/PerformanceAppraisal';

@Component({
  selector: 'app-view-appraisals',
  templateUrl: './view-appraisals.component.html',
  styleUrls: ['./view-appraisals.component.css']
})
export class ViewAppraisalsComponent extends BaseComponent implements OnInit {

  appraisals: PerformanceAppraisal[] = new Array<PerformanceAppraisal>();

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
    this.busy = this.performanceService
      .getEmployeeAppraisals(this.currentUser.id)
      .subscribe(
        appraisals => {
          this.appraisals = appraisals;
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }
}
