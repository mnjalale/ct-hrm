import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceAppraisal } from '../../../models/performance/PerformanceAppraisal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-appraisal-list',
  templateUrl: './appraisal-list.component.html',
  styleUrls: ['./appraisal-list.component.css']
})
export class AppraisalListComponent extends BaseComponent implements OnInit {

  private appraisalModalId = 'appraisalDetails';

  @Input()
  appraisals: PerformanceAppraisal[] = new Array<PerformanceAppraisal>();

  @Input()
  isBusy: Subscription;

  constructor() {
    super();
  }

  ngOnInit() {

  }

  viewAppraisal(appraisalId: string) {
    this.showModal(this.appraisalModalId, { appraisalId: appraisalId });
  }

}
