import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceService } from '../../../services/performance.service';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { PerformanceAppraisalAdditionalAssignment } from '../../../models/performance/PerformanceAppraisalAdditionalAssignment';

@Component({
  selector: 'app-performance-additional-assignment-details',
  templateUrl: './performance-additional-assignment-details.component.html',
  styleUrls: ['./performance-additional-assignment-details.component.css']
})
export class PerformanceAdditionalAssignmentDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'performanceAdditionalAssignments';
  private entryMode: EntryMode = EntryMode.Add;
  model: PerformanceAppraisalAdditionalAssignment = new PerformanceAppraisalAdditionalAssignment();

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const appraisalAdditionalAssignmentId = !!data && !!data.appraisalAdditionalAssignmentId ? data.appraisalAdditionalAssignmentId : null;

          if (!appraisalAdditionalAssignmentId) {
            this.entryMode = EntryMode.Add;
            this.model = new PerformanceAppraisalAdditionalAssignment();
            this.model.performanceAppraisalId = data.appraisalId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.performanceService.getAppraisalAdditionalAssignment(appraisalAdditionalAssignmentId)
            .subscribe(
              appraisalAdditionalAssignment => {
                this.model = appraisalAdditionalAssignment;
              },
              error => {
                this.showErrorMessage(error, 'Error getting appraisal additional assignment.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.addAppraisalAdditionalAssignment();
    } else {
      this.updateAppraisalAdditionalAssignment();
    }
  }

  // Helper functions
  private addAppraisalAdditionalAssignment() {
    this.busy = this.performanceService
      .addAppraisalAdditionalAssignment(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Additional Assignment Saved Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Adding Appraisal Additional Assignment');
        }
      );
  }

  private updateAppraisalAdditionalAssignment() {
    this.busy = this.performanceService
      .updateAppraisalAdditionalAssignment(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Additional Assignment Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Appraisal Additional Assignment');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }

}
