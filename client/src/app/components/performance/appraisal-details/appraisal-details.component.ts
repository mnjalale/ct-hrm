import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceAppraisal } from '../../../models/performance/PerformanceAppraisal';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { PerformanceService } from '../../../services/performance.service';
import { Employee } from '../../../models/employees/Employee';
import { DatepickerOptions } from 'ng2-datepicker';
import { UtilityFunctions } from '../../../utils/UtilityFunctions';

@Component({
  selector: 'app-appraisal-details',
  templateUrl: './appraisal-details.component.html',
  styleUrls: ['./appraisal-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppraisalDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'appraisalDetails';
  private appraisalTargetModal = 'performanceTargetDetails';
  private appraisalTrainingNeedModal = 'performanceTrainingNeeds';
  private appraisalAdditionalAssignmentModal = 'performanceAdditionalAssignments';
  datePickerOptions: DatepickerOptions = UtilityFunctions.getDatePickerOptions();
  model: PerformanceAppraisal = new PerformanceAppraisal();
  employee: Employee = new Employee();
  jobGroupHAndBelow: boolean = false;
  jobGroupJAndAbove: boolean = false;
  jobGroup: string = '';
  employmentType: string = '';
  designation: string = '';
  supervisorDesignation: string = '';
  employeeFullName: string = '';
  supervisorName: string = '';
  activeTab = [true, false, false, false, false, false];
  totalAppraisalScore: number = 0;
  meanAppraisalScore: number = 0;
  totalItems: number = 64;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;

  isEmployeeViewing: boolean = false;
  isManagerViewing: boolean = false;

  currentPager: number = 1;

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const appraisalId = !!data && !!data.appraisalId ? data.appraisalId : null;
          if (!appraisalId) {
            this.model = new PerformanceAppraisal();
            return;
          }

          this.busy = this.performanceService
            .getAppraisal(appraisalId)
            .subscribe(
              appraisal => {
                this.model = appraisal;
                this.employee = this.model.employee;
                this.isEmployeeViewing = this.employee && this.employee.id === this.currentUser.id;
                this.isManagerViewing = this.employee && this.employee.managerId == this.currentUser.id;
                this.designation = !!this.employee.designation ? this.employee.designation.name : '';
                this.employmentType = !!this.employee.employmentType ? this.employee.employmentType.name : '';
                this.jobGroup = !!this.employee.jobGroup ? this.employee.jobGroup.description : '';
                this.employeeFullName = this.getFullName(this.employee.firstName, this.employee.middleName, this.employee.lastName);
                this.jobGroupHAndBelow = this.employee && this.employee.jobGroup && 'ABCDEFGH'.includes(this.employee.jobGroup.alternativeCode);
                this.jobGroupJAndAbove = this.employee && this.employee.jobGroup && 'JKLMNPQRST'.includes(this.employee.jobGroup.alternativeCode);

                const manager = this.model.employee.manager;
                if (manager) {
                  this.supervisorName = this.getFullName(manager.firstName, manager.middleName, manager.lastName);
                  this.supervisorDesignation = !!manager.designation ? manager.designation.name : '';
                }

                this.computePerformanceTargetsTotal();
              },
              error => {
                this.showErrorMessage(error, 'Error getting appraisal.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );


    this.setModalCloseAction(this.appraisalTargetModal, () => this.refreshPerformanceAppraisalTargets());
    this.setModalCloseAction(this.appraisalTrainingNeedModal, () => this.refreshPerformanceAppraisalTrainingNeeds());
    this.setModalCloseAction(this.appraisalAdditionalAssignmentModal, () => this.refreshPerformanceAppraisalAdditionalAssignments());
  }

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    // console.log('Page changed to: ' + event.page);
    // console.log('Number items per page: ' + event.itemsPerPage);
    this.activeTab = this.activeTab.map((value, index) => {
      return index === event.page - 1 ? true : false
    });
  }

  // Performance Appraisal Target
  addPerformanceAppraisalTarget() {
    this.showModal(this.appraisalTargetModal, { appraisalId: this.model.id, employee: this.employee });
  }

  editPerformanceAppraisalTarget(appraisalTargetId: string, isMidYearReview: boolean) {
    this.showModal(this.appraisalTargetModal, { appraisalId: this.model.id, appraisalTargetId: appraisalTargetId, isMidYearReview: isMidYearReview, employee: this.employee });
  }

  deletePerformanceAppraisalTarget(appraisalTargetId: string) {
    this.busy = this.performanceService.deleteAppraisalTarget(appraisalTargetId)
      .subscribe(
        () => {
          this.showSuccessMessage('Performance target deleted successfully.');
          this.refreshPerformanceAppraisalTargets();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  // Staff Training and Development Needs
  addPerformanceAppraisalTrainingNeed() {
    this.showModal(this.appraisalTrainingNeedModal, { appraisalId: this.model.id });
  }

  editPerformanceAppraisalTrainingNeed(appraisalTrainingNeedId: string) {
    this.showModal(this.appraisalTrainingNeedModal, { appraisalId: this.model.id, appraisalTrainingNeedId: appraisalTrainingNeedId });
  }

  deletePerformanceAppraisalTrainingNeed(appraisalTrainingNeedId: string) {
    this.busy = this.performanceService.deleteAppraisalTrainingNeed(appraisalTrainingNeedId)
      .subscribe(
        () => {
          this.showSuccessMessage('Performance training need deleted successfully.');
          this.refreshPerformanceAppraisalTrainingNeeds();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  // Additional Assignments
  addPerformanceAppraisalAdditionalAssignment() {
    this.showModal(this.appraisalAdditionalAssignmentModal, { appraisalId: this.model.id });
  }

  editPerformanceAppraisalAdditionalAssignment(appraisalAdditionalAssignmentId: string) {
    this.showModal(this.appraisalAdditionalAssignmentModal, { appraisalId: this.model.id, appraisalAdditionalAssignmentId: appraisalAdditionalAssignmentId });
  }

  deletePerformanceAppraisalAdditionalAssignment(appraisalAdditionalAssignmentId: string) {
    this.busy = this.performanceService.deleteAppraisalAdditionalAssignment(appraisalAdditionalAssignmentId)
      .subscribe(
        () => {
          this.showSuccessMessage('Performance additional assignment deleted successfully.');
          this.refreshPerformanceAppraisalAdditionalAssignments();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  save() {
    this.performanceService
      .updateAppraisal(this.model)
      .subscribe(
        updatedAppraisal => {
          this.showSuccessMessage('Appraisal Saved Successfully');
          this.model = updatedAppraisal
        },
        error => {
          this.showErrorMessage(error, 'Error Saving Appraisal');
        }
      );
  }

  // Helper functions
  private getFullName(firstName: string, middleName: string, lastName: string): string {
    return `${firstName} ${!!middleName ? middleName + ' ' : ''}${lastName}`
  }

  private refreshPerformanceAppraisalTargets() {
    this.busy = this.performanceService
      .getAppraisalTargets(this.model.id)
      .subscribe(
        appraisalTargets => {
          this.model.performanceAppraisalTargets = appraisalTargets;
          this.computePerformanceTargetsTotal();
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing appraisal targets.');
        }
      )
  }

  private refreshPerformanceAppraisalTrainingNeeds() {
    this.busy = this.performanceService
      .getAppraisalTrainingNeeds(this.model.id)
      .subscribe(
        appraisalTargets => {
          this.model.performanceAppraisalTrainingNeeds = appraisalTargets;
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing appraisal training needs.');
        }
      )
  }

  private refreshPerformanceAppraisalAdditionalAssignments() {
    this.busy = this.performanceService
      .getAppraisalAdditionalAssignments(this.model.id)
      .subscribe(
        additionalAssignments => {
          this.model.performanceAppraisalAdditionalAssignments = additionalAssignments;
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing appraisal additional assignments.');
        }
      )
  }

  private computePerformanceTargetsTotal() {
    this.totalAppraisalScore = 0;
    this.model.performanceAppraisalTargets.forEach(performanceAppraisalTarget => {
      this.totalAppraisalScore += Number(performanceAppraisalTarget.performanceAppraisalScore);
    });

    this.meanAppraisalScore = UtilityFunctions.round(this.totalAppraisalScore / this.model.performanceAppraisalTargets.length, 2);
  }
}
