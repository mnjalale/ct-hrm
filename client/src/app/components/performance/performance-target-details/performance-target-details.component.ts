import { Component, OnInit, AfterViewInit } from '@angular/core';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { PerformanceAppraisalTarget } from '../../../models/performance/PerformanceAppraisalTarget';
import { PerformanceService } from '../../../services/performance.service';
import { BaseComponent } from '../../shared/base.component';
import { JobGroup } from '../../../models/lookups/JobGroup';
import { Employee } from '../../../models/employees/Employee';

@Component({
  selector: 'app-performance-target-details',
  templateUrl: './performance-target-details.component.html',
  styleUrls: ['./performance-target-details.component.css']
})
export class PerformanceTargetDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'performanceTargetDetails';
  private entryMode: EntryMode = EntryMode.Add;
  model: PerformanceAppraisalTarget = new PerformanceAppraisalTarget();
  employee: Employee = new Employee();
  jobGroupHAndBelow: boolean = false;
  jobGroupJAndAbove: boolean = false;
  isEmployeeViewing: boolean = false;
  isManagerViewing: boolean = false;
  isMidYearReview: boolean = false;

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const appraisalTargetId = !!data && !!data.appraisalTargetId ? data.appraisalTargetId : null;
          this.employee = !!data && !!data.employee ? data.employee : null;
          this.isMidYearReview = !!data && !!data.isMidYearReview ? data.isMidYearReview : false;
          this.isEmployeeViewing = this.employee && this.employee.id === this.currentUser.id;
          this.isManagerViewing = this.employee && this.employee.managerId == this.currentUser.id;
          this.jobGroupHAndBelow = this.employee && this.employee.jobGroup && 'ABCDEFGH'.includes(this.employee.jobGroup.alternativeCode);
          this.jobGroupJAndAbove = this.employee && this.employee.jobGroup && 'JKLMNPQRST'.includes(this.employee.jobGroup.alternativeCode);

          if (!appraisalTargetId) {
            this.entryMode = EntryMode.Add;
            this.model = new PerformanceAppraisalTarget();
            this.model.performanceAppraisalId = data.appraisalId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.performanceService.getAppraisalTarget(appraisalTargetId)
            .subscribe(
              appraisalTarget => {
                this.model = appraisalTarget;
              },
              error => {
                this.showErrorMessage(error, 'Error getting appraisal target.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.addAppraisalTarget();
    } else {
      this.updateAppraisalTarget();
    }
  }

  // Helper functions
  private addAppraisalTarget() {
    this.busy = this.performanceService
      .addAppraisalTarget(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Target Saved Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Adding Appraisal Target');
        }
      );
  }

  private updateAppraisalTarget() {
    this.busy = this.performanceService
      .updateAppraisalTarget(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Target Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Appraisal Target');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }
}
