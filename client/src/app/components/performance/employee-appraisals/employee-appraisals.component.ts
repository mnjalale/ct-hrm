import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceService } from '../../../services/performance.service';
import { PerformanceAppraisal } from '../../../models/performance/PerformanceAppraisal';

@Component({
  selector: 'app-employee-appraisals',
  templateUrl: './employee-appraisals.component.html',
  styleUrls: ['./employee-appraisals.component.css']
})
export class EmployeeAppraisalsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'employeeAppraisals';
  private createAppraisalModalId = 'createAppraisal';
  employeeId: string;
  employeeName: string;

  appraisals: PerformanceAppraisal[] = new Array<PerformanceAppraisal>();

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          this.employeeId = !!data && !!data.employeeId ? data.employeeId : null;
          this.employeeName = !!data && !!data.employeeName ? data.employeeName : null;
          this.loadAppraisals(this.employeeId);
        },
        error => {
          this.showErrorMessage(error);
        }
      );

    this.setModalCloseAction(this.createAppraisalModalId, () => this.loadAppraisals(this.employeeId));

  }

  createAppraisal() {
    this.showModal(this.createAppraisalModalId, { employeeId: this.employeeId });
  }

  close() {
    this.closeModal(this.modalId);
  }

  // Helpers
  private loadAppraisals(employeeId: string) {
    this.busy = this.performanceService
      .getEmployeeAppraisals(employeeId)
      .subscribe(
        appraisals => {
          this.appraisals = appraisals;
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

}
