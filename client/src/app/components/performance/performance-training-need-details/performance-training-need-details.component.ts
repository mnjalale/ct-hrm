import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceAppraisalTrainingNeed } from '../../../models/performance/PerformanceAppraisalTrainingNeed';
import { PerformanceService } from '../../../services/performance.service';
import { EntryMode } from '../../../utils/enums/EntryMode';

@Component({
  selector: 'app-performance-training-need-details',
  templateUrl: './performance-training-need-details.component.html',
  styleUrls: ['./performance-training-need-details.component.css']
})
export class PerformanceTrainingNeedDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'performanceTrainingNeeds';
  private entryMode: EntryMode = EntryMode.Add;
  model: PerformanceAppraisalTrainingNeed = new PerformanceAppraisalTrainingNeed();

  constructor(
    private performanceService: PerformanceService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const appraisalTrainingNeedId = !!data && !!data.appraisalTrainingNeedId ? data.appraisalTrainingNeedId : null;

          if (!appraisalTrainingNeedId) {
            this.entryMode = EntryMode.Add;
            this.model = new PerformanceAppraisalTrainingNeed();
            this.model.performanceAppraisalId = data.appraisalId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.performanceService.getAppraisalTrainingNeed(appraisalTrainingNeedId)
            .subscribe(
              appraisalTrainingNeed => {
                this.model = appraisalTrainingNeed;
              },
              error => {
                this.showErrorMessage(error, 'Error getting appraisal training need.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.addAppraisalTrainingNeed();
    } else {
      this.updateAppraisalTrainingNeed();
    }
  }

  // Helper functions
  private addAppraisalTrainingNeed() {
    this.busy = this.performanceService
      .addAppraisalTrainingNeed(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Training Need Saved Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Adding Appraisal Training Need');
        }
      );
  }

  private updateAppraisalTrainingNeed() {
    this.busy = this.performanceService
      .updateAppraisalTrainingNeed(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Training Need Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Appraisal Training Need');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }

}
