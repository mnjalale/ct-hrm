import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PerformanceAppraisal } from '../../../models/performance/PerformanceAppraisal';
import { BaseComponent } from '../../shared/base.component';
import { PerformanceService } from '../../../services/performance.service';

@Component({
  selector: 'app-create-appraisal',
  templateUrl: './create-appraisal.component.html',
  styleUrls: ['./create-appraisal.component.css']
})
export class CreateAppraisalComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'createAppraisal';
  model: PerformanceAppraisal = new PerformanceAppraisal();
  constructor(
    private performanceService: PerformanceService
  ) {
    super()
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const employeeId = !!data && !!data.employeeId ? data.employeeId : null;
          if (!employeeId) {
            this.showErrorMessage('Employee Id is required.');
            this.closeModal(this.modalId);
            return;
          }

          this.model = new PerformanceAppraisal();
          this.model.employeeId = employeeId;
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    this.busy = this.performanceService
      .createAppraisal(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Appraisal Saved Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Adding Appraisal');
        }
      );
  }

  // private methods
  private close() {
    this.closeModal(this.modalId);
  }

}
