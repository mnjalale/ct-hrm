import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginModel } from '../../../models/auth/LoginModel';
import { AuthenticationService } from '../../../services/authentication.service';
import { BaseComponent } from '../../shared/base.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {

  @ViewChild('username')
  usernameField: ElementRef;

  model: LoginModel = new LoginModel();
  returnUrl: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.authService.logout();
    this.usernameField.nativeElement.focus();

    this.route.queryParamMap.subscribe(queryParam => {
      this.returnUrl = queryParam.get('returnUrl') || '/dashboard';
    });


  }

  login() {
    this.busy = this.authService
      .login(this.model)
      .subscribe(
        data => {
          this.router.navigateByUrl(this.returnUrl);
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

}
