import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Employee } from '../../../models/employees/Employee';
import { JobGroup } from '../../../models/lookups/JobGroup';
import { EmploymentType } from '../../../models/lookups/EmploymentType';
import { Manager } from '../../../models/lookups/Manager';
import { Designation } from '../../../models/lookups/Designation';
import { BaseComponent } from '../../shared/base.component';
import { EmployeeService } from '../../../services/employee.service';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { EmployeeViewMode } from '../../../utils/enums/EmployeeViewMode';
import { LookupService } from '../../../services/lookup.service';
import { Subscription } from 'rxjs';
import { Ministry } from '../../../models/lookups/Ministry';
import { Department } from '../../../models/lookups/Department';
import { Section } from '../../../models/lookups/Section';
import { DutyStation } from '../../../models/lookups/DutyStation';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent extends BaseComponent implements OnInit {

  @Input()
  model: Employee = new Employee();
  @Input()
  managers: Array<Manager> = new Array<Manager>();
  @Input()
  entryMode: EntryMode = EntryMode.Add;
  @Input()
  employeeViewMode: EmployeeViewMode = EmployeeViewMode.Entry;
  @Input()
  allowCancel: boolean = true;
  @Input()
  isBusy: Subscription;

  designations: Array<Designation> = new Array<Designation>();
  ministries: Array<Ministry> = new Array<Ministry>();
  departments: Array<Department> = new Array<Department>();
  allDepartments: Array<Department> = new Array<Department>();
  sections: Array<Section> = new Array<Section>();
  allSections: Array<Section> = new Array<Section>();
  dutyStations: Array<DutyStation> = new Array<DutyStation>();
  allDutyStations: Array<DutyStation> = new Array<DutyStation>();
  employmentTypes: Array<EmploymentType> = new Array<EmploymentType>();
  jobGroups: Array<JobGroup> = new Array<JobGroup>();

  genders = [
    {
      code: 'M',
      description: 'Male'
    },
    {
      code: 'F',
      description: 'Female'
    }
  ];

  @Output()
  onCancel: EventEmitter<any> = new EventEmitter();

  constructor(
    private employeeService: EmployeeService,
    private lookupService: LookupService) {
    super();
  }

  ngOnInit() {
    this.loadLookupLists();
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.addEmployee();
    } else {
      this.updateEmployee();
    }
  }

  cancel() {
    this.onCancel.emit();
  }

  ministryChanged() {
    this.departments = this.allDepartments.filter(department => department.ministryId === this.model.ministryId);
    this.model.departmentId = undefined;
    this.sections = new Array<Section>();
    this.model.sectionId = undefined;
    this.dutyStations = new Array<DutyStation>();
    this.model.dutyStationId = undefined;
  }

  departmentChanged() {
    this.sections = this.allSections.filter(section => section.departmentId === this.model.departmentId);
    this.model.sectionId = undefined;
    this.dutyStations = new Array<DutyStation>();
    this.model.dutyStationId = undefined;
  }

  sectionChanged() {
    this.dutyStations = this.allDutyStations.filter(dutyStation => dutyStation.sectionId === this.model.sectionId);
    this.model.dutyStationId = undefined;
  }

  // Helper functions

  private addEmployee() {
    this.model.password = '12345678';
    this.isBusy = this.employeeService
      .addEmployee(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Employee Saved Successfully');
          this.cancel();
        },
        error => {
          this.showErrorMessage(error, 'Error Adding Employee');
        }
      );
  }

  private updateEmployee() {
    this.isBusy = this.employeeService
      .updateEmployee(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Employee Updated Successfully');
          this.cancel();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Employee');
        }
      )
  }

  private loadLookupLists() {
    this.lookupService.getDesignations().subscribe(
      designations => {
        this.designations = designations;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Designations');
      }
    );

    this.lookupService.getEmploymentTypes().subscribe(
      employmentTypes => {
        this.employmentTypes = employmentTypes;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Employment Types');
      }
    )

    this.lookupService.getJobGroups().subscribe(
      jobGroups => {
        this.jobGroups = jobGroups;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Job Groups');
      }
    );

    this.lookupService.getMinistries().subscribe(
      ministries => {
        this.ministries = ministries;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Ministries');
      }
    );

    this.lookupService.getDepartments().subscribe(
      departments => {
        this.allDepartments = departments;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Departments');
      }
    );

    this.lookupService.getSections().subscribe(
      sections => {
        this.allSections = sections;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Sections');
      }
    );

    this.lookupService.getDutyStations().subscribe(
      dutyStations => {
        this.allDutyStations = dutyStations;
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Duty Stations');
      }
    );

  }
}

