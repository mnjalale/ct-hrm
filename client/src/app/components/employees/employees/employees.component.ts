import { Component, OnInit, AfterViewInit } from '@angular/core';
import { EmployeeService } from '../../../services/employee.service';
import { Employee } from '../../..//models/employees/Employee';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from '../../shared/base.component';
import { EmployeesViewMode } from '../../../utils/enums/EmployeesViewMode';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent extends BaseComponent implements OnInit, AfterViewInit {

  private upsertEmployeeModalId = 'upsertEmployee';
  private employeeAppraisalsModalId = 'employeeAppraisals';

  employees: Employee[] = new Array<Employee>();
  totalEmployeeCount: number = 0;
  maleEmployeeCount: number = 0;
  maleEmployeePercentage: number = 0;
  femaleEmployeeCount: number = 0;
  femaleEmployeePercentage: number = 0;
  employeesViewMode: EmployeesViewMode;

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.route.data
      .subscribe(
        data => {
          if (!data.viewMode) {
            this.showErrorMessage('View Mode is not defined.');
            return;
          }

          this.employeesViewMode = <EmployeesViewMode>data.viewMode;
          this.loadEmployees(this.employeesViewMode);
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  ngAfterViewInit() {
    this.setModalCloseAction(this.upsertEmployeeModalId, () => this.loadEmployees(this.employeesViewMode));
  }

  addEmployee() {
    this.showModal(this.upsertEmployeeModalId);
  }

  editEmployee(employeeId: string) {
    this.showModal(this.upsertEmployeeModalId, { employeeId: employeeId });
  }

  viewAppraisals(employee: Employee) {
    this.showModal(this.employeeAppraisalsModalId, { employeeId: employee.id, employeeName: employee.firstName + ' ' + employee.lastName });
  }

  // Private methods
  private loadEmployees(employeesViewMode: EmployeesViewMode) {
    let callMethod: Observable<Employee[]>;

    if (employeesViewMode === EmployeesViewMode.AllEmployees) {
      callMethod = this.employeeService.getEmployees();
    } else if (employeesViewMode === EmployeesViewMode.ManagerTeamMembers) {
      callMethod = this.employeeService.getManagerTeamMembers();
    }

    this.busy = callMethod
      .subscribe(
        employees => {
          this.employees = employees;
          this.totalEmployeeCount = employees.length;
          this.maleEmployeeCount = employees.filter(employee => employee.gender === 'M').length;
          this.maleEmployeePercentage = Math.round((this.maleEmployeeCount / this.totalEmployeeCount) * 100);
          this.femaleEmployeeCount = employees.filter(employee => employee.gender === 'F').length;
          this.femaleEmployeePercentage = Math.round((this.femaleEmployeeCount / this.totalEmployeeCount) * 100);
        },
        error => {
          this.showErrorMessage(error, 'Error Getting Employees');
        }
      )
  }
}
