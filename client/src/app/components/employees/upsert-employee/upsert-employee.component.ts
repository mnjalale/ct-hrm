import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Employee } from '../../../models/employees/Employee';
import { Manager } from '../../../models/lookups/Manager';
import { BaseComponent } from '../../shared/base.component';
import { EmployeeService } from '../../../services/employee.service';
import { LookupService } from '../../../services/lookup.service';
import { EntryMode } from '../../../utils/enums/EntryMode';

@Component({
  selector: 'app-upsert-employee',
  templateUrl: './upsert-employee.component.html',
  styleUrls: ['./upsert-employee.component.css']
})
export class UpsertEmployeeComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'upsertEmployee';
  entryMode: EntryMode = EntryMode.Add;
  model: Employee = new Employee();
  managers: Array<Manager> = new Array<Manager>();

  constructor(
    private lookupService: LookupService,
    private employeeService: EmployeeService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const employeeId = !!data && !!data.employeeId ? data.employeeId : null;
          this.loadLookupLists(employeeId);
          if (!employeeId) {
            this.entryMode = EntryMode.Add;
            this.model = new Employee();
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.employeeService.getEmployee(employeeId)
            .subscribe(
              employee => {
                this.model = employee;
              },
              error => {
                this.showErrorMessage(error, 'Error getting employee.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  close() {
    this.closeModal(this.modalId);
  }

  // Helper functions
  private loadLookupLists(employeeId: string) {

    this.lookupService.getManagers().subscribe(
      managers => {
        if (employeeId) {
          this.managers = managers.filter(manager => {
            return manager.id != employeeId
          });
        } else {
          this.managers = managers;
        }
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Managers');
      }
    )
  }

}
