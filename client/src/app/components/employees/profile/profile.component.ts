import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Employee } from '../../../models/employees/Employee';
import { Manager } from '../../../models/lookups/Manager';
import { BaseComponent } from '../../shared/base.component';
import { EmployeeService } from '../../../services/employee.service';
import { LookupService } from '../../../services/lookup.service';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { EmployeeViewMode } from '../../../utils/enums/EmployeeViewMode';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends BaseComponent implements OnInit {

  entryMode: EntryMode = EntryMode.Edit;
  model: Employee = new Employee();
  managers: Array<Manager> = new Array<Manager>();
  employeeViewMode: EmployeeViewMode = EmployeeViewMode.Profile;

  constructor(
    private lookupService: LookupService,
    private employeeService: EmployeeService
  ) {
    super();
  }

  ngOnInit() {
    const employeeId = this.currentUser.id;
    this.loadLookupLists(employeeId);
    if (!employeeId) {
      this.showErrorMessage('Invalid parameters: Id');
    }

    this.busy = this.employeeService.getEmployee(employeeId)
      .subscribe(
        employee => {
          this.model = employee;
        },
        error => {
          this.showErrorMessage(error, 'Error getting employee.');
        }
      );
  }

  // Helper functions
  private loadLookupLists(employeeId: string) {

    this.lookupService.getManagers().subscribe(
      managers => {
        if (employeeId) {
          this.managers = managers.filter(manager => {
            return manager.id != employeeId
          });
        } else {
          this.managers = managers;
        }
      },
      error => {
        this.showErrorMessage(error, 'Error Getting Managers');
      }
    )
  }

}

