import { UtilityService } from '../../services/utility.service';
import { Injectable } from '@angular/core';
import { AppInjector } from '../../services/app-injector.service';
import { Observable, Subscription } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';
import { LoggedInUser } from '../../models/auth/LoggedInUser';

@Injectable()
export abstract class BaseComponent {
    private utilityService: UtilityService;
    protected authenticationService: AuthenticationService;
    busy: Subscription;

    constructor() {
        // Manually retrieve dependencies from the injector 
        // so that constructor has no dependencies that must be passed in from the child
        const injector = AppInjector.getInjector();
        this.utilityService = injector.get(UtilityService);
        this.authenticationService = injector.get(AuthenticationService);
    }

    protected showErrorMessage(message: string, title: string = '') {
        this.utilityService.showErrorMessage(message, title || 'Error');
    }

    protected showSuccessMessage(message: string, title: string = '') {
        this.utilityService.showSuccessMessage(message, title || 'Success');
    }

    protected showModal(modalId: string, data?: any) {
        this.utilityService.showModal(modalId, data);
    }

    protected getModalData(modalId: string): Observable<any> {
        return this.utilityService.getModalData(modalId);
    }

    protected closeModal(modalId: string) {
        this.utilityService.closeModal(modalId);
    }

    protected setModalCloseAction(modalId: string, action: any) {
        this.utilityService.setModalCloseAction(modalId, action);
    }

    get currentUser(): LoggedInUser {
        return this.authenticationService.getCurrentUser();
    }
}