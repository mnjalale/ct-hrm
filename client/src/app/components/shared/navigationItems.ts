import { Roles } from "../../utils/enums/Roles";
import { NavigationItem } from "./navigationItem";

const allRoles = [Roles.Administrator, Roles.Employee, Roles.Manager];

export const navItems = [
    new NavigationItem(
        'Home',
        '/dashboard',
        'fa fa-desktop',
        allRoles),
    // 'info',
    // 'NEW'),
    new NavigationItem(
        'Peformance',
        '/performance',
        'fa fa-check',
        allRoles,
        '',
        '',
        [
            new NavigationItem('My Appraisals', '/performance/myAppraisals', 'icon-sign-blank', allRoles),
        ]),
    new NavigationItem(
        'My Team',
        '/myTeam',
        'icon-people',
        [Roles.Manager],
        '',
        '',
        [
            new NavigationItem('Team Members', '/myTeam/members', 'icon-sign-blank', [Roles.Manager]),
            new NavigationItem('Performance Reviews', '/myTeam/performanceReviews', 'icon-sign-blank', [Roles.Manager])
        ]),
    new NavigationItem(
        'Admin',
        '/admin',
        'icon-puzzle',
        [Roles.Administrator],
        '',
        '',
        [
            new NavigationItem('Employees', '/admin/employees', 'icon-sign-blank', [Roles.Administrator]),
            new NavigationItem('System Settings', '/admin/settings', 'icon-sign-blank', [Roles.Administrator])
        ]
    ),
    new NavigationItem(
        'Reports',
        '/reports',
        'icon-notebook',
        allRoles,
        '',
        '',
        [
            new NavigationItem('Appraisal', '/reports/appraisal', 'icon-sign-blank', allRoles),
            new NavigationItem('Gender Balance', '/reports/genderBalanceReport', 'icon-sign-blank', allRoles)
        ]),
    new NavigationItem(
        'Account',
        '/account',
        'icon-grid',
        allRoles,
        '',
        '',
        [
            new NavigationItem('Profile', '/account/profile', 'icon-sign-blank', allRoles)
        ])
];
