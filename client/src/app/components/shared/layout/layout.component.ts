import { Component, OnDestroy, OnInit } from '@angular/core';
import { navItems } from '../navigationItems'
import { Router } from '@angular/router';
import { BaseComponent } from '../base.component';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent extends BaseComponent implements OnDestroy, OnInit {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor(
    private router: Router
  ) {
    super();
    this.changes = new MutationObserver(() => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit() {
    const currentUserRoles = this.currentUser.roles;
    this.navItems = navItems.filter(navItem => {
      let isAuthorized = false;
      for (let currentUserRoleIndex = 0; currentUserRoleIndex < currentUserRoles.length; currentUserRoleIndex++) {
        const userRole = currentUserRoles[currentUserRoleIndex];
        if (navItem.roles.find(role => role === userRole)) {
          isAuthorized = true;
          break;
        }
      }
      return isAuthorized;
    })
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
  }

  goToProfile() {
    this.router.navigateByUrl('/account/profile');
  }
}

