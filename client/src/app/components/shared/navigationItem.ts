export class NavigationItem {

    constructor(
        public name: string,
        public url: string,
        public icon: string,
        public roles: Array<string>,
        private badgeVariant?: string,
        private badgeText?: string,
        public children?: Array<NavigationItem>
    ) {
        if (badgeVariant || badgeText) {
            this.badge = {
                variant: this.badgeVariant,
                text: this.badgeText
            }
        }
    }

    public badge: any;
}