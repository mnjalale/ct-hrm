import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { Section } from '../../../models/lookups/Section';
import { Ministry } from '../../../models/lookups/Ministry';
import { Department } from '../../../models/lookups/Department';
import { LookupService } from '../../../services/lookup.service';

@Component({
  selector: 'app-section-details',
  templateUrl: './section-details.component.html',
  styleUrls: ['./section-details.component.css']
})
export class SectionDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'sectionDetails';
  private entryMode: EntryMode = EntryMode.Add;
  model: Section = new Section();
  ministries: Ministry[] = new Array<Ministry>();
  departments: Department[] = new Array<Department>();
  allDepartments: Department[] = new Array<Department>();
  selectedMinistryId: string;

  constructor(
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          this.selectedMinistryId = !!data && !!data.ministryId ? data.ministryId : null;
          const sectionId = !!data && !!data.sectionId ? data.sectionId : null;
          this.ministries = !!data && !!data.ministries ? data.ministries : new Array<Ministry>();
          this.allDepartments = !!data && !!data.allDepartments ? data.allDepartments : new Array<Department>();
          this.departments = this.allDepartments.filter(department => department.ministryId === this.selectedMinistryId);

          if (!sectionId) {
            this.entryMode = EntryMode.Add;
            this.model = new Section();
            this.model.departmentId = data.departmentId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.lookupService.getSection(sectionId)
            .subscribe(
              section => {
                this.model = section;
              },
              error => {
                this.showErrorMessage(error, 'Error getting section.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.createSection();
    } else {
      this.updateSection();
    }
  }

  ministryChanged() {
    this.departments = this.allDepartments.filter(department => department.ministryId === this.selectedMinistryId);
    this.model.departmentId = undefined;
  }

  // Helper functions
  private createSection() {
    this.busy = this.lookupService
      .createSection(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Section Saved Successfully.');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Creating Section.');
        }
      );
  }

  private updateSection() {
    this.busy = this.lookupService
      .updateSection(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Section Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Section');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }
}
