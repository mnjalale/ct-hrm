import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { DutyStation } from '../../../models/lookups/DutyStation';
import { Ministry } from '../../../models/lookups/Ministry';
import { Department } from '../../../models/lookups/Department';
import { Section } from '../../../models/lookups/Section';
import { LookupService } from '../../../services/lookup.service';

@Component({
  selector: 'app-duty-station-details',
  templateUrl: './duty-station-details.component.html',
  styleUrls: ['./duty-station-details.component.css']
})
export class DutyStationDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'dutyStationDetails';
  private entryMode: EntryMode = EntryMode.Add;
  model: DutyStation = new DutyStation();
  ministries: Ministry[] = new Array<Ministry>();
  departments: Department[] = new Array<Department>();
  allDepartments: Department[] = new Array<Department>();
  sections: Section[] = new Array<Section>();
  allSections: Section[] = new Array<Section>();
  selectedMinistryId: string;
  selectedDepartmentId: string;

  constructor(
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          this.selectedMinistryId = !!data && !!data.ministryId ? data.ministryId : null;
          this.selectedDepartmentId = !!data && !!data.departmentId ? data.departmentId : null;
          const dutyStationId = !!data && !!data.dutyStationId ? data.dutyStationId : null;
          this.ministries = !!data && !!data.ministries ? data.ministries : new Array<Ministry>();
          this.allDepartments = !!data && !!data.allDepartments ? data.allDepartments : new Array<Department>();
          this.departments = this.allDepartments.filter(department => department.ministryId === this.selectedMinistryId);
          this.allSections = !!data && !!data.allSections ? data.allSections : new Array<Section>();
          this.sections = this.allSections.filter(section => section.departmentId === this.selectedDepartmentId);

          if (!dutyStationId) {
            this.entryMode = EntryMode.Add;
            this.model = new DutyStation();
            this.model.sectionId = data.sectionId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.lookupService.getDutyStation(dutyStationId)
            .subscribe(
              dutyStation => {
                this.model = dutyStation;
              },
              error => {
                this.showErrorMessage(error, 'Error getting duty station.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.createDutyStation();
    } else {
      this.updateDutyStation();
    }
  }

  ministryChanged() {
    this.departments = this.allDepartments.filter(department => department.ministryId === this.selectedMinistryId);
    this.selectedDepartmentId = undefined;
    this.sections = new Array<Section>();
    this.model.sectionId = undefined;
  }

  departmentChanged() {
    this.sections = this.allSections.filter(section => section.departmentId === this.selectedDepartmentId);
    this.model.sectionId = undefined;
  }

  // Helper functions
  private createDutyStation() {
    this.busy = this.lookupService
      .createDutyStation(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Duty Station Saved Successfully.');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Creating Section.');
        }
      );
  }

  private updateDutyStation() {
    this.busy = this.lookupService
      .updateDutyStation(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Duty Station Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Section');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }
}
