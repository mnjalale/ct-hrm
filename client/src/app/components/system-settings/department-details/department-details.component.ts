import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { Ministry } from '../../../models/lookups/Ministry';
import { Department } from '../../../models/lookups/Department';
import { LookupService } from '../../../services/lookup.service';

@Component({
  selector: 'app-department-details',
  templateUrl: './department-details.component.html',
  styleUrls: ['./department-details.component.css']
})
export class DepartmentDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'departmentDetails';
  private entryMode: EntryMode = EntryMode.Add;
  model: Department = new Department();
  ministries: Ministry[] = new Array<Ministry>();

  constructor(
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const departmentId = !!data && !!data.departmentId ? data.departmentId : null;
          this.ministries = !!data && !!data.ministries ? data.ministries : new Array<Ministry>();

          if (!departmentId) {
            this.entryMode = EntryMode.Add;
            this.model = new Department();
            this.model.ministryId = data.ministryId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.lookupService.getDepartment(departmentId)
            .subscribe(
              department => {
                this.model = department;
              },
              error => {
                this.showErrorMessage(error, 'Error getting department.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.createDepartment();
    } else {
      this.updateDepartment();
    }
  }

  // Helper functions
  private createDepartment() {
    this.busy = this.lookupService
      .createDepartment(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Department Saved Successfully.');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Creating Department.');
        }
      );
  }

  private updateDepartment() {
    this.busy = this.lookupService
      .updateDepartment(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Department Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Department');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }

}
