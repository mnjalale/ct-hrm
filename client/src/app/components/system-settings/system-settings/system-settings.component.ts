import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Ministry } from '../../../models/lookups/Ministry';
import { Department } from '../../../models/lookups/Department';
import { Section } from '../../../models/lookups/Section';
import { DutyStation } from '../../../models/lookups/DutyStation';
import { BaseComponent } from '../../shared/base.component';
import { LookupService } from '../../../services/lookup.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-system-settings',
  templateUrl: './system-settings.component.html',
  styleUrls: ['./system-settings.component.css']
})
export class SystemSettingsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private ministryDetailsModal = 'ministryDetails';
  private departmentDetailsModal = 'departmentDetails';
  private sectionDetailsModal = 'sectionDetails';
  private dutyStationDetailsModal = 'dutyStationDetails';
  ministriesLoading: Subscription;
  departmentsLoading: Subscription;
  sectionsLoading: Subscription;
  dutyStationsLoading: Subscription;

  activeTab = [true, false];
  ministries: Ministry[] = new Array<Ministry>();

  allDepartments: Department[] = new Array<Department>();
  sections: Section[] = new Array<Section>();
  allSections: Section[] = new Array<Section>();
  dutyStations: DutyStation[] = new Array<DutyStation>();
  allDutyStations: DutyStation[] = new Array<DutyStation>();
  selectedMinistryId: string;

  // Ministies


  // Departments
  departments: Department[] = new Array<Department>();
  departmentSelectedMinistryId: string;

  // Sections
  sectionDepartments: Department[] = new Array<Department>();
  sectionSelectedMinistryId: string;
  sectionSelectedDepartmentId: string;

  // Duty Stations
  dutyStationDepartments: Department[] = new Array<Department>();
  dutyStationSections: Section[] = new Array<Section>();
  dutyStationSelectedMinistryId: string;
  dutyStationSelectedDepartmentId: string;
  dutyStationSelectedSectionId: string;

  constructor(
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
    this.loadRecords();
  }

  ngAfterViewInit(): void {
    this.setModalCloseAction(this.ministryDetailsModal, () => this.refreshMinistries());
    this.setModalCloseAction(this.departmentDetailsModal, () => this.refreshDepartments());
    this.setModalCloseAction(this.sectionDetailsModal, () => this.refreshSections());
    this.setModalCloseAction(this.dutyStationDetailsModal, () => this.refreshDutyStations());
  }

  // Ministries
  addMinistry() {
    this.showModal(this.ministryDetailsModal);
  }

  editMinistry(ministryId: string) {
    this.showModal(this.ministryDetailsModal, { ministryId: ministryId });
  }

  deleteMinistry(ministryId: string) {
    this.busy = this.lookupService.deleteMinistry(ministryId)
      .subscribe(
        () => {
          this.showSuccessMessage('Ministry deleted successfully.');
          this.refreshMinistries();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  // Departments
  addDepartment() {
    const modalData = {
      ministryId: this.departmentSelectedMinistryId,
      ministries: this.ministries
    };

    this.showModal(this.departmentDetailsModal, modalData);
  }

  editDepartment(departmentId: string) {
    const modalData = {
      departmentId: departmentId,
      ministryId: this.departmentSelectedMinistryId,
      ministries: this.ministries
    };

    this.showModal(this.departmentDetailsModal, modalData);
  }

  deleteDepartment(departmentId: string) {
    this.busy = this.lookupService.deleteDepartment(departmentId)
      .subscribe(
        () => {
          this.showSuccessMessage('Department deleted successfully.');
          this.refreshDepartments();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  departmentMinistryChanged() {
    this.departments = this.allDepartments.filter(department => department.ministryId === this.departmentSelectedMinistryId);
  }

  // Sections
  addSection() {
    const modalData = {
      ministryId: this.sectionSelectedMinistryId,
      departmentId: this.sectionSelectedDepartmentId,
      ministries: this.ministries,
      allDepartments: this.allDepartments
    };

    this.showModal(this.sectionDetailsModal, modalData);
  }

  editSection(sectionId: string) {
    const modalData = {
      sectionId: sectionId,
      ministryId: this.sectionSelectedMinistryId,
      departmentId: this.sectionSelectedDepartmentId,
      ministries: this.ministries,
      allDepartments: this.allDepartments
    };

    this.showModal(this.sectionDetailsModal, modalData);
  }

  deleteSection(sectionId: string) {
    this.busy = this.lookupService.deleteSection(sectionId)
      .subscribe(
        () => {
          this.showSuccessMessage('Section deleted successfully.');
          this.refreshSections();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  sectionMinistryChanged() {
    this.sectionDepartments = this.allDepartments.filter(department => department.ministryId === this.sectionSelectedMinistryId);
    this.sectionSelectedDepartmentId = undefined;
    this.sections = new Array<Section>();
  }

  sectionDepartmentChanged() {
    this.sections = this.allSections.filter(section => section.departmentId === this.sectionSelectedDepartmentId);
  }

  // Duty Stations
  addDutyStation() {
    const modalData = {
      ministryId: this.dutyStationSelectedMinistryId,
      departmentId: this.dutyStationSelectedDepartmentId,
      sectionId: this.dutyStationSelectedSectionId,
      ministries: this.ministries,
      allDepartments: this.allDepartments,
      allSections: this.allSections
    };

    this.showModal(this.dutyStationDetailsModal, modalData);
  }

  editDutyStation(dutyStationId: string) {
    const modalData = {
      dutyStationId: dutyStationId,
      ministryId: this.dutyStationSelectedMinistryId,
      departmentId: this.dutyStationSelectedDepartmentId,
      sectionId: this.dutyStationSelectedSectionId,
      ministries: this.ministries,
      allDepartments: this.allDepartments,
      allSections: this.allSections
    };

    this.showModal(this.dutyStationDetailsModal, modalData);
  }

  deleteDutyStation(dutyStationId: string) {
    this.busy = this.lookupService.deleteDutyStation(dutyStationId)
      .subscribe(
        () => {
          this.showSuccessMessage('Duty Station deleted successfully.');
          this.refreshDutyStations();
        },
        error => {
          this.showErrorMessage(error);
        }
      )
  }

  dutyStationMinistryChanged() {
    this.dutyStationDepartments = this.allDepartments.filter(department => department.ministryId === this.dutyStationSelectedMinistryId);
    this.dutyStationSelectedDepartmentId = undefined;
    this.dutyStationSections = new Array<Section>();
    this.dutyStationSelectedSectionId = undefined;
    this.dutyStations = new Array<DutyStation>();
  }

  dutyStationDepartmentChanged() {
    this.dutyStationSections = this.allSections.filter(section => section.departmentId === this.dutyStationSelectedDepartmentId);
    this.dutyStationSelectedSectionId = undefined;
    this.dutyStations = new Array<DutyStation>();
  }

  dutyStationSectionChanged() {
    this.dutyStations = this.allDutyStations.filter(dutyStation => dutyStation.sectionId === this.dutyStationSelectedSectionId);
  }

  // Helpers
  private loadRecords() {
    this.ministriesLoading = this.lookupService
      .getMinistries()
      .subscribe(
        ministries => {
          this.ministries = ministries;
        },
        error => {
          this.showErrorMessage(error, 'Error loading ministries.');
        }
      );

    this.departmentsLoading = this.lookupService
      .getDepartments()
      .subscribe(
        departments => {
          this.allDepartments = departments;
        },
        error => {
          this.showErrorMessage(error, 'Error loading departments.')
        }
      );

    this.sectionsLoading = this.lookupService
      .getSections()
      .subscribe(
        sections => {
          this.allSections = sections;
        },
        error => {
          this.showErrorMessage(error, 'Error loading sections.')
        }
      );

    this.dutyStationsLoading = this.lookupService
      .getDutyStations()
      .subscribe(
        dutyStations => {
          this.allDutyStations = dutyStations;
        },
        error => {
          this.showErrorMessage(error, 'Error loading duty stations.')
        }
      );
  }

  private refreshMinistries() {
    this.ministriesLoading = this.lookupService
      .getMinistries()
      .subscribe(
        ministries => {
          this.ministries = ministries;
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing ministries.');
        }
      )
  }

  private refreshDepartments() {
    this.departmentsLoading = this.lookupService
      .getDepartments()
      .subscribe(
        departments => {
          this.allDepartments = departments;
          this.departments = departments.filter(department => department.ministryId === this.departmentSelectedMinistryId);
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing departments.');
        }
      )
  }

  private refreshSections() {
    this.sectionsLoading = this.lookupService
      .getSections()
      .subscribe(
        sections => {
          this.allSections = sections;
          this.sections = sections.filter(section => section.departmentId === this.sectionSelectedDepartmentId);
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing departments.');
        }
      );
  }

  private refreshDutyStations() {
    this.dutyStationsLoading = this.lookupService
      .getDutyStations()
      .subscribe(
        dutyStations => {
          this.allDutyStations = dutyStations;
          this.dutyStations = dutyStations.filter(dutyStation => dutyStation.sectionId === this.dutyStationSelectedSectionId);
        },
        error => {
          this.showErrorMessage(error, 'Error refreshing departments.');
        }
      );
  }

}
