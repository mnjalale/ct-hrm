import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from '../../shared/base.component';
import { EntryMode } from '../../../utils/enums/EntryMode';
import { Ministry } from '../../../models/lookups/Ministry';
import { LookupService } from '../../../services/lookup.service';

@Component({
  selector: 'app-ministry-details',
  templateUrl: './ministry-details.component.html',
  styleUrls: ['./ministry-details.component.css']
})
export class MinistryDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  private modalId = 'ministryDetails';
  private entryMode: EntryMode = EntryMode.Add;
  model: Ministry = new Ministry();

  constructor(
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getModalData(this.modalId)
      .subscribe(
        data => {
          const ministryId = !!data && !!data.ministryId ? data.ministryId : null;

          if (!ministryId) {
            this.entryMode = EntryMode.Add;
            this.model = new Ministry();
            // this.model.ministryId = data.ministryId;
            return;
          }

          this.entryMode = EntryMode.Edit;
          this.busy = this.lookupService.getMinistry(ministryId)
            .subscribe(
              ministry => {
                this.model = ministry;
              },
              error => {
                this.showErrorMessage(error, 'Error getting ministry.');
              }
            );
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  save() {
    if (this.entryMode === EntryMode.Add) {
      this.createMinistry();
    } else {
      this.updateMinistry();
    }
  }

  // Helper functions
  private createMinistry() {
    this.busy = this.lookupService
      .createMinistry(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Ministry Saved Successfully.');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Creating Ministry.');
        }
      );
  }

  private updateMinistry() {
    this.busy = this.lookupService
      .updateMinistry(this.model)
      .subscribe(
        () => {
          this.showSuccessMessage('Ministry Updated Successfully');
          this.close();
        },
        error => {
          this.showErrorMessage(error, 'Error Updating Ministry');
        }
      )
  }

  private close() {
    this.closeModal(this.modalId);
  }

}
