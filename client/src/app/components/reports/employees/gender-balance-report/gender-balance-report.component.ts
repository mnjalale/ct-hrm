import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../components/shared/base.component';
import { ReportService } from '../../../../services/report.service';
import { LookupService } from '../../../../services/lookup.service';
import { Ministry } from '../../../../models/lookups/Ministry';
import { Department } from '../../../../models/lookups/Department';
import { Section } from '../../../../models/lookups/Section';
import { DutyStation } from '../../../../models/lookups/DutyStation';

@Component({
  selector: 'app-gender-balance-report',
  templateUrl: './gender-balance-report.component.html',
  styleUrls: ['./gender-balance-report.component.css']
})
export class GenderBalanceReportComponent extends BaseComponent implements OnInit {

  public selectedMinistryId: string;
  public selectedMinistryName: string;
  public ministries: Ministry[] = new Array<Ministry>();
  public selectedDepartmentId: string;
  public selectedDepartmentName: string;
  public departments: Department[] = new Array<Department>();
  public allDepartments: Department[] = new Array<Department>();
  public selectedSectionId: string;
  public selectedSectionName: string;
  public sections: Section[] = new Array<Section>();
  public allSections: Section[] = new Array<Section>();
  public selectedDutyStationId: string;
  public selectedDutyStationName: string;
  public dutyStations: DutyStation[] = new Array<DutyStation>();
  public allDutyStations: DutyStation[] = new Array<DutyStation>();
  public reportHeader: string = 'Gender Balance Report';

  public reportLabels: string[] = ['Female', 'Male'];
  public reportData: number[] = [0, 0];
  public pieChartType = 'pie';

  constructor(
    private reportService: ReportService,
    private lookupService: LookupService
  ) {
    super();
  }

  ngOnInit() {
    this.loadLookups();
    this.loadReport();
  }

  loadReport() {
    this.busy = this.reportService
      .genderBalanceReport(this.selectedMinistryId, this.selectedDepartmentId, this.selectedSectionId, this.selectedDutyStationId)
      .subscribe(
        data => {
          this.reportData = [data.female, data.male];

          this.reportHeader = 'Gender Balance Report';
          if (this.selectedMinistryId) {
            this.reportHeader += ': ' + this.selectedMinistryName;
          }
          if (this.selectedDepartmentId) {
            this.reportHeader += ' > ' + this.selectedDepartmentName;
          }
          if (this.selectedSectionId) {
            this.reportHeader += ' > ' + this.selectedSectionName;
          }
          if (this.selectedDutyStationId) {
            this.reportHeader += ' > ' + this.selectedDutyStationName;
          }
        },
        error => {
          this.showErrorMessage(error);
        }
      );
  }

  ministryChanged() {
    this.selectedMinistryName = !!this.selectedMinistryId ? this.ministries.find(ministry => ministry.id == this.selectedMinistryId).name : '';
    this.departments = this.allDepartments.filter(department => department.ministryId === this.selectedMinistryId);
    this.selectedDepartmentId = undefined;
    this.selectedDepartmentName = undefined;
    this.sections = new Array<Section>();
    this.selectedSectionId = undefined;
    this.selectedSectionName = undefined;
    this.dutyStations = new Array<DutyStation>();
    this.selectedDutyStationId = undefined;
    this.selectedDutyStationName = undefined;
  }

  departmentChanged() {
    this.selectedDepartmentName = !!this.selectedDepartmentId ? this.departments.find(department => department.id == this.selectedDepartmentId).name : '';
    this.sections = this.allSections.filter(section => section.departmentId === this.selectedDepartmentId);
    this.selectedSectionId = undefined;
    this.selectedSectionName = undefined;
    this.dutyStations = new Array<DutyStation>();
    this.selectedDutyStationId = undefined;
    this.selectedDutyStationName = undefined;
  }

  sectionChanged() {
    this.selectedSectionName = !!this.selectedSectionId ? this.sections.find(section => section.id == this.selectedSectionId).name : '';
    this.dutyStations = this.allDutyStations.filter(dutyStation => dutyStation.sectionId === this.selectedSectionId);
    this.selectedDutyStationId = undefined;
    this.selectedDutyStationName = undefined;
  }

  dutyStationChanged() {
    this.selectedDutyStationName = !!this.selectedDutyStationId ? this.dutyStations.find(dutyStation => dutyStation.id == this.selectedDutyStationId).name : '';
  }

  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  // Helpers
  private loadLookups() {
    this.lookupService
      .getMinistries()
      .subscribe(
        ministries => {
          this.ministries = ministries;
        },
        error => {
          this.showErrorMessage(error, 'Error loading ministries.');
        }
      );

    this.lookupService
      .getDepartments()
      .subscribe(
        departments => {
          this.allDepartments = departments;
        },
        error => {
          this.showErrorMessage(error, 'Error loading departments.')
        }
      );

    this.lookupService
      .getSections()
      .subscribe(
        sections => {
          this.allSections = sections;
        },
        error => {
          this.showErrorMessage(error, 'Error loading sections.')
        }
      );

    this.lookupService
      .getDutyStations()
      .subscribe(
        dutyStations => {
          this.allDutyStations = dutyStations;
        },
        error => {
          this.showErrorMessage(error, 'Error loading duty stations.')
        }
      );
  }

}
