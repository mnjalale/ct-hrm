import { HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";
import { environment } from '../../../environments/environment';
import { UtilityFunctions } from '../../utils/UtilityFunctions';

export class ServiceBase {

    protected baseUrl = environment.apiUrl;

    protected errorHandler = (error: HttpErrorResponse) => {
        const apiError = error.error;
        if (apiError) {
            const errors = apiError.errors;
            if (errors && errors.length > 0) {
                const errorMessage = this.getErrorMessage(errors);
                return throwError(errorMessage);
            } else {
                return throwError(apiError.message || error.message || 'Server Error');
            }
        } else {
            return throwError(error.message || "Server Error");
        }
    }

    protected getErrorMessage = (errorList: Array<string>): string => {
        const errorMessage = UtilityFunctions.getHTMLStringListFromArray(errorList);
        return errorMessage;
    }

}