import { Injectable } from '@angular/core';
import { ServiceBase } from './base/service-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeesResponse } from '../models/employees/server-response/EmployeesResponse';
import { map, catchError } from 'rxjs/operators';
import { GenderBalanceReportModel } from '../models/reports/GenderBalanceReportModel';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends ServiceBase {
  constructor(
    private http: HttpClient
  ) {
    super();
  }

  genderBalanceReport(ministryId: string = '', departmentId: string = '', sectionId: string = '', dutyStationId: string = ''): Observable<GenderBalanceReportModel> {
    const url = this.baseUrl + 'api/employees/get';
    return this.http
      .post<EmployeesResponse>(url, {
        ministryId: ministryId,
        departmentId: departmentId,
        sectionId: sectionId,
        dutyStationId: dutyStationId
      })
      .pipe(
        map(response => {
          if (response.statusCode === 200) {

            const employees = response.data;
            const reportData = new GenderBalanceReportModel();

            employees.forEach(employee => {
              if (employee.gender === 'M') {
                reportData.male += 1;
              } else if (employee.gender === 'F') {
                reportData.female += 1;
              }
            });

            return reportData;
          } else {
            const message = this.getErrorMessage(response.errors);
            throw new Error(message);
          }
        }),
        catchError(this.errorHandler)
      )
  }
}
