import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employees/Employee';
import { map, catchError } from 'rxjs/operators';
import { EmployeeResponse } from '../models/employees/server-response/EmployeeResponse';
import { ServiceBase } from './base/service-base.service';
import { Observable } from 'rxjs';
import { EmployeesResponse } from '../models/employees/server-response/EmployeesResponse';

@Injectable()
export class EmployeeService extends ServiceBase {
  constructor(
    private http: HttpClient
  ) {
    super();
  }

  addEmployee(employee: Employee): Observable<Employee> {
    const url = this.baseUrl + 'api/auth/createUser';
    return this.http
      .post<EmployeeResponse>(url, employee)
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            return response.data;
          } else {
            const errors = this.getErrorMessage(response.errors);
            throw new Error(errors);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    const url = this.baseUrl + 'api/auth/updateUser';
    return this.http
      .post<EmployeeResponse>(url, employee)
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            return response.data;
          } else {
            const errors = this.getErrorMessage(response.errors);
            throw new Error(errors);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getEmployee(employeeId: string): Observable<Employee> {
    const url = this.baseUrl + 'api/auth/getUser';
    return this.http
      .post<EmployeeResponse>(url, { id: employeeId })
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            return response.data;
          } else {
            const message = this.getErrorMessage(response.errors);
            throw new Error(message);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getEmployees(): Observable<Employee[]> {
    const url = this.baseUrl + 'api/employees/get';
    return this.http
      .post<EmployeesResponse>(url, {})
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            return response.data;
          } else {
            const message = this.getErrorMessage(response.errors);
            throw new Error(message);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getManagerTeamMembers(): Observable<Employee[]> {
    const url = this.baseUrl + 'api/employees/getManagerTeamMembers';
    return this.http
      .post<EmployeesResponse>(url, {})
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            return response.data;
          } else {
            const message = this.getErrorMessage(response.errors);
            throw new Error(message);
          }
        }),
        catchError(this.errorHandler)
      )
  }
}
