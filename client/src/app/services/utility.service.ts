import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProgressAnimationType } from 'ngx-toastr/toastr/toastr-config'
import { NgxSmartModalService, NgxSmartModalComponent } from 'ngx-smart-modal';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(
    private toastrService: ToastrService,
    private ngxSmartModalService: NgxSmartModalService
  ) { }

  // Messages display
  showSuccessMessage(message: string, title: string = '') {
    // this.toastrService.clear();
    this.toastrService.success(message, title, this.toastrConfiguration);
  }

  showErrorMessage(message: string, title: string = '') {
    this.toastrService.error(message, title, this.toastrConfiguration);
  }

  showWarningMessage(message: string, title: string = '') {
    this.toastrService.warning(message, title, this.toastrConfiguration);
  }

  showInfoMessage(message: string, title: string = '') {
    this.toastrService.info(message, title, this.toastrConfiguration);
  }

  private get toastrConfiguration() {
    const animationType: ProgressAnimationType = 'increasing'
    return {
      progressBar: true,
      progressAnimation: animationType,
      closeButton: true,
      enableHtml: true
    };
  }

  // Modal popups
  showModal(modalId: string, data?: any) {
    if (data) {
      this.ngxSmartModalService.setModalData(data, modalId, true);
    } else {
      this.ngxSmartModalService.resetModalData(modalId);
    }

    this.ngxSmartModalService.getModal(modalId).open();
    this.ngxSmartModalService.getModal(modalId).onClose.subscribe
  }

  getModalData(modalId: string): Observable<any> {
    return this.ngxSmartModalService
      .getModal(modalId)
      .onOpen
      .pipe(
        map((modal: NgxSmartModalComponent) => {
          const data = modal.getData();
          return !!data ? data : null;
        }),
        catchError(error => {
          throw error;
        })
      );
  }

  closeModal(modalId: string) {
    this.ngxSmartModalService.getModal(modalId).close();
  }

  setModalCloseAction(modalId: string, action: any) {
    this.ngxSmartModalService
      .getModal(modalId)
      .onClose
      .subscribe(
        (modal: NgxSmartModalComponent) => {
          action();
        }
      )
  }
}
