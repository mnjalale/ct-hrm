import { Injectable } from '@angular/core';
import { ServiceBase } from './base/service-base.service';
import { Observable } from 'rxjs';
import { PerformanceAppraisal } from '../models/performance/PerformanceAppraisal';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { PerformanceAppraisalResponse } from '../models/performance/server-response/PerformanceAppraisalResponse';
import { PerformanceAppraisalsResponse } from '../models/performance/server-response/PerformanceAppraisalsResponse';
import { PerformanceAppraisalTarget } from '../models/performance/PerformanceAppraisalTarget';
import { PerformanceAppraisalTargetResponse } from '../models/performance/server-response/PerformanceAppraisalTargetResponse';
import { PerformanceAppraisalTargetsResponse } from '../models/performance/server-response/PerformanceAppraisalTargetsResponse';
import { ServerResponseBase } from '../models/base/ServerResponseBase';
import { PerformanceAppraisalTrainingNeed } from '../models/performance/PerformanceAppraisalTrainingNeed';
import { PerformanceAppraisalTrainingNeedResponse } from '../models/performance/server-response/PerformanceAppraisalTrainingNeedResponse';
import { PerformanceAppraisalTrainingNeedsResponse } from '../models/performance/server-response/PerformanceAppraisalTrainingNeedsResponse';
import { PerformanceAppraisalAdditionalAssignment } from '../models/performance/PerformanceAppraisalAdditionalAssignment';
import { PerformanceAppraisalAdditionalAssignmentResponse } from '../models/performance/server-response/PerformanceAppraisalAdditionalAssignmentResponse';
import { PerformanceAppraisalAdditionalAssignmentsResponse } from '../models/performance/server-response/PerformanceAppraisalAdditionalAssignmentsResponse';

@Injectable()
export class PerformanceService extends ServiceBase {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  createAppraisal(model: PerformanceAppraisal): Observable<PerformanceAppraisal> {
    const url = this.baseUrl + 'api/performance/createAppraisal'
    return this.http.post<PerformanceAppraisalResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  updateAppraisal(model: PerformanceAppraisal): Observable<PerformanceAppraisal> {
    const url = this.baseUrl + 'api/performance/updateAppraisal'
    return this.http.post<PerformanceAppraisalResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }


  getEmployeeAppraisals(employeeId: string): Observable<PerformanceAppraisal[]> {
    const url = this.baseUrl + 'api/performance/getEmployeeAppraisals'
    return this.http.post<PerformanceAppraisalsResponse>(url, { employeeId: employeeId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisal(appraisalId: string): Observable<PerformanceAppraisal> {
    const url = this.baseUrl + 'api/performance/getAppraisal'
    return this.http.post<PerformanceAppraisalResponse>(url, { appraisalId: appraisalId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Appraisal Targets
  addAppraisalTarget(model: PerformanceAppraisalTarget): Observable<PerformanceAppraisalTarget> {
    const url = this.baseUrl + 'api/performance/createAppraisalTarget'
    return this.http.post<PerformanceAppraisalTargetResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  updateAppraisalTarget(model: PerformanceAppraisalTarget): Observable<PerformanceAppraisalTarget> {
    const url = this.baseUrl + 'api/performance/updateAppraisalTarget'
    return this.http.post<PerformanceAppraisalTargetResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  deleteAppraisalTarget(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/performance/deleteAppraisalTarget'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalTarget(appraisalTargetId: string): Observable<PerformanceAppraisalTarget> {
    const url = this.baseUrl + 'api/performance/getAppraisalTarget'
    return this.http.post<PerformanceAppraisalTargetResponse>(url, { id: appraisalTargetId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalTargets(appraisalId: string): Observable<PerformanceAppraisalTarget[]> {
    const url = this.baseUrl + 'api/performance/getAppraisalTargets'
    return this.http.post<PerformanceAppraisalTargetsResponse>(url, { appraisalId: appraisalId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Appraisal Training Needs
  addAppraisalTrainingNeed(model: PerformanceAppraisalTrainingNeed): Observable<PerformanceAppraisalTrainingNeed> {
    const url = this.baseUrl + 'api/performance/createAppraisalTrainingNeed'
    return this.http.post<PerformanceAppraisalTrainingNeedResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  updateAppraisalTrainingNeed(model: PerformanceAppraisalTrainingNeed): Observable<PerformanceAppraisalTrainingNeed> {
    const url = this.baseUrl + 'api/performance/updateAppraisalTrainingNeed'
    return this.http.post<PerformanceAppraisalTrainingNeedResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  deleteAppraisalTrainingNeed(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/performance/deleteAppraisalTrainingNeed'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalTrainingNeed(appraisalTrainingNeedId: string): Observable<PerformanceAppraisalTrainingNeed> {
    const url = this.baseUrl + 'api/performance/getAppraisalTrainingNeed'
    return this.http.post<PerformanceAppraisalTrainingNeedResponse>(url, { id: appraisalTrainingNeedId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalTrainingNeeds(appraisalId: string): Observable<PerformanceAppraisalTrainingNeed[]> {
    const url = this.baseUrl + 'api/performance/getAppraisalTrainingNeeds'
    return this.http.post<PerformanceAppraisalTrainingNeedsResponse>(url, { appraisalId: appraisalId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Appraisal Additional Assignments
  addAppraisalAdditionalAssignment(model: PerformanceAppraisalAdditionalAssignment): Observable<PerformanceAppraisalAdditionalAssignment> {
    const url = this.baseUrl + 'api/performance/createAppraisalAdditionalAssignment'
    return this.http.post<PerformanceAppraisalAdditionalAssignmentResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  updateAppraisalAdditionalAssignment(model: PerformanceAppraisalAdditionalAssignment): Observable<PerformanceAppraisalAdditionalAssignment> {
    const url = this.baseUrl + 'api/performance/updateAppraisalAdditionalAssignment'
    return this.http.post<PerformanceAppraisalAdditionalAssignmentResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  deleteAppraisalAdditionalAssignment(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/performance/deleteAppraisalAdditionalAssignment'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalAdditionalAssignment(appraisalAdditionalAssignmentId: string): Observable<PerformanceAppraisalAdditionalAssignment> {
    const url = this.baseUrl + 'api/performance/getAppraisalAdditionalAssignment'
    return this.http.post<PerformanceAppraisalAdditionalAssignmentResponse>(url, { id: appraisalAdditionalAssignmentId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getAppraisalAdditionalAssignments(appraisalId: string): Observable<PerformanceAppraisalAdditionalAssignment[]> {
    const url = this.baseUrl + 'api/performance/getAppraisalAdditionalAssignments'
    return this.http.post<PerformanceAppraisalAdditionalAssignmentsResponse>(url, { appraisalId: appraisalId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }
}
