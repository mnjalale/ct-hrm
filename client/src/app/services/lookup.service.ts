import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JobGroup } from '../models/lookups/JobGroup';
import { JobGroupResponse } from '../models/lookups/server-response/JobGroupResponse';
import { map, catchError } from 'rxjs/operators';
import { ServiceBase } from './base/service-base.service';
import { EmploymentType } from '../models/lookups/EmploymentType';
import { EmploymentTypeResponse } from '../models/lookups/server-response/EmploymentTypeResponse';
import { Designation } from '../models/lookups/Designation';
import { DesignationResponse } from '../models/lookups/server-response/DesignationResponse';
import { Manager } from '../models/lookups/Manager';
import { ManagerResponse } from '../models/lookups/server-response/ManagerResponse';
import { Ministry } from '../models/lookups/Ministry';
import { MinistriesResponse } from '../models/lookups/server-response/MinistriesResponse';
import { MinistryResponse } from '../models/lookups/server-response/MinistryResponse';
import { Department } from '../models/lookups/Department';
import { DepartmentsResponse } from '../models/lookups/server-response/DepartmentsResponse';
import { DepartmentResponse } from '../models/lookups/server-response/DepartmentResponse';
import { Section } from '../models/lookups/Section';
import { SectionResponse } from '../models/lookups/server-response/SectionResponse';
import { SectionsResponse } from '../models/lookups/server-response/SectionsResponse';
import { DutyStation } from '../models/lookups/DutyStation';
import { DutyStationsResponse } from '../models/lookups/server-response/DutyStationsResponse';
import { DutyStationResponse } from '../models/lookups/server-response/DutyStationResponse';
import { ServerResponseBase } from '../models/base/ServerResponseBase';

@Injectable({
  providedIn: 'root'
})
export class LookupService extends ServiceBase {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getDesignations(): Observable<Designation[]> {
    const url = this.baseUrl + 'api/designations/getAll';
    return this.http
      .post<DesignationResponse>(url, {})
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getEmploymentTypes(): Observable<EmploymentType[]> {
    const url = this.baseUrl + 'api/employmentTypes/getAll';
    return this.http
      .post<EmploymentTypeResponse>(url, {})
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getJobGroups(): Observable<JobGroup[]> {
    const url = this.baseUrl + 'api/lookup/jobGroups';
    return this.http
      .get<JobGroupResponse>(url)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getManagers(): Observable<Manager[]> {
    const url = this.baseUrl + 'api/lookup/managers';
    return this.http
      .get<ManagerResponse>(url)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  // Ministries
  getMinistries(): Observable<Ministry[]> {
    const url = this.baseUrl + 'api/ministries/getAll';
    return this.http
      .post<MinistriesResponse>(url, {})
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getMinistry(id: string): Observable<Ministry> {
    const url = this.baseUrl + 'api/ministries/get';
    return this.http
      .post<MinistryResponse>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  createMinistry(ministry: Ministry): Observable<Ministry> {
    const url = this.baseUrl + 'api/ministries/create';
    return this.http
      .post<MinistryResponse>(url, ministry)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateMinistry(ministry: Ministry): Observable<Ministry> {
    const url = this.baseUrl + 'api/ministries/update';
    return this.http
      .post<MinistryResponse>(url, ministry)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  deleteMinistry(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/ministries/delete'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Departments
  getDepartments(ministryId: string = ''): Observable<Department[]> {
    const url = this.baseUrl + 'api/departments/getAll';
    return this.http
      .post<DepartmentsResponse>(url, { ministryId: ministryId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getDepartment(id: string): Observable<Department> {
    const url = this.baseUrl + 'api/departments/get';
    return this.http
      .post<DepartmentResponse>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  createDepartment(department: Department): Observable<Department> {
    const url = this.baseUrl + 'api/departments/create';
    return this.http
      .post<DepartmentResponse>(url, department)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateDepartment(department: Department): Observable<Department> {
    const url = this.baseUrl + 'api/departments/update';
    return this.http
      .post<DepartmentResponse>(url, department)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  deleteDepartment(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/departments/delete'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Sections
  getSections(departmentId: string = ''): Observable<Section[]> {
    const url = this.baseUrl + 'api/sections/getAll';
    return this.http
      .post<SectionsResponse>(url, { departmentId: departmentId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getSection(id: string): Observable<Section> {
    const url = this.baseUrl + 'api/sections/get';
    return this.http
      .post<SectionResponse>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  createSection(section: Section): Observable<Section> {
    const url = this.baseUrl + 'api/sections/create';
    return this.http
      .post<SectionResponse>(url, section)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateSection(section: Section): Observable<Section> {
    const url = this.baseUrl + 'api/sections/update';
    return this.http
      .post<SectionResponse>(url, section)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  deleteSection(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/sections/delete'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Duty Stations
  getDutyStations(sectionId: string = ''): Observable<DutyStation[]> {
    const url = this.baseUrl + 'api/dutyStations/getAll';
    return this.http
      .post<DutyStationsResponse>(url, { sectionId: sectionId })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getDutyStation(id: string): Observable<DutyStation> {
    const url = this.baseUrl + 'api/dutyStations/get';
    return this.http
      .post<DutyStationResponse>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  createDutyStation(dutyStation: DutyStation): Observable<DutyStation> {
    const url = this.baseUrl + 'api/dutyStations/create';
    return this.http
      .post<DutyStationResponse>(url, dutyStation)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateDutyStation(dutyStation: DutyStation): Observable<DutyStation> {
    const url = this.baseUrl + 'api/dutyStations/update';
    return this.http
      .post<DutyStationResponse>(url, dutyStation)
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return response.data;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  deleteDutyStation(id: string): Observable<boolean> {
    const url = this.baseUrl + 'api/dutyStations/delete'
    return this.http.post<ServerResponseBase>(url, { id: id })
      .pipe(
        map(response => {
          if (response.statusCode == 200) {
            return true;
          } else {
            const errorMessage = this.getErrorMessage(response.errors)
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }
}
