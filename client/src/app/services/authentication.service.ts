import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { LoginModel } from '../models/auth/LoginModel';
import { catchError, map } from 'rxjs/operators'
import { ServiceBase } from './base/service-base.service';
import { Observable } from 'rxjs';
import { LoginResponse } from '../models/auth/server-response/LoginResponse';
import { LoggedInUser } from '../models/auth/LoggedInUser';

@Injectable()
export class AuthenticationService extends ServiceBase {


  constructor(private http: HttpClient) {
    super();
  }

  get isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return token && token.trim().length > 0;
  }

  login(model: LoginModel): Observable<LoginResponse> {
    const url = this.baseUrl + 'api/auth/login'
    return this.http
      .post<LoginResponse>(url, model)
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('refreshToken', response.data.refreshToken);
            localStorage.setItem('user', JSON.stringify(response.data.user));
            return response;
          } else {
            const errorMessage = this.getErrorMessage(response.errors);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('user');
  }

  getCurrentUser(): LoggedInUser {
    const user = JSON.parse(localStorage.getItem('user'));
    const currentUser = new LoggedInUser();
    currentUser.id = user['id'];
    currentUser.firstName = user['firstName'];
    currentUser.lastName = user['lastName'];
    currentUser.roles = user['roles'];
    return currentUser;
  }

  getToken(): string {
    const token = localStorage.getItem('token');
    return token;
  }

  getRefreshToken(): string {
    const token = localStorage.getItem('refreshToken');
    return token;
  }

  refreshToken(): Observable<string> {
    const url = this.baseUrl + 'api/auth/refreshToken'
    const refreshToken = this.getRefreshToken();
    return this.http
      .post<LoginResponse>(url, { refreshToken: refreshToken })
      .pipe(
        map(response => {
          if (response.statusCode === 200) {
            localStorage.setItem('token', response.data.token);
            return response.data.token;
          } else {
            const errorMessage = this.getErrorMessage(response.errors);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

}
