export class Roles {
    public static Administrator = "Administrator";
    public static Employee = "Employee";
    public static Manager = "Manager";
}