export enum EmployeesViewMode {
    AllEmployees = 'All Employees',
    ManagerTeamMembers = 'Manager Team Members'
}