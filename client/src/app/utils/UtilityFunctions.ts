import { DatepickerOptions } from "ng2-datepicker";

export class UtilityFunctions {
    public static getStringListFromArray = (list: Array<string>): string => {
        let stringList = "";
        list.forEach(item => {
            if (stringList.length == 0) {
                stringList = item;
            } else {
                stringList += `\n${item}`;
            }
        });

        return stringList;
    }

    public static getHTMLStringListFromArray = (list: Array<string>): string => {
        let stringList = "";
        list.forEach(item => {
            if (stringList.length == 0) {
                stringList = item;
            } else {
                stringList += `<br/>${item}`;
            }
        });

        return stringList;
    }

    public static getDatePickerOptions = (): DatepickerOptions => {
        const options: DatepickerOptions = {
            displayFormat: 'DD MMM YYYY',
            // addClass: 'form-control'
        };
        return options;
    }

    public static round = (num: number, scale: number): number => {
        // Ensure the scale is an int
        scale = Math.round(scale);
        if (scale < 0 || scale > 20) {
            throw Error('Invalid parameter. Decimal Places must be between 0 and 20');
        }

        const roundedNumber: number = +(Math.round(+(num + `e+${scale}`)) + `e-${scale}`);
        return roundedNumber;
    }

}