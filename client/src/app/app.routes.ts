import { Routes } from '@angular/router';

import { RegisterComponent } from './components/auth/register/register.component';
import { LoginComponent } from './components/auth/login/login.component';
import { P500Component } from './components/error/p500/p500.component';
import { P404Component } from './components/error/p404/p404.component';
import { LayoutComponent } from './components/shared/layout/layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { EmployeesComponent } from './components/employees/employees/employees.component';
import { EmployeesViewMode } from './utils/enums/EmployeesViewMode';
import { ViewAppraisalsComponent } from './components/performance/view-appraisals/view-appraisals.component';
import { ProfileComponent } from './components/employees/profile/profile.component';
import { EmployeeViewMode } from './utils/enums/EmployeeViewMode';
import { SystemSettingsComponent } from './components/system-settings/system-settings/system-settings.component';
import { GenderBalanceReportComponent } from './components/reports/employees/gender-balance-report/gender-balance-report.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: P404Component,
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: P500Component,
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: 'Login'
        }
    },
    {
        path: 'register',
        component: RegisterComponent,
        data: {
            title: 'Register'
        }
    },
    {
        path: '',
        component: LayoutComponent,
        data: {
            title: 'Home'
        },
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'myTeam/members',
                component: EmployeesComponent,
                data: {
                    title: 'My Team',
                    viewMode: EmployeesViewMode.ManagerTeamMembers
                }
            },
            {
                path: 'performance/myAppraisals',
                component: ViewAppraisalsComponent,
                data: {
                    title: 'My Appraisals'
                }
            },
            {
                path: 'admin/employees',
                component: EmployeesComponent,
                data: {
                    title: 'Employees',
                    viewMode: EmployeesViewMode.AllEmployees
                }
            },
            {
                path: 'admin/settings',
                component: SystemSettingsComponent,
                data: {
                    title: 'System Settings'
                }
            },
            {
                path: 'reports/genderBalanceReport',
                component: GenderBalanceReportComponent,
                data: {
                    title: 'Gender Balance Report'
                }
            },

            {
                path: 'account/profile',
                component: ProfileComponent,
                data: {
                    title: 'Profile'
                }
            }
        ]
    }
];
