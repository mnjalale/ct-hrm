import { NgModule, Injector } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgDatepickerModule } from 'ng2-datepicker';
import { NgBusyModule } from 'ng-busy';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ToastrModule } from 'ngx-toastr'

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { P404Component } from './components/error/p404/p404.component';
import { P500Component } from './components/error/p500/p500.component';
import { LayoutComponent } from './components/shared/layout/layout.component';

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { AuthenticationService } from './services/authentication.service';
import { JWTInterceptor } from './interceptors/jwt.interceptor';
import { EmployeesComponent } from './components/employees/employees/employees.component';
import { LookupService } from './services/lookup.service';
import { UtilityService } from './services/utility.service';
import { AppInjector } from './services/app-injector.service';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { UpsertEmployeeComponent } from './components/employees/upsert-employee/upsert-employee.component';
import { CreateAppraisalComponent } from './components/performance/create-appraisal/create-appraisal.component';
import { EmployeeService } from './services/employee.service';
import { PerformanceService } from './services/performance.service';
import { ViewAppraisalsComponent } from './components/performance/view-appraisals/view-appraisals.component';
import { AppraisalDetailsComponent } from './components/performance/appraisal-details/appraisal-details.component';
import { PerformanceTargetDetailsComponent } from './components/performance/performance-target-details/performance-target-details.component';
import { PerformanceTrainingNeedDetailsComponent } from './components/performance/performance-training-need-details/performance-training-need-details.component';
import { PerformanceAdditionalAssignmentDetailsComponent } from './components/performance/performance-additional-assignment-details/performance-additional-assignment-details.component';
import { EmployeeComponent } from './components/employees/employee/employee.component';
import { ProfileComponent } from './components/employees/profile/profile.component';
import { SystemSettingsComponent } from './components/system-settings/system-settings/system-settings.component';
import { MinistryDetailsComponent } from './components/system-settings/ministry-details/ministry-details.component';
import { DepartmentDetailsComponent } from './components/system-settings/department-details/department-details.component';
import { SectionDetailsComponent } from './components/system-settings/section-details/section-details.component';
import { DutyStationDetailsComponent } from './components/system-settings/duty-station-details/duty-station-details.component';
import { AppraisalListComponent } from './components/performance/appraisal-list/appraisal-list.component';
import { EmployeeAppraisalsComponent } from './components/performance/employee-appraisals/employee-appraisals.component';
import { GenderBalanceReportComponent } from './components/reports/employees/gender-balance-report/gender-balance-report.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    P404Component,
    P500Component,
    LayoutComponent,
    DashboardComponent,
    EmployeesComponent,
    UpsertEmployeeComponent,
    CreateAppraisalComponent,
    ViewAppraisalsComponent,
    AppraisalDetailsComponent,
    PerformanceTargetDetailsComponent,
    PerformanceTrainingNeedDetailsComponent,
    PerformanceAdditionalAssignmentDetailsComponent,
    EmployeeComponent,
    ProfileComponent,
    SystemSettingsComponent,
    MinistryDetailsComponent,
    DepartmentDetailsComponent,
    SectionDetailsComponent,
    DutyStationDetailsComponent,
    AppraisalListComponent,
    EmployeeAppraisalsComponent,
    GenderBalanceReportComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgDatepickerModule,
    NgBusyModule,
    ToastrModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    ChartsModule,
    FormsModule,
    ButtonsModule.forRoot(),
    NgSelectModule
  ],
  providers: [
    AuthenticationService,
    EmployeeService,
    LookupService,
    PerformanceService,
    UtilityService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    // Store module's injector in the AppInjector class
    AppInjector.setInjector(injector);
  }
}
