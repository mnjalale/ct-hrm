import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError, BehaviorSubject } from "rxjs";
import { catchError, switchMap, filter, take } from "rxjs/operators";
import { AuthenticationService } from "../services/authentication.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    private refreshTokenInProgress = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null); // Tracks the current token, is null if no token is currently available

    constructor(
        private authenticationService: AuthenticationService,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(request)
            .pipe
            (
                catchError(err => {

                    if (err.status !== 401 || request.url.includes('login')) {
                        throw err;
                    }

                    //********************************************
                    // Handle token refresh if this is a 401 error
                    //********************************************

                    // If there is no token/refresh token available or if this is a refresh token request failure, logout
                    if (!this.authenticationService.getToken()
                        || !this.authenticationService.getRefreshToken()
                        || request.url.includes('refreshToken')) {
                        this.authenticationService.logout();
                        location.reload(true);
                        throw err;
                    }

                    if (this.refreshTokenInProgress) {
                        // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
                        // – which means the new token is ready and we can retry the request again
                        return this.refreshTokenSubject
                            .pipe(
                                filter(result => result !== null),
                                take(1),
                                switchMap(() => {
                                    return next.handle(this.resendFailedRequest(request));
                                })
                            )

                    } else {
                        this.refreshTokenInProgress = true;

                        // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
                        this.refreshTokenSubject.next(null);

                        // Call auth.refreshAccessToken(this is an Observable that will be returned)
                        return this.authenticationService
                            .refreshToken()
                            .pipe(
                                switchMap(token => {
                                    this.refreshTokenInProgress = false;
                                    this.refreshTokenSubject.next(token);
                                    return next.handle(this.resendFailedRequest(request));
                                }),
                                catchError((error: HttpErrorResponse) => {
                                    this.refreshTokenInProgress = false;
                                    this.authenticationService.logout();
                                    location.reload(true);
                                    throw error;
                                })
                            )
                    }
                })
            )
    }

    private resendFailedRequest(request: HttpRequest<any>) {
        const token = this.authenticationService.getToken();

        if (!token) {
            return request;
        }

        return request.clone({
            setHeaders: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        });
    }

}