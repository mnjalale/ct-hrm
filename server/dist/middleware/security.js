"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _appConstants = _interopRequireDefault(require("../utils/appConstants"));

var _index = _interopRequireDefault(require("../models/index"));

var _roles = _interopRequireDefault(require("../utils/enums/roles"));

var User = _index.default['User'];
var Role = _index.default['Role'];

var _throwNotAuthenticatedError = Symbol('throwNotAuthenticatedError');

var _throwNotAuthorizedError = Symbol('throwNotAuthorizedError');

var _getDecodedToken = Symbol('getDecodedToken');

var Security =
/*#__PURE__*/
function () {
  function Security() {
    var _this = this;

    (0, _classCallCheck2.default)(this, Security);
    (0, _defineProperty2.default)(this, "checkAuthentication", function (req, res, next) {
      try {
        var decodedToken = _this[_getDecodedToken](req);

        req.userId = decodedToken.userId;
        next();
      } catch (error) {
        if (!error.statusCode) {
          error.statusCode = 401;
        }

        next(error);
      }
    });
    (0, _defineProperty2.default)(this, "checkAdminAuthorization",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var decodedToken, user;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                decodedToken = _this[_getDecodedToken](req);
                req.userId = decodedToken.userId;
                _context.next = 5;
                return User.findByPk(decodedToken.userId, {
                  include: Role
                });

              case 5:
                user = _context.sent;

                // Confirm that the user is admin
                if (!user.Roles.find(function (role) {
                  return role.name === _roles.default.Administrator;
                })) {
                  _this[_throwNotAuthorizedError]();
                }

                next();
                _context.next = 14;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);

                if (!_context.t0.statusCode) {
                  _context.t0.statusCode = 403;
                }

                next(_context.t0);

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 10]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
  }

  (0, _createClass2.default)(Security, [{
    key: _getDecodedToken,
    // Private methods
    value: function value(req) {
      try {
        var authHeader = req.get('Authorization');

        if (!authHeader) {
          this[_throwNotAuthenticatedError]();
        } // Expecting 'Bearer [token]'


        var token = authHeader.split(' ')[1];

        if (!token) {
          this[_throwNotAuthenticatedError]();
        }

        var decodedToken = _jsonwebtoken.default.verify(token, _appConstants.default.tokenSecret);

        if (!decodedToken) {
          this[_throwNotAuthenticatedError]();
        }

        return decodedToken;
      } catch (error) {
        this[_throwNotAuthenticatedError](error.message);
      }
    }
  }, {
    key: _throwNotAuthenticatedError,
    value: function value(errorDescription) {
      var error = new Error('Not authenticated.');
      error.statusCode = 401;

      if (errorDescription) {
        error.errors = [errorDescription];
      }

      throw error;
    }
  }, {
    key: _throwNotAuthorizedError,
    value: function value() {
      var error = new Error('Not authorized.');
      error.statusCode = 403;
      throw error;
    }
  }]);
  return Security;
}();

var _default = new Security();

exports.default = _default;