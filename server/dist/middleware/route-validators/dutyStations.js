"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateDutyStation = Symbol('_validateDutyStation');

var DutyStationsValidator =
/*#__PURE__*/
function () {
  function DutyStationsValidator() {
    (0, _classCallCheck2.default)(this, DutyStationsValidator);
  }

  (0, _createClass2.default)(DutyStationsValidator, [{
    key: "validateGetDutyStation",
    value: function validateGetDutyStation() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateDutyStation",
    value: function validateCreateDutyStation() {
      return (0, _toConsumableArray2.default)(this[_validateDutyStation]());
    }
  }, {
    key: "validateUpdateDutyStation",
    value: function validateUpdateDutyStation() {
      return (0, _toConsumableArray2.default)(this[_validateDutyStation]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteDutyStation",
    value: function validateDeleteDutyStation() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateDutyStation,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.'), (0, _check.body)('sectionId').not().isEmpty().withMessage('Section is required.')];
    }
  }]);
  return DutyStationsValidator;
}();

var _default = new DutyStationsValidator();

exports.default = _default;