"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateDesignation = Symbol('_validateDesignation');

var DesignationsValidator =
/*#__PURE__*/
function () {
  function DesignationsValidator() {
    (0, _classCallCheck2.default)(this, DesignationsValidator);
  }

  (0, _createClass2.default)(DesignationsValidator, [{
    key: "validateGetDesignation",
    value: function validateGetDesignation() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateDesignation",
    value: function validateCreateDesignation() {
      return (0, _toConsumableArray2.default)(this[_validateDesignation]());
    }
  }, {
    key: "validateUpdateDesignation",
    value: function validateUpdateDesignation() {
      return (0, _toConsumableArray2.default)(this[_validateDesignation]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteDesignation",
    value: function validateDeleteDesignation() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateDesignation,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.')];
    }
  }]);
  return DesignationsValidator;
}();

var _default = new DesignationsValidator();

exports.default = _default;