"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateEmploymentType = Symbol('_validateEmploymentType');

var EmploymentTypesValidator =
/*#__PURE__*/
function () {
  function EmploymentTypesValidator() {
    (0, _classCallCheck2.default)(this, EmploymentTypesValidator);
  }

  (0, _createClass2.default)(EmploymentTypesValidator, [{
    key: "validateGetEmploymentType",
    value: function validateGetEmploymentType() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateEmploymentType",
    value: function validateCreateEmploymentType() {
      return (0, _toConsumableArray2.default)(this[_validateEmploymentType]());
    }
  }, {
    key: "validateUpdateEmploymentType",
    value: function validateUpdateEmploymentType() {
      return (0, _toConsumableArray2.default)(this[_validateEmploymentType]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteEmploymentType",
    value: function validateDeleteEmploymentType() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateEmploymentType,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.')];
    }
  }]);
  return EmploymentTypesValidator;
}();

var _default = new EmploymentTypesValidator();

exports.default = _default;