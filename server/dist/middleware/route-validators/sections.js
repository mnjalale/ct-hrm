"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateSection = Symbol('_validateSection');

var SectionsValidator =
/*#__PURE__*/
function () {
  function SectionsValidator() {
    (0, _classCallCheck2.default)(this, SectionsValidator);
  }

  (0, _createClass2.default)(SectionsValidator, [{
    key: "validateGetSection",
    value: function validateGetSection() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateSection",
    value: function validateCreateSection() {
      return (0, _toConsumableArray2.default)(this[_validateSection]());
    }
  }, {
    key: "validateUpdateSection",
    value: function validateUpdateSection() {
      return (0, _toConsumableArray2.default)(this[_validateSection]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteSection",
    value: function validateDeleteSection() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateSection,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.'), (0, _check.body)('departmentId').not().isEmpty().withMessage('Department is required.')];
    }
  }]);
  return SectionsValidator;
}();

var _default = new SectionsValidator();

exports.default = _default;