"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateDepartment = Symbol('_validateDepartment');

var DepartmentsValidator =
/*#__PURE__*/
function () {
  function DepartmentsValidator() {
    (0, _classCallCheck2.default)(this, DepartmentsValidator);
  }

  (0, _createClass2.default)(DepartmentsValidator, [{
    key: "validateGetDepartment",
    value: function validateGetDepartment() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateDepartment",
    value: function validateCreateDepartment() {
      return (0, _toConsumableArray2.default)(this[_validateDepartment]());
    }
  }, {
    key: "validateUpdateDepartment",
    value: function validateUpdateDepartment() {
      return (0, _toConsumableArray2.default)(this[_validateDepartment]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteDepartment",
    value: function validateDeleteDepartment() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateDepartment,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.'), (0, _check.body)('ministryId').not().isEmpty().withMessage('Ministry is required.')];
    }
  }]);
  return DepartmentsValidator;
}();

var _default = new DepartmentsValidator();

exports.default = _default;