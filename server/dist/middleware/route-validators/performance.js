"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _moment = _interopRequireDefault(require("moment"));

var _getDate = Symbol('_getDate');

var _validateAppraisal = Symbol('_validateAppraisal');

var PerformanceValidator =
/*#__PURE__*/
function () {
  function PerformanceValidator() {
    (0, _classCallCheck2.default)(this, PerformanceValidator);
  }

  (0, _createClass2.default)(PerformanceValidator, [{
    key: "validateCreateAppraisal",
    value: function validateCreateAppraisal() {
      return (0, _toConsumableArray2.default)(this[_validateAppraisal]());
    }
  }, {
    key: "validateUpdateAppraisal",
    value: function validateUpdateAppraisal() {
      return (0, _toConsumableArray2.default)(this[_validateAppraisal]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required.')]);
    }
  }, {
    key: "validateGetEmployeeAppraisals",
    value: function validateGetEmployeeAppraisals() {
      return [(0, _check.body)('employeeId').not().isEmpty().withMessage('Employee Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisal",
    value: function validateGetPerformanceAppraisal() {
      return [(0, _check.body)('appraisalId').not().isEmpty().withMessage('Appraisal Id is required.')];
    } // Performance Appraisal Targets

  }, {
    key: "validateCreatePerformanceAppraisalTarget",
    value: function validateCreatePerformanceAppraisalTarget() {
      return [(0, _check.body)('performanceTarget').not().isEmpty().withMessage('Performance target is required.'), (0, _check.body)('performanceIndicator').not().isEmpty().withMessage('Performance indicator is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateUpdateAppraisalTarget",
    value: function validateUpdateAppraisalTarget() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.'), (0, _check.body)('performanceTarget').not().isEmpty().withMessage('Performance target is required.'), (0, _check.body)('performanceIndicator').not().isEmpty().withMessage('Performance indicator is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateDeleteAppraisalTarget",
    value: function validateDeleteAppraisalTarget() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalTarget",
    value: function validateGetPerformanceAppraisalTarget() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalTargets",
    value: function validateGetPerformanceAppraisalTargets() {
      return [(0, _check.body)('appraisalId').not().isEmpty().withMessage('Appraisal Id is required.')];
    } // Performance Appraisal Training Needs

  }, {
    key: "validateCreatePerformanceAppraisalTrainingNeed",
    value: function validateCreatePerformanceAppraisalTrainingNeed() {
      return [(0, _check.body)('description').not().isEmpty().withMessage('Description is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateUpdatePerformanceAppraisalTrainingNeed",
    value: function validateUpdatePerformanceAppraisalTrainingNeed() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.'), (0, _check.body)('description').not().isEmpty().withMessage('Description is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateDeletePerformanceAppraisalTrainingNeed",
    value: function validateDeletePerformanceAppraisalTrainingNeed() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalTrainingNeed",
    value: function validateGetPerformanceAppraisalTrainingNeed() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Appraisal Training Need Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalTrainingNeeds",
    value: function validateGetPerformanceAppraisalTrainingNeeds() {
      return [(0, _check.body)('appraisalId').not().isEmpty().withMessage('Appraisal Id is required.')];
    } // Performance Appraisal Additional Assignments

  }, {
    key: "validateCreatePerformanceAppraisalAdditionalAssignment",
    value: function validateCreatePerformanceAppraisalAdditionalAssignment() {
      return [(0, _check.body)('description').not().isEmpty().withMessage('Description is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateUpdatePerformanceAppraisalAdditionalAssignment",
    value: function validateUpdatePerformanceAppraisalAdditionalAssignment() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.'), (0, _check.body)('description').not().isEmpty().withMessage('Description is required.'), (0, _check.body)('performanceAppraisalId').not().isEmpty().withMessage('Performance Appraisal Id is required.')];
    }
  }, {
    key: "validateDeletePerformanceAppraisalAdditionalAssignment",
    value: function validateDeletePerformanceAppraisalAdditionalAssignment() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalAdditionalAssignment",
    value: function validateGetPerformanceAppraisalAdditionalAssignment() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Additional Assignment Id is required.')];
    }
  }, {
    key: "validateGetPerformanceAppraisalAdditionalAssignments",
    value: function validateGetPerformanceAppraisalAdditionalAssignments() {
      return [(0, _check.body)('appraisalId').not().isEmpty().withMessage('Appraisal Id is required.')];
    }
  }, {
    key: _getDate,
    value: function value(stringValue, format) {
      if (format) {
        var date = (0, _moment.default)(stringValue, _moment.default);
        return date;
      } else {
        var dateFormat = 'YYYY-MM-DD';

        var _date = (0, _moment.default)(stringValue, dateFormat);

        return _date;
      }
    }
  }, {
    key: _validateAppraisal,
    value: function value() {
      var _this = this;

      return [(0, _check.body)('employeeId').not().isEmpty().withMessage('Employee Id is required.'), (0, _check.body)('startDate').not().isEmpty().withMessage('Start date is required.').custom(function (value, _ref) {
        var req = _ref.req;

        if (!value || value.trim().length === 0) {
          return true;
        }

        var startDate = _this[_getDate](value);

        if (!startDate.isValid()) {
          throw new Error("Invalid start date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
        }

        return true;
      }), (0, _check.body)('midYearReviewDate').not().isEmpty().withMessage('Mid year review date is required.').custom(function (value, _ref2) {
        var req = _ref2.req;

        if (!value || value.trim().length === 0) {
          return true;
        }

        var midYearReviewDate = _this[_getDate](value);

        if (!midYearReviewDate.isValid()) {
          throw new Error("Invalid mid year review date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
        }

        var startDate = _this[_getDate](req.body.startDate);

        if (startDate.isValid() && !startDate.isBefore(midYearReviewDate)) {
          throw new Error('Mid year review date must be after the start date.');
        }

        return true;
      }), (0, _check.body)('endDate').not().isEmpty().withMessage('End date is required.').custom(function (value, _ref3) {
        var req = _ref3.req;

        if (!value || value.trim().length === 0) {
          return true;
        }

        var endDate = _this[_getDate](value);

        if (!endDate.isValid()) {
          throw new Error("Invalid end date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
        }

        var midYearReviewDate = _this[_getDate](req.body.midYearReviewDate);

        if (midYearReviewDate.isValid() && !endDate.isAfter(midYearReviewDate)) {
          throw new Error('End date must be after the mid year review date.');
        }

        return true;
      })];
    }
  }]);
  return PerformanceValidator;
}();

var _default = new PerformanceValidator();

exports.default = _default;