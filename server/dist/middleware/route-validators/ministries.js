"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _validateMinistry = Symbol('_validateMinistry');

var MinistriesValidator =
/*#__PURE__*/
function () {
  function MinistriesValidator() {
    (0, _classCallCheck2.default)(this, MinistriesValidator);
  }

  (0, _createClass2.default)(MinistriesValidator, [{
    key: "validateGetMinistry",
    value: function validateGetMinistry() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: "validateCreateMinistry",
    value: function validateCreateMinistry() {
      return (0, _toConsumableArray2.default)(this[_validateMinistry]());
    }
  }, {
    key: "validateUpdateMinistry",
    value: function validateUpdateMinistry() {
      return (0, _toConsumableArray2.default)(this[_validateMinistry]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required')]);
    }
  }, {
    key: "validateDeleteMinistry",
    value: function validateDeleteMinistry() {
      return [(0, _check.body)('id').not().isEmpty().withMessage('Id is required')];
    }
  }, {
    key: _validateMinistry,
    value: function value() {
      return [(0, _check.body)('name').not().isEmpty().withMessage('Name is required.')];
    }
  }]);
  return MinistriesValidator;
}();

var _default = new MinistriesValidator();

exports.default = _default;