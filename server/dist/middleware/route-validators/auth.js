"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var _index = _interopRequireDefault(require("../../models/index"));

var _validateUser = Symbol('validateUser');

var User = _index.default['User'];
var Op = _index.default.Sequelize.Op;

var AuthValidator =
/*#__PURE__*/
function () {
  function AuthValidator() {
    (0, _classCallCheck2.default)(this, AuthValidator);
  }

  (0, _createClass2.default)(AuthValidator, [{
    key: "validateLogin",
    value: function validateLogin() {
      return [(0, _check.body)('username').not().isEmpty().withMessage('Username is required'), (0, _check.body)('password').not().isEmpty().withMessage('Password is required.')];
    }
  }, {
    key: "validateRefreshToken",
    value: function validateRefreshToken() {
      return [(0, _check.body)('refreshToken').not().isEmpty().withMessage('Refresh token is required.')];
    }
  }, {
    key: "validateCreateUser",
    value: function validateCreateUser() {
      return (0, _toConsumableArray2.default)(this[_validateUser]()).concat([(0, _check.body)('username').not().isEmpty().withMessage('Username is required').custom(
      /*#__PURE__*/
      function () {
        var _ref2 = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee(value, _ref) {
          var req, user, error;
          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  req = _ref.req;
                  _context.next = 3;
                  return User.findOne({
                    where: {
                      username: value
                    }
                  });

                case 3:
                  user = _context.sent;

                  if (!user) {
                    _context.next = 9;
                    break;
                  }

                  error = new Error('Username is already in use.');
                  throw error;

                case 9:
                  return _context.abrupt("return", true);

                case 10:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        return function (_x, _x2) {
          return _ref2.apply(this, arguments);
        };
      }()), (0, _check.body)('password').not().isEmpty().withMessage('Password is required.'), (0, _check.body)('email').optional({
        nullable: true
      }).isEmail().withMessage('Enter valid email address.').normalizeEmail().custom(
      /*#__PURE__*/
      function () {
        var _ref4 = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee2(value, _ref3) {
          var req, user, error;
          return _regenerator.default.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  req = _ref3.req;

                  if (value) {
                    _context2.next = 3;
                    break;
                  }

                  return _context2.abrupt("return", true);

                case 3:
                  _context2.next = 5;
                  return User.findOne({
                    where: {
                      email: value
                    }
                  });

                case 5:
                  user = _context2.sent;

                  if (!user) {
                    _context2.next = 11;
                    break;
                  }

                  error = new Error('Email address is already in use.');
                  throw error;

                case 11:
                  return _context2.abrupt("return", true);

                case 12:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        return function (_x3, _x4) {
          return _ref4.apply(this, arguments);
        };
      }())]);
    }
  }, {
    key: "validateUpdateUser",
    value: function validateUpdateUser() {
      return (0, _toConsumableArray2.default)(this[_validateUser]()).concat([(0, _check.body)('id').not().isEmpty().withMessage('Id is required'), (0, _check.body)('username').not().isEmpty().withMessage('Username is required').custom(
      /*#__PURE__*/
      function () {
        var _ref6 = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee3(value, _ref5) {
          var req, user, error;
          return _regenerator.default.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  req = _ref5.req;
                  _context3.next = 3;
                  return User.findOne({
                    where: {
                      username: value,
                      id: (0, _defineProperty2.default)({}, Op.ne, req.body.id)
                    }
                  });

                case 3:
                  user = _context3.sent;

                  if (!user) {
                    _context3.next = 9;
                    break;
                  }

                  error = new Error('Username is already in use.');
                  throw error;

                case 9:
                  return _context3.abrupt("return", true);

                case 10:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this);
        }));

        return function (_x5, _x6) {
          return _ref6.apply(this, arguments);
        };
      }()), (0, _check.body)('email').optional({
        nullable: true
      }).isEmail().withMessage('Enter valid email address.').normalizeEmail().custom(
      /*#__PURE__*/
      function () {
        var _ref8 = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee4(value, _ref7) {
          var req, user, error;
          return _regenerator.default.wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  req = _ref7.req;

                  if (value) {
                    _context4.next = 3;
                    break;
                  }

                  return _context4.abrupt("return", true);

                case 3:
                  _context4.next = 5;
                  return User.findOne({
                    where: {
                      email: value,
                      id: (0, _defineProperty2.default)({}, Op.ne, req.body.id)
                    }
                  });

                case 5:
                  user = _context4.sent;

                  if (!user) {
                    _context4.next = 11;
                    break;
                  }

                  error = new Error('Email address is already in use.');
                  throw error;

                case 11:
                  return _context4.abrupt("return", true);

                case 12:
                case "end":
                  return _context4.stop();
              }
            }
          }, _callee4, this);
        }));

        return function (_x7, _x8) {
          return _ref8.apply(this, arguments);
        };
      }())]);
    }
  }, {
    key: "validateGetUser",
    value: function validateGetUser() {
      return [(0, _check.body)('id').not().isEmpty().withMessage("Id is required")];
    }
  }, {
    key: _validateUser,
    value: function value() {
      return [(0, _check.body)('firstName').not().isEmpty().withMessage('First name is required.'), (0, _check.body)('lastName').not().isEmpty().withMessage('Last name is required.'), (0, _check.body)('phoneNumber').not().isEmpty().withMessage('Phone number is required.'), (0, _check.body)('gender').not().isEmpty().withMessage('Gender is required.').custom(function (value, _ref9) {
        var req = _ref9.req;

        if (!value) {
          return true;
        }

        var gender = value.toUpperCase();

        if (gender !== 'M' && gender !== 'F') {
          var error = new Error("Gender value must be either 'M' or 'F'");
          throw error;
        } else {
          return true;
        }
      }), (0, _check.body)('isManager').optional().isBoolean().withMessage("'Is Manager' must be a boolean value."), (0, _check.body)('isAdmin').optional().isBoolean().withMessage("'Is Admin' must be a boolean value.")];
    }
  }]);
  return AuthValidator;
}();

var _default = new AuthValidator();

exports.default = _default;