"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var AppConstants = function AppConstants() {
  (0, _classCallCheck2.default)(this, AppConstants);
};

(0, _defineProperty2.default)(AppConstants, "passwordHashSalt", 12);
(0, _defineProperty2.default)(AppConstants, "tokenSecret", 'apptokensecret');
(0, _defineProperty2.default)(AppConstants, "tokenLife", 10 * 60);
(0, _defineProperty2.default)(AppConstants, "refreshTokenSecret", 'apprefreshtokensecret');
(0, _defineProperty2.default)(AppConstants, "refreshTokenLife", 24 * 60 * 60);
var _default = AppConstants;
exports.default = _default;