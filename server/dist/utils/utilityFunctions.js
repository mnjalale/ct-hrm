"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var UtilityFunctions = function UtilityFunctions() {
  (0, _classCallCheck2.default)(this, UtilityFunctions);
};

var _default = new UtilityFunctions();

exports.default = _default;