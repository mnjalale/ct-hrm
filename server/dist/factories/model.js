"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _formatDate = Symbol('_formatDate');

var ModelFactory =
/*#__PURE__*/
function () {
  function ModelFactory() {
    var _this = this;

    (0, _classCallCheck2.default)(this, ModelFactory);
    (0, _defineProperty2.default)(this, "getPerformanceAppraisalModel", function (input) {
      if (!input) {
        return null;
      }

      var appraisal = {
        id: input.id,
        startDate: _this[_formatDate](input.startDate),
        midYearReviewDate: _this[_formatDate](input.midYearReviewDate),
        endDate: _this[_formatDate](input.endDate),
        appraiseePeriodStartSignature: input.appraiseePeriodStartSignature,
        appraiseePeriodStartSignatureDate: _this[_formatDate](input.appraiseePeriodStartSignatureDate),
        supervisorPeriodStartName: input.supervisorPeriodStartName,
        supervisorPeriodStartSignature: input.supervisorPeriodStartSignature,
        supervisorPeriodStartSignatureDate: _this[_formatDate](input.supervisorPeriodStartSignatureDate),
        supervisorMidYearReviewName: input.supervisorMidYearReviewName,
        supervisorMidYearReviewSignature: input.supervisorMidYearReviewSignature,
        supervisorMidYearReviewSignatureDate: _this[_formatDate](input.supervisorMidYearReviewSignatureDate),
        trainingAppraiseeSignature: input.trainingAppraiseeSignature,
        trainingAppraiseeSignatureDate: _this[_formatDate](input.trainingAppraiseeSignatureDate),
        trainingSupervisorName: input.trainingSupervisorName,
        trainingSupervisorSignature: input.trainingSupervisorSignature,
        trainingSupervisorSignatureDate: _this[_formatDate](input.trainingSupervisorSignatureDate),
        appraiseePeriodEndComments: input.appraiseePeriodEndComments,
        supervisorPeriodEndComments: input.supervisorPeriodEndComments,
        supervisorPeriodEndName: input.supervisorPeriodEndName,
        supervisorPeriodEndSignature: input.supervisorPeriodEndSignature,
        supervisorPeriodEndSignatureDate: _this[_formatDate](input.supervisorPeriodEndSignatureDate),
        recommendedRewardType: input.recommendedRewardType,
        recommendedOtherInterventions: input.recommendedOtherInterventions,
        recommendedSanction: input.recommendedSanction,
        recommendationMinuteNo: input.recommendationMinuteNo,
        recommendationMeetingDate: _this[_formatDate](input.recommendationMeetingDate),
        recommendationChairPersonName: input.recommendationChairPersonName,
        recommendationChairPersonSignature: input.recommendationChairPersonSignature,
        recommendationChairPersonSignatureDate: _this[_formatDate](input.recommendationChairPersonSignatureDate),
        recommendationSecretaryName: input.recommendationSecretaryName,
        recommendationSecretarySignature: input.recommendationSecretarySignature,
        recommendationSecretarySignatureDate: _this[_formatDate](input.recommendationSecretarySignatureDate),
        recommendationAuthorizedOfficerApproved: input.recommendationAuthorizedOfficerApproved,
        recommendationAuthorizedOfficerName: input.recommendationAuthorizedOfficerName,
        recommendationAuthorizedOfficerSignature: input.recommendationAuthorizedOfficerSignature,
        recommendationAuthorizedOfficerSignatureDate: _this[_formatDate](input.recommendationAuthorizedOfficerSignatureDate),
        employeeId: input.employeeId
      };
      return appraisal;
    });
  }

  (0, _createClass2.default)(ModelFactory, [{
    key: _formatDate,
    value: function value(_value) {
      var returnValue = null;

      if (_value) {
        returnValue = new Date(_value);
      }

      return returnValue;
    }
  }]);
  return ModelFactory;
}();

var _default = new ModelFactory();

exports.default = _default;