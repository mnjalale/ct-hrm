"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _roles = _interopRequireDefault(require("../utils/enums/roles"));

var DtoFactory = function DtoFactory() {
  var _this = this;

  (0, _classCallCheck2.default)(this, DtoFactory);
  (0, _defineProperty2.default)(this, "getUserDto", function (user) {
    if (!user) {
      return null;
    }

    var isManager = !!user.Roles ? !!user.Roles.find(function (role) {
      return role.name === _roles.default.Manager;
    }) : false;
    var isAdmin = !!user.Roles ? !!user.Roles.find(function (role) {
      return role.name === _roles.default.Administrator;
    }) : false;
    var hasActiveAppraisal = !!user.PerformanceAppraisals ? !!user.PerformanceAppraisals.find(function (performanceAppraisal) {
      var today = new Date();
      return performanceAppraisal.startDate <= today && today <= performanceAppraisal.endDate;
    }) : false;
    var userDto = {
      id: user.id,
      firstName: user.firstName,
      middleName: user.middleName,
      lastName: user.lastName,
      gender: !!user.Gender ? user.Gender.code : '',
      email: user.email,
      phoneNumber: user.phoneNumber,
      username: user.username,
      jobGroupId: user.jobGroupId,
      jobGroup: !!user.JobGroup ? _this.getJobGroupDto(user.JobGroup) : null,
      designationId: user.designationId,
      ministryId: user.ministryId,
      departmentId: user.departmentId,
      sectionId: user.sectionId,
      dutyStationId: user.dutyStationId,
      designation: !!user.Designation ? _this.getDesignationDto(user.Designation) : null,
      managerId: user.managerId,
      // manager: !!user.Manager
      employmentTypeId: user.employmentTypeId,
      employmentType: !!user.EmploymentType ? _this.getEmploymentTypeDto(user.EmploymentType) : null,
      isManager: isManager,
      isAdmin: isAdmin,
      hasActiveAppraisal: hasActiveAppraisal
    };
    return userDto;
  });
  (0, _defineProperty2.default)(this, "getJobGroupDto", function (jobGroup) {
    if (!jobGroup) {
      return null;
    }

    var jobGroupDto = {
      id: jobGroup.id,
      code: jobGroup.code,
      alternativeCode: jobGroup.alternativeCode,
      description: jobGroup.description
    };
    return jobGroupDto;
  });
  (0, _defineProperty2.default)(this, "getDesignationDto", function (designation) {
    if (!designation) {
      return null;
    }

    var designationDto = {
      id: designation.id,
      name: designation.name
    };
    return designationDto;
  });
  (0, _defineProperty2.default)(this, "getEmploymentTypeDto", function (employmentType) {
    if (!employmentType) {
      return null;
    }

    var employmentTypeDto = {
      id: employmentType.id,
      name: employmentType.name
    };
    return employmentTypeDto;
  });
  (0, _defineProperty2.default)(this, "getPerformanceAppraisalDto", function (appraisal) {
    if (!appraisal) {
      return null;
    }

    var employee = !!appraisal.User ? _this.getUserDto(appraisal.User) : null;
    var appraisalDto = {
      id: appraisal.id,
      startDate: appraisal.startDate,
      midYearReviewDate: appraisal.midYearReviewDate,
      endDate: appraisal.endDate,
      appraiseePeriodStartSignature: appraisal.appraiseePeriodStartSignature,
      appraiseePeriodStartSignatureDate: appraisal.appraiseePeriodStartSignatureDate,
      supervisorPeriodStartName: appraisal.supervisorPeriodStartName,
      supervisorPeriodStartSignature: appraisal.supervisorPeriodStartSignature,
      supervisorPeriodStartSignatureDate: appraisal.supervisorPeriodStartSignatureDate,
      supervisorMidYearReviewName: appraisal.supervisorMidYearReviewName,
      supervisorMidYearReviewSignature: appraisal.supervisorMidYearReviewSignature,
      supervisorMidYearReviewSignatureDate: appraisal.supervisorMidYearReviewSignatureDate,
      trainingAppraiseeSignature: appraisal.trainingAppraiseeSignature,
      trainingAppraiseeSignatureDate: appraisal.trainingAppraiseeSignatureDate,
      trainingSupervisorName: appraisal.trainingSupervisorName,
      trainingSupervisorSignature: appraisal.trainingSupervisorSignature,
      trainingSupervisorSignatureDate: appraisal.trainingSupervisorSignatureDate,
      appraiseePeriodEndComments: appraisal.appraiseePeriodEndComments,
      supervisorPeriodEndComments: appraisal.supervisorPeriodEndComments,
      supervisorPeriodEndName: appraisal.supervisorPeriodEndName,
      supervisorPeriodEndSignature: appraisal.supervisorPeriodEndSignature,
      supervisorPeriodEndSignatureDate: appraisal.supervisorPeriodEndSignatureDate,
      recommendedRewardType: appraisal.recommendedRewardType,
      recommendedOtherInterventions: appraisal.recommendedOtherInterventions,
      recommendedSanction: appraisal.recommendedSanction,
      recommendationMinuteNo: appraisal.recommendationMinuteNo,
      recommendationMeetingDate: appraisal.recommendationMeetingDate,
      recommendationChairPersonName: appraisal.recommendationChairPersonName,
      recommendationChairPersonSignature: appraisal.recommendationChairPersonSignature,
      recommendationChairPersonSignatureDate: appraisal.recommendationChairPersonSignatureDate,
      recommendationSecretaryName: appraisal.recommendationSecretaryName,
      recommendationSecretarySignature: appraisal.recommendationSecretarySignature,
      recommendationSecretarySignatureDate: appraisal.recommendationSecretarySignatureDate,
      recommendationAuthorizedOfficerApproved: appraisal.recommendationAuthorizedOfficerApproved,
      recommendationAuthorizedOfficerName: appraisal.recommendationAuthorizedOfficerName,
      recommendationAuthorizedOfficerSignature: appraisal.recommendationAuthorizedOfficerSignature,
      recommendationAuthorizedOfficerSignatureDate: appraisal.recommendationAuthorizedOfficerSignatureDate,
      employeeId: appraisal.employeeId,
      employee: employee,
      performanceAppraisalTargets: appraisal.PerformanceAppraisalTargets.map(function (target) {
        return _this.getPerformanceAppraisalTargetDto(target);
      }),
      performanceAppraisalTrainingNeeds: appraisal.PerformanceAppraisalTrainingNeeds.map(function (need) {
        return _this.getPerformanceAppraisalTrainingNeedDto(need);
      }),
      performanceAppraisalAdditionalAssignments: appraisal.PerformanceAppraisalAdditionalAssignments.map(function (additionalAssignment) {
        return _this.getPerformanceAppraisalAdditionalAssignmentDto(additionalAssignment);
      })
    };
    return appraisalDto;
  });
  (0, _defineProperty2.default)(this, "getPerformanceAppraisalTargetDto", function (performanceTarget) {
    var performanceTargetDto = {
      id: performanceTarget.id,
      performanceTarget: performanceTarget.performanceTarget,
      performanceIndicator: performanceTarget.performanceIndicator,
      midYearReviewRemarks: performanceTarget.midYearReviewRemarks,
      updatedPerformanceTarget: performanceTarget.updatedPerformanceTarget,
      achievedResult: performanceTarget.achievedResult,
      performanceAppraisalScore: performanceTarget.performanceAppraisalScore,
      performanceAppraisalId: performanceTarget.performanceAppraisalId
    };
    return performanceTargetDto;
  });
  (0, _defineProperty2.default)(this, "getPerformanceAppraisalTrainingNeedDto", function (performanceAppraisalTrainingNeed) {
    var performanceAppraisalTrainingNeedDto = {
      id: performanceAppraisalTrainingNeed.id,
      description: performanceAppraisalTrainingNeed.description,
      performanceAppraisalId: performanceAppraisalTrainingNeed.performanceAppraisalId
    };
    return performanceAppraisalTrainingNeedDto;
  });
  (0, _defineProperty2.default)(this, "getPerformanceAppraisalAdditionalAssignmentDto", function (performanceAppraisalAdditionalAssignment) {
    var performanceAppraisalAdditionalAssignmentDto = {
      id: performanceAppraisalAdditionalAssignment.id,
      description: performanceAppraisalAdditionalAssignment.description,
      performanceAppraisalId: performanceAppraisalAdditionalAssignment.performanceAppraisalId
    };
    return performanceAppraisalAdditionalAssignmentDto;
  });
};

var _default = new DtoFactory();

exports.default = _default;