'use strict';

module.exports = function (sequelize, DataTypes) {
  var Role = sequelize.define('Role', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  Role.associate = function (models) {
    // associations can be defined here
    Role.belongsToMany(models.User, {
      through: models.UserRole
    });
  };

  return Role;
};