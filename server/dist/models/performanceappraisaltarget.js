'use strict';

module.exports = function (sequelize, DataTypes) {
  var PerformanceAppraisalTarget = sequelize.define('PerformanceAppraisalTarget', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    performanceTarget: {
      type: DataTypes.STRING,
      allowNull: false
    },
    performanceIndicator: {
      type: DataTypes.STRING,
      allowNull: false
    },
    midYearReviewRemarks: {
      type: DataTypes.STRING
    },
    updatedPerformanceTarget: {
      type: DataTypes.STRING
    },
    achievedResult: {
      type: DataTypes.STRING
    },
    performanceAppraisalScore: {
      type: DataTypes.DECIMAL
    }
  }, {});

  PerformanceAppraisalTarget.associate = function (models) {
    // associations can be defined here
    PerformanceAppraisalTarget.belongsTo(models.PerformanceAppraisal, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });
  };

  return PerformanceAppraisalTarget;
};