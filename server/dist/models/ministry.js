'use strict';

module.exports = function (sequelize, DataTypes) {
  var Ministry = sequelize.define('Ministry', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  Ministry.associate = function (models) {
    // associations can be defined here
    Ministry.hasMany(models.User, {
      foreignKey: 'ministryId',
      onDelete: 'RESTRICT'
    });
    Ministry.hasMany(models.Department, {
      foreignKey: 'ministryId',
      onDelete: 'RESTRICT'
    });
  };

  return Ministry;
};