'use strict';

module.exports = function (sequelize, DataTypes) {
  var PerformanceAppraisalAdditionalAssignment = sequelize.define('PerformanceAppraisalAdditionalAssignment', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  PerformanceAppraisalAdditionalAssignment.associate = function (models) {
    // associations can be defined here
    PerformanceAppraisalAdditionalAssignment.belongsTo(models.PerformanceAppraisal, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });
  };

  return PerformanceAppraisalAdditionalAssignment;
};