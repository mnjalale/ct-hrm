'use strict';

module.exports = function (sequelize, DataTypes) {
  var DutyStation = sequelize.define('DutyStation', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  DutyStation.associate = function (models) {
    // associations can be defined here
    DutyStation.hasMany(models.User, {
      foreignKey: 'dutyStationId',
      onDelete: 'RESTRICT'
    });
    DutyStation.belongsTo(models.Section, {
      foreignKey: 'sectionId',
      onDelete: 'RESTRICT'
    });
  };

  return DutyStation;
};