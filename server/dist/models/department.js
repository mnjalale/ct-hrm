'use strict';

module.exports = function (sequelize, DataTypes) {
  var Department = sequelize.define('Department', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  Department.associate = function (models) {
    // associations can be defined here
    Department.hasMany(models.User, {
      foreignKey: 'departmentId',
      onDelete: 'RESTRICT'
    });
    Department.hasMany(models.Section, {
      foreignKey: 'departmentId',
      onDelete: 'RESTRICT'
    });
    Department.belongsTo(models.Ministry, {
      foreignKey: 'ministryId',
      onDelete: 'RESTRICT'
    });
  };

  return Department;
};