'use strict';

module.exports = function (sequelize, DataTypes) {
  var UserRole = sequelize.define('UserRole', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false
    }
  }, {});

  UserRole.associate = function (models) {// associations can be defined here
  };

  return UserRole;
};