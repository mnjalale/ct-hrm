'use strict';

module.exports = function (sequelize, DataTypes) {
  var JobGroup = sequelize.define('JobGroup', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    alternativeCode: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    alternativeDescription: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  JobGroup.associate = function (models) {
    // associations can be defined here
    JobGroup.hasMany(models.User, {
      foreignKey: 'jobGroupId',
      onDelete: 'RESTRICT'
    });
  };

  return JobGroup;
};