"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _dutyStations = _interopRequireDefault(require("../controllers/dutyStations"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _dutyStations2 = _interopRequireDefault(require("../middleware/route-validators/dutyStations"));

var _config = Symbol('config');

var DutyStationRoutes =
/*#__PURE__*/
function () {
  function DutyStationRoutes() {
    (0, _classCallCheck2.default)(this, DutyStationRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(DutyStationRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _dutyStations2.default.validateGetDutyStation(), _dutyStations.default.getDutyStation);
      this.router.post('/getAll', _security.default.checkAuthentication, _dutyStations.default.getDutyStations);
      this.router.post('/create', _security.default.checkAuthentication, _dutyStations2.default.validateCreateDutyStation(), _dutyStations.default.createDutyStation);
      this.router.post('/update', _security.default.checkAuthentication, _dutyStations2.default.validateUpdateDutyStation(), _dutyStations.default.updateDutyStation);
      this.router.post('/delete', _security.default.checkAuthentication, _dutyStations2.default.validateDeleteDutyStation(), _dutyStations.default.deleteDutyStation);
    }
  }]);
  return DutyStationRoutes;
}();

var _default = new DutyStationRoutes().router;
exports.default = _default;