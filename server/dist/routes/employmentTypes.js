"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _employmentTypes = _interopRequireDefault(require("../controllers/employmentTypes"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _employmentTypes2 = _interopRequireDefault(require("../middleware/route-validators/employmentTypes"));

var _config = Symbol('config');

var EmploymentTypeRoutes =
/*#__PURE__*/
function () {
  function EmploymentTypeRoutes() {
    (0, _classCallCheck2.default)(this, EmploymentTypeRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(EmploymentTypeRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _employmentTypes2.default.validateGetEmploymentType(), _employmentTypes.default.getEmploymentType);
      this.router.post('/getAll', _security.default.checkAuthentication, _employmentTypes.default.getEmploymentTypes);
      this.router.post('/create', _security.default.checkAuthentication, _employmentTypes2.default.validateCreateEmploymentType(), _employmentTypes.default.createEmploymentType);
      this.router.post('/update', _security.default.checkAuthentication, _employmentTypes2.default.validateUpdateEmploymentType(), _employmentTypes.default.updateEmploymentType);
      this.router.post('/delete', _security.default.checkAuthentication, _employmentTypes2.default.validateDeleteEmploymentType(), _employmentTypes.default.deleteEmploymentType);
    }
  }]);
  return EmploymentTypeRoutes;
}();

var _default = new EmploymentTypeRoutes().router;
exports.default = _default;