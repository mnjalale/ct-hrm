"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _sections = _interopRequireDefault(require("../controllers/sections"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _sections2 = _interopRequireDefault(require("../middleware/route-validators/sections"));

var _config = Symbol('config');

var SectionRoutes =
/*#__PURE__*/
function () {
  function SectionRoutes() {
    (0, _classCallCheck2.default)(this, SectionRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(SectionRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _sections2.default.validateGetSection(), _sections.default.getSection);
      this.router.post('/getAll', _security.default.checkAuthentication, _sections.default.getSections);
      this.router.post('/create', _security.default.checkAuthentication, _sections2.default.validateCreateSection(), _sections.default.createSection);
      this.router.post('/update', _security.default.checkAuthentication, _sections2.default.validateUpdateSection(), _sections.default.updateSection);
      this.router.post('/delete', _security.default.checkAuthentication, _sections2.default.validateDeleteSection(), _sections.default.deleteSection);
    }
  }]);
  return SectionRoutes;
}();

var _default = new SectionRoutes().router;
exports.default = _default;