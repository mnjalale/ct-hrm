"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _auth = _interopRequireDefault(require("../controllers/auth"));

var _auth2 = _interopRequireDefault(require("../middleware/route-validators/auth"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _config = Symbol('config');

var AuthRoutes =
/*#__PURE__*/
function () {
  function AuthRoutes() {
    (0, _classCallCheck2.default)(this, AuthRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(AuthRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/login', _auth2.default.validateLogin(), _auth.default.login);
      this.router.post('/refreshToken', _auth2.default.validateRefreshToken(), _auth.default.refreshToken);
      this.router.post('/createUser', _security.default.checkAdminAuthorization, _auth2.default.validateCreateUser(), _auth.default.createUser);
      this.router.post('/updateUser', _security.default.checkAuthentication, _auth2.default.validateUpdateUser(), _auth.default.updateUser);
      this.router.post('/getUser', _security.default.checkAuthentication, _auth2.default.validateGetUser(), _auth.default.getUser);
      this.router.post('/getUsers', _security.default.checkAdminAuthorization, _auth.default.getUsers);
    }
  }]);
  return AuthRoutes;
}();

var _default = new AuthRoutes().router;
exports.default = _default;