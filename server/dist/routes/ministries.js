"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _ministries = _interopRequireDefault(require("../controllers/ministries"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _ministries2 = _interopRequireDefault(require("../middleware/route-validators/ministries"));

var _config = Symbol('config');

var MinistryRoutes =
/*#__PURE__*/
function () {
  function MinistryRoutes() {
    (0, _classCallCheck2.default)(this, MinistryRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(MinistryRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _ministries2.default.validateGetMinistry(), _ministries.default.getMinistry);
      this.router.post('/getAll', _security.default.checkAuthentication, _ministries.default.getMinistries);
      this.router.post('/create', _security.default.checkAuthentication, _ministries2.default.validateCreateMinistry(), _ministries.default.createMinistry);
      this.router.post('/update', _security.default.checkAuthentication, _ministries2.default.validateUpdateMinistry(), _ministries.default.updateMinistry);
      this.router.post('/delete', _security.default.checkAuthentication, _ministries2.default.validateDeleteMinistry(), _ministries.default.deleteMinistry);
    }
  }]);
  return MinistryRoutes;
}();

var _default = new MinistryRoutes().router;
exports.default = _default;