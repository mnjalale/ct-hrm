"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _designations = _interopRequireDefault(require("../controllers/designations"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _designations2 = _interopRequireDefault(require("../middleware/route-validators/designations"));

var _config = Symbol('config');

var DesignationRoutes =
/*#__PURE__*/
function () {
  function DesignationRoutes() {
    (0, _classCallCheck2.default)(this, DesignationRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(DesignationRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _designations2.default.validateGetDesignation(), _designations.default.getDesignation);
      this.router.post('/getAll', _security.default.checkAuthentication, _designations.default.getDesignations);
      this.router.post('/create', _security.default.checkAuthentication, _designations2.default.validateCreateDesignation(), _designations.default.createDesignation);
      this.router.post('/update', _security.default.checkAuthentication, _designations2.default.validateUpdateDesignation(), _designations.default.updateDesignation);
      this.router.post('/delete', _security.default.checkAuthentication, _designations2.default.validateDeleteDesignation(), _designations.default.deleteDesignation);
    }
  }]);
  return DesignationRoutes;
}();

var _default = new DesignationRoutes().router;
exports.default = _default;