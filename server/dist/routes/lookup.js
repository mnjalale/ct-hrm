"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _lookup = _interopRequireDefault(require("../controllers/lookup"));

var _designations = _interopRequireDefault(require("../controllers/designations"));

var _employmentTypes = _interopRequireDefault(require("../controllers/employmentTypes"));

var _ministries = _interopRequireDefault(require("../controllers/ministries"));

var _departments = _interopRequireDefault(require("../controllers/departments"));

var _sections = _interopRequireDefault(require("../controllers/sections"));

var _dutyStations = _interopRequireDefault(require("../controllers/dutyStations"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _departments2 = _interopRequireDefault(require("../middleware/route-validators/departments"));

var _designations2 = _interopRequireDefault(require("../middleware/route-validators/designations"));

var _dutyStations2 = _interopRequireDefault(require("../middleware/route-validators/dutyStations"));

var _employmentTypes2 = _interopRequireDefault(require("../middleware/route-validators/employmentTypes"));

var _ministries2 = _interopRequireDefault(require("../middleware/route-validators/ministries"));

var _sections2 = _interopRequireDefault(require("../middleware/route-validators/sections"));

var _config = Symbol('config');

var LookupRoutes =
/*#__PURE__*/
function () {
  function LookupRoutes() {
    (0, _classCallCheck2.default)(this, LookupRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(LookupRoutes, [{
    key: _config,
    value: function value() {
      this.router.get('/jobGroups', _security.default.checkAuthentication, _lookup.default.getJobGroups);
      this.router.get('/managers', _security.default.checkAuthentication, _lookup.default.getManagers);
    }
  }]);
  return LookupRoutes;
}();

var _default = new LookupRoutes().router;
exports.default = _default;