"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _employees = _interopRequireDefault(require("../controllers/employees"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _config = Symbol('config');

var EmployeesRoutes =
/*#__PURE__*/
function () {
  function EmployeesRoutes() {
    (0, _classCallCheck2.default)(this, EmployeesRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(EmployeesRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAdminAuthorization, _employees.default.getEmployees);
      this.router.post('/getManagerTeamMembers', _security.default.checkAuthentication, _employees.default.getManagerTeamMembers);
    }
  }]);
  return EmployeesRoutes;
}();

var _default = new EmployeesRoutes().router;
exports.default = _default;