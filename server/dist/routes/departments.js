"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _departments = _interopRequireDefault(require("../controllers/departments"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _departments2 = _interopRequireDefault(require("../middleware/route-validators/departments"));

var _config = Symbol('config');

var DepartmentRoutes =
/*#__PURE__*/
function () {
  function DepartmentRoutes() {
    (0, _classCallCheck2.default)(this, DepartmentRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(DepartmentRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/get', _security.default.checkAuthentication, _departments2.default.validateGetDepartment(), _departments.default.getDepartment);
      this.router.post('/getAll', _security.default.checkAuthentication, _departments.default.getDepartments);
      this.router.post('/create', _security.default.checkAuthentication, _departments2.default.validateCreateDepartment(), _departments.default.createDepartment);
      this.router.post('/update', _security.default.checkAuthentication, _departments2.default.validateUpdateDepartment(), _departments.default.updateDepartment);
      this.router.post('/delete', _security.default.checkAuthentication, _departments2.default.validateDeleteDepartment(), _departments.default.deleteDepartment);
    }
  }]);
  return DepartmentRoutes;
}();

var _default = new DepartmentRoutes().router;
exports.default = _default;