"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _performance = _interopRequireDefault(require("../middleware/route-validators/performance"));

var _performance2 = _interopRequireDefault(require("../controllers/performance"));

var _security = _interopRequireDefault(require("../middleware/security"));

var _config = Symbol('config');

var PerformanceRoutes =
/*#__PURE__*/
function () {
  function PerformanceRoutes() {
    (0, _classCallCheck2.default)(this, PerformanceRoutes);
    this.router = _express.default.Router();

    this[_config]();
  }

  (0, _createClass2.default)(PerformanceRoutes, [{
    key: _config,
    value: function value() {
      this.router.post('/createAppraisal', _security.default.checkAuthentication, _performance.default.validateCreateAppraisal(), _performance2.default.createAppraisal);
      this.router.post('/updateAppraisal', _security.default.checkAuthentication, _performance.default.validateUpdateAppraisal(), _performance2.default.updateAppraisal);
      this.router.post('/getAppraisal', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisal(), _performance2.default.getPerformanceAppraisal);
      this.router.post('/getEmployeeAppraisals', _security.default.checkAuthentication, _performance.default.validateGetEmployeeAppraisals(), _performance2.default.getEmployeeAppraisals); // Appraisal targets

      this.router.post('/createAppraisalTarget', _security.default.checkAuthentication, _performance.default.validateCreatePerformanceAppraisalTarget(), _performance2.default.createPerformanceAppraisalTarget);
      this.router.post('/updateAppraisalTarget', _security.default.checkAuthentication, _performance.default.validateUpdateAppraisalTarget(), _performance2.default.updatePerformanceAppraisalTarget);
      this.router.post('/deleteAppraisalTarget', _security.default.checkAuthentication, _performance.default.validateDeleteAppraisalTarget(), _performance2.default.deletePerformanceAppraisalTarget);
      this.router.post('/getAppraisalTarget', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalTarget(), _performance2.default.getPerformanceAppraisalTarget);
      this.router.post('/getAppraisalTargets', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalTargets(), _performance2.default.getPerformanceAppraisalTargets); // Appraisal training needs

      this.router.post('/createAppraisalTrainingNeed', _security.default.checkAuthentication, _performance.default.validateCreatePerformanceAppraisalTrainingNeed(), _performance2.default.createPerformanceAppraisalTrainingNeed);
      this.router.post('/updateAppraisalTrainingNeed', _security.default.checkAuthentication, _performance.default.validateUpdatePerformanceAppraisalTrainingNeed(), _performance2.default.updatePerformanceAppraisalTrainingNeed);
      this.router.post('/deleteAppraisalTrainingNeed', _security.default.checkAuthentication, _performance.default.validateDeletePerformanceAppraisalTrainingNeed(), _performance2.default.deletePerformanceAppraisalTrainingNeed);
      this.router.post('/getAppraisalTrainingNeed', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalTrainingNeed(), _performance2.default.getPerformanceAppraisalTrainingNeed);
      this.router.post('/getAppraisalTrainingNeeds', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalTrainingNeeds(), _performance2.default.getPerformanceAppraisalTrainingNeeds); // Appraisal additional assignment

      this.router.post('/createAppraisalAdditionalAssignment', _security.default.checkAuthentication, _performance.default.validateCreatePerformanceAppraisalAdditionalAssignment(), _performance2.default.createPerformanceAppraisalAdditionalAssignment);
      this.router.post('/updateAppraisalAdditionalAssignment', _security.default.checkAuthentication, _performance.default.validateUpdatePerformanceAppraisalAdditionalAssignment(), _performance2.default.updatePerformanceAppraisalAdditionalAssignment);
      this.router.post('/deleteAppraisalAdditionalAssignment', _security.default.checkAuthentication, _performance.default.validateDeletePerformanceAppraisalAdditionalAssignment(), _performance2.default.deletePerformanceAppraisalAdditionalAssignment);
      this.router.post('/getAppraisalAdditionalAssignment', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalAdditionalAssignment(), _performance2.default.getPerformanceAppraisalAdditionalAssignment);
      this.router.post('/getAppraisalAdditionalAssignments', _security.default.checkAuthentication, _performance.default.validateGetPerformanceAppraisalAdditionalAssignments(), _performance2.default.getPerformanceAppraisalAdditionalAssignments);
    }
  }]);
  return PerformanceRoutes;
}();

var _default = new PerformanceRoutes().router;
exports.default = _default;