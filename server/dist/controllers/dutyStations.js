"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var DutyStation = _index.default['DutyStation'];
var Op = _index.default.Sequelize.Op;

var DutyStationsController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(DutyStationsController, _BaseController);

  function DutyStationsController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, DutyStationsController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(DutyStationsController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createDutyStation",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var sectionId, name, existingDutyStation, dutyStation, returnValue;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                sectionId = req.body.sectionId;
                name = req.body.name; // Ensure a similar duty station hasn't been entered for the same section

                _context.next = 6;
                return DutyStation.findOne({
                  where: {
                    sectionId: sectionId,
                    name: name
                  }
                });

              case 6:
                existingDutyStation = _context.sent;

                if (existingDutyStation) {
                  _this.throwError('A duty station with a similar name already exists within the section.', 422);
                }

                _context.next = 10;
                return DutyStation.create({
                  sectionId: sectionId,
                  name: name
                });

              case 10:
                dutyStation = _context.sent;
                returnValue = {
                  id: dutyStation.id,
                  sectionId: dutyStation.sectionId,
                  name: dutyStation.name
                };

                _this.sendSuccessResponse(res, "Duty Station created successfully.", returnValue);

                _context.next = 18;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 15]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateDutyStation",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var id, name, sectionId, existingDutyStation, dutyStation, updatedDutyStation, returnValue;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                name = req.body.name;
                sectionId = req.body.sectionId; // Ensure a similar duty station hasn't been entered

                _context2.next = 7;
                return DutyStation.findOne({
                  where: {
                    id: (0, _defineProperty2.default)({}, Op.ne, id),
                    sectionId: sectionId,
                    name: name
                  }
                });

              case 7:
                existingDutyStation = _context2.sent;

                if (existingDutyStation) {
                  _this.throwError('A duty station with a similar name already exists within the section.', 422);
                }

                _context2.next = 11;
                return DutyStation.findByPk(id);

              case 11:
                dutyStation = _context2.sent;
                _context2.next = 14;
                return dutyStation.update({
                  name: name,
                  sectionId: sectionId
                });

              case 14:
                updatedDutyStation = _context2.sent;
                returnValue = {
                  id: updatedDutyStation.id,
                  name: updatedDutyStation.name,
                  sectionId: updatedDutyStation.sectionId
                };

                _this.sendSuccessResponse(res, "Duty Station updated successfully", returnValue);

                _context2.next = 22;
                break;

              case 19:
                _context2.prev = 19;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 22:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 19]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deleteDutyStation",
    /*#__PURE__*/
    function () {
      var _ref3 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(req, res, next) {
        var id, dutyStation;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context3.next = 5;
                return DutyStation.findByPk(id);

              case 5:
                dutyStation = _context3.sent;
                _context3.next = 8;
                return dutyStation.destroy();

              case 8:
                _this.sendSuccessResponse(res, "Duty Station deleted successfully", {
                  message: 'Duty Station deleted successfully"'
                });

                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);

                _this.catchControllerErrors(_context3.t0, next);

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 11]]);
      }));

      return function (_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getDutyStation",
    /*#__PURE__*/
    function () {
      var _ref4 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(req, res, next) {
        var dutyStationId, dutyStation, returnValue;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;

                _this.validateRequest(req);

                dutyStationId = req.body.id;
                _context4.next = 5;
                return DutyStation.findOne({
                  where: {
                    id: dutyStationId
                  }
                });

              case 5:
                dutyStation = _context4.sent;
                returnValue = {
                  id: dutyStation.id,
                  name: dutyStation.name,
                  sectionId: dutyStation.sectionId
                };

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);

                _this.catchControllerErrors(_context4.t0, next);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 10]]);
      }));

      return function (_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getDutyStations",
    /*#__PURE__*/
    function () {
      var _ref5 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(req, res, next) {
        var sectionId, dutyStations, returnValue;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                sectionId = req.body.sectionId;

                if (!sectionId) {
                  _context5.next = 8;
                  break;
                }

                _context5.next = 5;
                return DutyStation.findAll({
                  where: {
                    sectionId: sectionId
                  },
                  order: [['name', 'ASC']]
                });

              case 5:
                dutyStations = _context5.sent;
                _context5.next = 11;
                break;

              case 8:
                _context5.next = 10;
                return DutyStation.findAll({
                  order: [['name', 'ASC']]
                });

              case 10:
                dutyStations = _context5.sent;

              case 11:
                returnValue = dutyStations.map(function (dutyStation) {
                  return {
                    id: dutyStation.id,
                    name: dutyStation.name,
                    sectionId: dutyStation.sectionId
                  };
                });

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context5.next = 18;
                break;

              case 15:
                _context5.prev = 15;
                _context5.t0 = _context5["catch"](0);

                _this.catchControllerErrors(_context5.t0, next);

              case 18:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 15]]);
      }));

      return function (_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
      };
    }());
    return _this;
  }

  return DutyStationsController;
}(_baseController.default);

var _default = new DutyStationsController();

exports.default = _default;