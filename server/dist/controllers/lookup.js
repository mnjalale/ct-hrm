"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var _roles = _interopRequireDefault(require("../utils/enums/roles"));

var User = _index.default['User'];
var Role = _index.default['Role'];
var JobGroup = _index.default['JobGroup'];
var EmploymentType = _index.default['EmploymentType'];

var LookupController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(LookupController, _BaseController);

  function LookupController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, LookupController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(LookupController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getJobGroups",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var jobGroups;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return JobGroup.findAll({
                  order: [['code', 'ASC']]
                });

              case 3:
                jobGroups = _context.sent;

                _this.sendSuccessResponse(res, "Success", jobGroups);

                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getManagers",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var managers, returnValue;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return User.findAll({
                  include: {
                    model: Role,
                    required: true,
                    where: {
                      name: _roles.default.Manager
                    }
                  },
                  order: [['firstName', 'ASC']]
                });

              case 3:
                managers = _context2.sent;
                returnValue = managers.map(function (manager) {
                  return {
                    id: manager.id,
                    firstName: manager.firstName,
                    middleName: manager.middleName,
                    lastName: manager.lastName,
                    fullName: manager.firstName + ' ' + manager.lastName
                  };
                });

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context2.next = 11;
                break;

              case 8:
                _context2.prev = 8;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 8]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    return _this;
  }

  return LookupController;
}(_baseController.default);

var _default = new LookupController();

exports.default = _default;