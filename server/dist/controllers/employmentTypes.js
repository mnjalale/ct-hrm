"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var EmploymentType = _index.default['EmploymentType'];
var Op = _index.default.Sequelize.Op;

var EmploymentTypesController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(EmploymentTypesController, _BaseController);

  function EmploymentTypesController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, EmploymentTypesController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(EmploymentTypesController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createEmploymentType",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var name, existingEmploymentType, employmentType, returnValue;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                name = req.body.name; // Ensure a similar employment type hasn't been entered

                _context.next = 5;
                return EmploymentType.findOne({
                  where: {
                    name: name
                  }
                });

              case 5:
                existingEmploymentType = _context.sent;

                if (existingEmploymentType) {
                  _this.throwError('An employment type with a similar name already exists.', 422);
                }

                _context.next = 9;
                return EmploymentType.create({
                  name: name
                });

              case 9:
                employmentType = _context.sent;
                returnValue = {
                  id: employmentType.id,
                  name: employmentType.name
                };

                _this.sendSuccessResponse(res, "Employment type created successfully.", returnValue);

                _context.next = 17;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 14]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateEmploymentType",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var id, name, existingEmploymentType, employmentType, updatedEmploymentType, returnValue;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                name = req.body.name; // Ensure a similar employment type hasn't been entered

                _context2.next = 6;
                return EmploymentType.findOne({
                  where: {
                    name: name,
                    id: (0, _defineProperty2.default)({}, Op.ne, id)
                  }
                });

              case 6:
                existingEmploymentType = _context2.sent;

                if (existingEmploymentType) {
                  _this.throwError('An employment type with a similar name already exists.', 422);
                }

                _context2.next = 10;
                return EmploymentType.findByPk(id);

              case 10:
                employmentType = _context2.sent;
                _context2.next = 13;
                return employmentType.update({
                  name: name
                });

              case 13:
                updatedEmploymentType = _context2.sent;
                returnValue = {
                  id: updatedEmploymentType.id,
                  name: updatedEmploymentType.name
                };

                _this.sendSuccessResponse(res, "Employment type updated successfully", returnValue);

                _context2.next = 21;
                break;

              case 18:
                _context2.prev = 18;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 21:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 18]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deleteEmploymentType",
    /*#__PURE__*/
    function () {
      var _ref3 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(req, res, next) {
        var id, employmentType;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context3.next = 5;
                return EmploymentType.findByPk(id);

              case 5:
                employmentType = _context3.sent;
                _context3.next = 8;
                return employmentType.destroy();

              case 8:
                _this.sendSuccessResponse(res, "Employment Type deleted successfully", {
                  message: 'Employment Type deleted successfully'
                });

                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);

                _this.catchControllerErrors(_context3.t0, next);

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 11]]);
      }));

      return function (_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getEmploymentType",
    /*#__PURE__*/
    function () {
      var _ref4 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(req, res, next) {
        var employmentTypeId, employmentType, returnValue;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;

                _this.validateRequest(req);

                employmentTypeId = req.body.id;
                _context4.next = 5;
                return EmploymentType.findOne({
                  where: {
                    id: employmentTypeId
                  }
                });

              case 5:
                employmentType = _context4.sent;
                returnValue = {
                  id: employmentType.id,
                  name: employmentType.name
                };

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);

                _this.catchControllerErrors(_context4.t0, next);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 10]]);
      }));

      return function (_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getEmploymentTypes",
    /*#__PURE__*/
    function () {
      var _ref5 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(req, res, next) {
        var employmentTypes, returnValue;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return EmploymentType.findAll({
                  order: [['name', 'ASC']]
                });

              case 3:
                employmentTypes = _context5.sent;
                returnValue = employmentTypes.map(function (employmentType) {
                  return {
                    id: employmentType.id,
                    name: employmentType.name
                  };
                });

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context5.next = 11;
                break;

              case 8:
                _context5.prev = 8;
                _context5.t0 = _context5["catch"](0);

                _this.catchControllerErrors(_context5.t0, next);

              case 11:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 8]]);
      }));

      return function (_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
      };
    }());
    return _this;
  }

  return EmploymentTypesController;
}(_baseController.default);

var _default = new EmploymentTypesController();

exports.default = _default;