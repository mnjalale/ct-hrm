"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var Section = _index.default['Section'];
var Op = _index.default.Sequelize.Op;

var SectionsController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(SectionsController, _BaseController);

  function SectionsController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, SectionsController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(SectionsController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createSection",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var departmentId, name, existingSection, section, returnValue;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                departmentId = req.body.departmentId;
                name = req.body.name; // Ensure a similar section hasn't been entered for the same department

                _context.next = 6;
                return Section.findOne({
                  where: {
                    departmentId: departmentId,
                    name: name
                  }
                });

              case 6:
                existingSection = _context.sent;

                if (existingSection) {
                  _this.throwError('A section with a similar name already exists within the department.', 422);
                }

                _context.next = 10;
                return Section.create({
                  departmentId: departmentId,
                  name: name
                });

              case 10:
                section = _context.sent;
                returnValue = {
                  id: section.id,
                  departmentId: section.departmentId,
                  name: section.name
                };

                _this.sendSuccessResponse(res, "Section created successfully.", returnValue);

                _context.next = 18;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 15]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateSection",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var id, name, departmentId, existingSection, section, updatedSection, returnValue;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                name = req.body.name;
                departmentId = req.body.departmentId; // Ensure a similar section hasn't been entered

                _context2.next = 7;
                return Section.findOne({
                  where: {
                    id: (0, _defineProperty2.default)({}, Op.ne, id),
                    departmentId: departmentId,
                    name: name
                  }
                });

              case 7:
                existingSection = _context2.sent;

                if (existingSection) {
                  _this.throwError('A section with a similar name already exists within the department.', 422);
                }

                _context2.next = 11;
                return Section.findByPk(id);

              case 11:
                section = _context2.sent;
                _context2.next = 14;
                return section.update({
                  name: name,
                  departmentId: departmentId
                });

              case 14:
                updatedSection = _context2.sent;
                returnValue = {
                  id: updatedSection.id,
                  name: updatedSection.name,
                  departmentId: updatedSection.departmentId
                };

                _this.sendSuccessResponse(res, "Section updated successfully", returnValue);

                _context2.next = 22;
                break;

              case 19:
                _context2.prev = 19;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 22:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 19]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deleteSection",
    /*#__PURE__*/
    function () {
      var _ref3 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(req, res, next) {
        var id, section;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context3.next = 5;
                return Section.findByPk(id);

              case 5:
                section = _context3.sent;
                _context3.next = 8;
                return section.destroy();

              case 8:
                _this.sendSuccessResponse(res, "Section deleted successfully", {
                  message: 'Section deleted successfully"'
                });

                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);

                _this.catchControllerErrors(_context3.t0, next);

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 11]]);
      }));

      return function (_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getSection",
    /*#__PURE__*/
    function () {
      var _ref4 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(req, res, next) {
        var sectionId, section, returnValue;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;

                _this.validateRequest(req);

                sectionId = req.body.id;
                _context4.next = 5;
                return Section.findOne({
                  where: {
                    id: sectionId
                  }
                });

              case 5:
                section = _context4.sent;
                returnValue = {
                  id: section.id,
                  name: section.name,
                  departmentId: section.departmentId
                };

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);

                _this.catchControllerErrors(_context4.t0, next);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 10]]);
      }));

      return function (_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getSections",
    /*#__PURE__*/
    function () {
      var _ref5 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(req, res, next) {
        var departmentId, sections, returnValue;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                departmentId = req.body.departmentId;

                if (!departmentId) {
                  _context5.next = 8;
                  break;
                }

                _context5.next = 5;
                return Section.findAll({
                  where: {
                    departmentId: departmentId
                  },
                  order: [['name', 'ASC']]
                });

              case 5:
                sections = _context5.sent;
                _context5.next = 11;
                break;

              case 8:
                _context5.next = 10;
                return Section.findAll({
                  order: [['name', 'ASC']]
                });

              case 10:
                sections = _context5.sent;

              case 11:
                returnValue = sections.map(function (section) {
                  return {
                    id: section.id,
                    name: section.name,
                    departmentId: section.departmentId
                  };
                });

                _this.sendSuccessResponse(res, "Success", returnValue);

                _context5.next = 18;
                break;

              case 15:
                _context5.prev = 15;
                _context5.t0 = _context5["catch"](0);

                _this.catchControllerErrors(_context5.t0, next);

              case 18:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 15]]);
      }));

      return function (_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
      };
    }());
    return _this;
  }

  return SectionsController;
}(_baseController.default);

var _default = new SectionsController();

exports.default = _default;