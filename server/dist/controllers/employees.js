"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var _dto = _interopRequireDefault(require("../factories/dto"));

var _roles = _interopRequireDefault(require("../utils/enums/roles"));

var User = _index.default['User'];
var Role = _index.default['Role'];
var Gender = _index.default['Gender'];
var JobGroup = _index.default['JobGroup'];
var EmploymentType = _index.default['EmploymentType'];
var Designation = _index.default['Designation'];
var PerformanceAppraisal = _index.default['PerformanceAppraisal'];

var _getWhereClause = Symbol('_getWhereClause');

var EmployeesController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(EmployeesController, _BaseController);

  function EmployeesController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, EmployeesController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(EmployeesController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getManagerTeamMembers",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var managerId, users, userDtos;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                managerId = req.userId;
                _context.next = 5;
                return User.findAll({
                  order: [['firstName', 'ASC']],
                  include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal],
                  where: {
                    managerId: managerId
                  }
                });

              case 5:
                users = _context.sent;
                userDtos = users.map(function (user) {
                  return _dto.default.getUserDto(user);
                });

                _this.sendSuccessResponse(res, 'Success', userDtos);

                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 10]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getEmployees",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var users, whereClause, userDtos;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                whereClause = _this[_getWhereClause](req.body.ministryId, req.body.departmentId, req.body.sectionId, req.body.dutyStationId);

                if (!whereClause) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 5;
                return User.findAll({
                  order: [['firstName', 'ASC']],
                  include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal],
                  where: whereClause
                });

              case 5:
                users = _context2.sent;
                _context2.next = 11;
                break;

              case 8:
                _context2.next = 10;
                return User.findAll({
                  order: [['firstName', 'ASC']],
                  include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal]
                });

              case 10:
                users = _context2.sent;

              case 11:
                // Get only users who are employees (have employee role)
                // todo: THIS IS VERY INEFFICIENT (the filtering should happen in the database). 
                // I'LL BE COMING BACK TO THIS.
                users = users.filter(function (user) {
                  return !!user.Roles.find(function (role) {
                    return role.name === _roles.default.Employee;
                  });
                });
                userDtos = users.map(function (user) {
                  return _dto.default.getUserDto(user);
                });

                _this.sendSuccessResponse(res, 'Success', userDtos);

                _context2.next = 19;
                break;

              case 16:
                _context2.prev = 16;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 19:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 16]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), _getWhereClause, function (ministryId, departmentId, sectionId, dutyStationId) {
      var whereClause;

      if (ministryId && departmentId && sectionId && dutyStationId) {
        whereClause = {
          ministryId: ministryId,
          departmentId: departmentId,
          sectionId: sectionId,
          dutyStationId: dutyStationId
        };
      } else if (ministryId && departmentId && sectionId) {
        whereClause = {
          ministryId: ministryId,
          departmentId: departmentId,
          sectionId: sectionId
        };
      } else if (ministryId && departmentId) {
        whereClause = {
          ministryId: ministryId,
          departmentId: departmentId
        };
      } else if (ministryId) {
        whereClause = {
          ministryId: ministryId
        };
      }

      return whereClause;
    });
    return _this;
  }

  return EmployeesController;
}(_baseController.default);

var _default = new EmployeesController();

exports.default = _default;