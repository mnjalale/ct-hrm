"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _check = require("express-validator/check");

var BaseController =
/*#__PURE__*/
function () {
  function BaseController() {
    (0, _classCallCheck2.default)(this, BaseController);
  }

  (0, _createClass2.default)(BaseController, [{
    key: "sendSuccessResponse",
    value: function sendSuccessResponse(res, message, data) {
      res.status(200).json({
        message: message,
        statusCode: 200,
        data: data
      });
    }
  }, {
    key: "validateRequest",
    value: function validateRequest(req) {
      var errors = (0, _check.validationResult)(req);

      if (!errors.isEmpty()) {
        var error = new Error('Validation Errors Occurred.');
        error.statusCode = 422;
        error.errors = errors.array().map(function (error) {
          return error.msg;
        });
        throw error;
      }
    }
  }, {
    key: "throwError",
    value: function throwError(message, statusCode) {
      var error = new Error(message);
      error.statusCode = statusCode;
      error.errors = [message];
      throw error;
    }
  }, {
    key: "catchControllerErrors",
    value: function catchControllerErrors(error, next) {
      if (!error.statusCode) {
        error.statusCode = 500;
      }

      next(error);
    }
  }]);
  return BaseController;
}();

var _default = BaseController;
exports.default = _default;