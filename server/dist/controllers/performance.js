"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _index = _interopRequireDefault(require("../models/index"));

var _dto = _interopRequireDefault(require("../factories/dto"));

var _model = _interopRequireDefault(require("../factories/model"));

var Op = _index.default.Sequelize.Op;
var User = _index.default['User'];
var Gender = _index.default['Gender'];
var JobGroup = _index.default['JobGroup'];
var EmploymentType = _index.default['EmploymentType'];
var Designation = _index.default['Designation'];
var PerformanceAppraisal = _index.default['PerformanceAppraisal'];
var PerformanceAppraisalTarget = _index.default['PerformanceAppraisalTarget'];
var PerformanceAppraisalTrainingNeed = _index.default['PerformanceAppraisalTrainingNeed'];
var PerformanceAppraisalAdditionalAssignment = _index.default['PerformanceAppraisalAdditionalAssignment'];

var _validateAppraisal = Symbol('_validateAppraisal');

var PerformanceController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(PerformanceController, _BaseController);

  function PerformanceController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, PerformanceController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(PerformanceController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createAppraisal",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var employeeId, startDate, midYearReviewDate, endDate, appraisal;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                employeeId = req.body.employeeId;
                startDate = new Date(req.body.startDate);
                midYearReviewDate = new Date(req.body.midYearReviewDate);
                endDate = new Date(req.body.endDate);
                _context.next = 8;
                return _this[_validateAppraisal](employeeId, startDate, endDate);

              case 8:
                _context.next = 10;
                return PerformanceAppraisal.create({
                  employeeId: employeeId,
                  startDate: startDate,
                  midYearReviewDate: midYearReviewDate,
                  endDate: endDate
                });

              case 10:
                appraisal = _context.sent;

                _this.sendSuccessResponse(res, 'Performance appraisal created successfully.', {
                  employeeId: appraisal.employeeId,
                  startDate: appraisal.startDate,
                  midYearReviewDate: appraisal.midYearReviewDate,
                  endDate: appraisal.endDate
                });

                _context.next = 17;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 14]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateAppraisal",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var model, appraisalToUpdate, updatedAppraisal;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                _this.validateRequest(req);

                model = _model.default.getPerformanceAppraisalModel(req.body);
                _context2.next = 5;
                return PerformanceAppraisal.findOne({
                  include: [PerformanceAppraisalAdditionalAssignment, PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, User],
                  where: {
                    id: req.body.id
                  }
                });

              case 5:
                appraisalToUpdate = _context2.sent;
                _context2.next = 8;
                return appraisalToUpdate.update(model);

              case 8:
                updatedAppraisal = _context2.sent;

                _this.sendSuccessResponse(res, 'Performance appraisal updated successfully.', _dto.default.getPerformanceAppraisalDto(updatedAppraisal));

                _context2.next = 15;
                break;

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](0);

                _this.catchControllerErrors(_context2.t0, next);

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 12]]);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisal",
    /*#__PURE__*/
    function () {
      var _ref3 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(req, res, next) {
        var appraisalId, appraisal, appraisalDto, employee, employeeDto, manager, managerDto;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                _this.validateRequest(req);

                appraisalId = req.body.appraisalId;
                _context3.next = 5;
                return PerformanceAppraisal.findByPk(appraisalId, {
                  include: [PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, PerformanceAppraisalAdditionalAssignment]
                });

              case 5:
                appraisal = _context3.sent;
                appraisalDto = _dto.default.getPerformanceAppraisalDto(appraisal);
                _context3.next = 9;
                return User.findByPk(appraisal.employeeId, {
                  include: [JobGroup, Designation, EmploymentType, Gender]
                });

              case 9:
                employee = _context3.sent;
                employeeDto = _dto.default.getUserDto(employee);
                _context3.next = 13;
                return User.findByPk(employee.managerId, {
                  include: [JobGroup, Designation, EmploymentType, Gender]
                });

              case 13:
                manager = _context3.sent;
                managerDto = _dto.default.getUserDto(manager);
                employeeDto.manager = managerDto;
                appraisalDto.employee = employeeDto;

                _this.sendSuccessResponse(res, 'Success', appraisalDto);

                _context3.next = 23;
                break;

              case 20:
                _context3.prev = 20;
                _context3.t0 = _context3["catch"](0);

                _this.catchControllerErrors(_context3.t0, next);

              case 23:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 20]]);
      }));

      return function (_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getEmployeeAppraisals",
    /*#__PURE__*/
    function () {
      var _ref4 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(req, res, next) {
        var employeeId, appraisals;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;

                _this.validateRequest(req);

                employeeId = req.body.employeeId;
                _context4.next = 5;
                return PerformanceAppraisal.findAll({
                  include: [PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, PerformanceAppraisalAdditionalAssignment],
                  order: [['startDate', 'DESC']],
                  where: {
                    employeeId: employeeId
                  }
                });

              case 5:
                appraisals = _context4.sent;

                _this.sendSuccessResponse(res, 'Success', appraisals);

                _context4.next = 12;
                break;

              case 9:
                _context4.prev = 9;
                _context4.t0 = _context4["catch"](0);

                _this.catchControllerErrors(_context4.t0, next);

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 9]]);
      }));

      return function (_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createPerformanceAppraisalTarget",
    /*#__PURE__*/
    function () {
      var _ref5 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(req, res, next) {
        var performanceTarget, performanceIndicator, performanceAppraisalId, performanceAppraisalTarget;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;

                _this.validateRequest(req);

                performanceTarget = req.body.performanceTarget;
                performanceIndicator = req.body.performanceIndicator;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context5.next = 7;
                return PerformanceAppraisalTarget.create({
                  performanceTarget: performanceTarget,
                  performanceIndicator: performanceIndicator,
                  performanceAppraisalId: performanceAppraisalId
                });

              case 7:
                performanceAppraisalTarget = _context5.sent;

                _this.sendSuccessResponse(res, 'Performance target created successfully.', _dto.default.getPerformanceAppraisalTargetDto(performanceAppraisalTarget));

                _context5.next = 14;
                break;

              case 11:
                _context5.prev = 11;
                _context5.t0 = _context5["catch"](0);

                _this.catchControllerErrors(_context5.t0, next);

              case 14:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 11]]);
      }));

      return function (_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updatePerformanceAppraisalTarget",
    /*#__PURE__*/
    function () {
      var _ref6 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(req, res, next) {
        var id, performanceTarget, performanceIndicator, midYearReviewRemarks, updatedPerformanceTarget, achievedResult, performanceAppraisalScore, performanceAppraisalId, performanceAppraisalTarget, updatedPerformanceAppraisalTarget;
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                performanceTarget = req.body.performanceTarget;
                performanceIndicator = req.body.performanceIndicator;
                midYearReviewRemarks = req.body.midYearReviewRemarks;
                updatedPerformanceTarget = req.body.updatedPerformanceTarget;
                achievedResult = req.body.achievedResult;
                performanceAppraisalScore = req.body.performanceAppraisalScore;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context6.next = 12;
                return PerformanceAppraisalTarget.findOne({
                  where: {
                    id: id,
                    performanceAppraisalId: performanceAppraisalId
                  }
                });

              case 12:
                performanceAppraisalTarget = _context6.sent;

                if (!performanceAppraisalTarget) {
                  _this.throwError('Performance Target not found.', 404);
                }

                _context6.next = 16;
                return performanceAppraisalTarget.update({
                  performanceTarget: performanceTarget,
                  performanceIndicator: performanceIndicator,
                  midYearReviewRemarks: midYearReviewRemarks,
                  updatedPerformanceTarget: updatedPerformanceTarget,
                  achievedResult: achievedResult,
                  performanceAppraisalScore: performanceAppraisalScore
                });

              case 16:
                updatedPerformanceAppraisalTarget = _context6.sent;

                _this.sendSuccessResponse(res, 'Performance target updated successfully.', _dto.default.getPerformanceAppraisalTargetDto(updatedPerformanceAppraisalTarget));

                _context6.next = 23;
                break;

              case 20:
                _context6.prev = 20;
                _context6.t0 = _context6["catch"](0);

                _this.catchControllerErrors(_context6.t0, next);

              case 23:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 20]]);
      }));

      return function (_x16, _x17, _x18) {
        return _ref6.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deletePerformanceAppraisalTarget",
    /*#__PURE__*/
    function () {
      var _ref7 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee7(req, res, next) {
        var id, performanceAppraisalTarget;
        return _regenerator.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context7.next = 5;
                return PerformanceAppraisalTarget.findByPk(id);

              case 5:
                performanceAppraisalTarget = _context7.sent;

                if (!performanceAppraisalTarget) {
                  _this.throwError('Performance Appraisal Target not found', 404);
                }

                _context7.next = 9;
                return performanceAppraisalTarget.destroy();

              case 9:
                _this.sendSuccessResponse(res, 'Performance target deleted successfully.', {
                  message: 'Performance target deleted successfully.'
                });

                _context7.next = 15;
                break;

              case 12:
                _context7.prev = 12;
                _context7.t0 = _context7["catch"](0);

                _this.catchControllerErrors(_context7.t0, next);

              case 15:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[0, 12]]);
      }));

      return function (_x19, _x20, _x21) {
        return _ref7.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalTarget",
    /*#__PURE__*/
    function () {
      var _ref8 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee8(req, res, next) {
        var id, appraisalTarget, appraisalTargetDto;
        return _regenerator.default.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context8.next = 5;
                return PerformanceAppraisalTarget.findByPk(id);

              case 5:
                appraisalTarget = _context8.sent;

                if (!appraisalTarget) {
                  _this.throwError('Performance Target not found', 404);
                }

                appraisalTargetDto = _dto.default.getPerformanceAppraisalTargetDto(appraisalTarget);

                _this.sendSuccessResponse(res, 'Success', appraisalTargetDto);

                _context8.next = 14;
                break;

              case 11:
                _context8.prev = 11;
                _context8.t0 = _context8["catch"](0);

                _this.catchControllerErrors(_context8.t0, next);

              case 14:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[0, 11]]);
      }));

      return function (_x22, _x23, _x24) {
        return _ref8.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalTargets",
    /*#__PURE__*/
    function () {
      var _ref9 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee9(req, res, next) {
        var appraisalId, appraisalTargets, appraisalTargetsDto;
        return _regenerator.default.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;

                _this.validateRequest(req);

                appraisalId = req.body.appraisalId;
                _context9.next = 5;
                return PerformanceAppraisalTarget.findAll({
                  order: [['createdAt', 'ASC']],
                  where: {
                    performanceAppraisalId: appraisalId
                  }
                });

              case 5:
                appraisalTargets = _context9.sent;
                appraisalTargetsDto = appraisalTargets.map(function (appraisalTarget) {
                  return _dto.default.getPerformanceAppraisalTargetDto(appraisalTarget);
                });

                _this.sendSuccessResponse(res, 'Success', appraisalTargetsDto);

                _context9.next = 13;
                break;

              case 10:
                _context9.prev = 10;
                _context9.t0 = _context9["catch"](0);

                _this.catchControllerErrors(_context9.t0, next);

              case 13:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this, [[0, 10]]);
      }));

      return function (_x25, _x26, _x27) {
        return _ref9.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createPerformanceAppraisalTrainingNeed",
    /*#__PURE__*/
    function () {
      var _ref10 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee10(req, res, next) {
        var description, performanceAppraisalId, performanceAppraisalTrainingNeed;
        return _regenerator.default.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.prev = 0;

                _this.validateRequest(req);

                description = req.body.description;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context10.next = 6;
                return PerformanceAppraisalTrainingNeed.create({
                  description: description,
                  performanceAppraisalId: performanceAppraisalId
                });

              case 6:
                performanceAppraisalTrainingNeed = _context10.sent;

                _this.sendSuccessResponse(res, 'Performance training need created successfully.', _dto.default.getPerformanceAppraisalTrainingNeedDto(performanceAppraisalTrainingNeed));

                _context10.next = 13;
                break;

              case 10:
                _context10.prev = 10;
                _context10.t0 = _context10["catch"](0);

                _this.catchControllerErrors(_context10.t0, next);

              case 13:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this, [[0, 10]]);
      }));

      return function (_x28, _x29, _x30) {
        return _ref10.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updatePerformanceAppraisalTrainingNeed",
    /*#__PURE__*/
    function () {
      var _ref11 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee11(req, res, next) {
        var id, description, performanceAppraisalId, performanceAppraisalTrainingNeed, updatedPerformanceAppraisalTrainingNeed;
        return _regenerator.default.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                description = req.body.description;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context11.next = 7;
                return PerformanceAppraisalTrainingNeed.findOne({
                  where: {
                    id: id,
                    performanceAppraisalId: performanceAppraisalId
                  }
                });

              case 7:
                performanceAppraisalTrainingNeed = _context11.sent;

                if (!performanceAppraisalTrainingNeed) {
                  _this.throwError('Performance training need not found.', 404);
                }

                _context11.next = 11;
                return performanceAppraisalTrainingNeed.update({
                  description: description
                });

              case 11:
                updatedPerformanceAppraisalTrainingNeed = _context11.sent;

                _this.sendSuccessResponse(res, 'Performance training need updated successfully.', _dto.default.getPerformanceAppraisalTrainingNeedDto(updatedPerformanceAppraisalTrainingNeed));

                _context11.next = 18;
                break;

              case 15:
                _context11.prev = 15;
                _context11.t0 = _context11["catch"](0);

                _this.catchControllerErrors(_context11.t0, next);

              case 18:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this, [[0, 15]]);
      }));

      return function (_x31, _x32, _x33) {
        return _ref11.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deletePerformanceAppraisalTrainingNeed",
    /*#__PURE__*/
    function () {
      var _ref12 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee12(req, res, next) {
        var id, performanceAppraisalTrainingNeed;
        return _regenerator.default.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context12.next = 5;
                return PerformanceAppraisalTrainingNeed.findByPk(id);

              case 5:
                performanceAppraisalTrainingNeed = _context12.sent;

                if (!performanceAppraisalTrainingNeed) {
                  _this.throwError('Performance Training Need not found', 404);
                }

                _context12.next = 9;
                return performanceAppraisalTrainingNeed.destroy();

              case 9:
                _this.sendSuccessResponse(res, 'Performance Training Need deleted successfully.', {
                  message: 'Performance Training Need deleted successfully.'
                });

                _context12.next = 15;
                break;

              case 12:
                _context12.prev = 12;
                _context12.t0 = _context12["catch"](0);

                _this.catchControllerErrors(_context12.t0, next);

              case 15:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, this, [[0, 12]]);
      }));

      return function (_x34, _x35, _x36) {
        return _ref12.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalTrainingNeed",
    /*#__PURE__*/
    function () {
      var _ref13 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee13(req, res, next) {
        var id, appraisalTrainingNeed, appraisalTrainingNeedDto;
        return _regenerator.default.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context13.next = 5;
                return PerformanceAppraisalTrainingNeed.findByPk(id);

              case 5:
                appraisalTrainingNeed = _context13.sent;

                if (!appraisalTrainingNeed) {
                  _this.throwError('Performance Training Need not found.', 404);
                }

                appraisalTrainingNeedDto = _dto.default.getPerformanceAppraisalTrainingNeedDto(appraisalTrainingNeed);

                _this.sendSuccessResponse(res, 'Success', appraisalTrainingNeedDto);

                _context13.next = 14;
                break;

              case 11:
                _context13.prev = 11;
                _context13.t0 = _context13["catch"](0);

                _this.catchControllerErrors(_context13.t0, next);

              case 14:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, this, [[0, 11]]);
      }));

      return function (_x37, _x38, _x39) {
        return _ref13.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalTrainingNeeds",
    /*#__PURE__*/
    function () {
      var _ref14 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee14(req, res, next) {
        var appraisalId, appraisalTrainingNeeds, appraisalTrainingNeedsDto;
        return _regenerator.default.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.prev = 0;

                _this.validateRequest(req);

                appraisalId = req.body.appraisalId;
                _context14.next = 5;
                return PerformanceAppraisalTrainingNeed.findAll({
                  order: [['createdAt', 'ASC']],
                  where: {
                    performanceAppraisalId: appraisalId
                  }
                });

              case 5:
                appraisalTrainingNeeds = _context14.sent;
                appraisalTrainingNeedsDto = appraisalTrainingNeeds.map(function (appraisalTrainingNeed) {
                  return _dto.default.getPerformanceAppraisalTrainingNeedDto(appraisalTrainingNeed);
                });

                _this.sendSuccessResponse(res, 'Success', appraisalTrainingNeedsDto);

                _context14.next = 13;
                break;

              case 10:
                _context14.prev = 10;
                _context14.t0 = _context14["catch"](0);

                _this.catchControllerErrors(_context14.t0, next);

              case 13:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14, this, [[0, 10]]);
      }));

      return function (_x40, _x41, _x42) {
        return _ref14.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createPerformanceAppraisalAdditionalAssignment",
    /*#__PURE__*/
    function () {
      var _ref15 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee15(req, res, next) {
        var description, performanceAppraisalId, performanceAdditionalAssignment;
        return _regenerator.default.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _context15.prev = 0;

                _this.validateRequest(req);

                description = req.body.description;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context15.next = 6;
                return PerformanceAppraisalAdditionalAssignment.create({
                  description: description,
                  performanceAppraisalId: performanceAppraisalId
                });

              case 6:
                performanceAdditionalAssignment = _context15.sent;

                _this.sendSuccessResponse(res, 'Additional assignment created successfully.', _dto.default.getPerformanceAppraisalAdditionalAssignmentDto(performanceAdditionalAssignment));

                _context15.next = 13;
                break;

              case 10:
                _context15.prev = 10;
                _context15.t0 = _context15["catch"](0);

                _this.catchControllerErrors(_context15.t0, next);

              case 13:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15, this, [[0, 10]]);
      }));

      return function (_x43, _x44, _x45) {
        return _ref15.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updatePerformanceAppraisalAdditionalAssignment",
    /*#__PURE__*/
    function () {
      var _ref16 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee16(req, res, next) {
        var id, description, performanceAppraisalId, performanceAppraisalAdditionalAssignment, updatedPerformanceAppraisalAdditionalAssignment;
        return _regenerator.default.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                _context16.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                description = req.body.description;
                performanceAppraisalId = req.body.performanceAppraisalId;
                _context16.next = 7;
                return PerformanceAppraisalAdditionalAssignment.findOne({
                  where: {
                    id: id,
                    performanceAppraisalId: performanceAppraisalId
                  }
                });

              case 7:
                performanceAppraisalAdditionalAssignment = _context16.sent;

                if (!performanceAppraisalAdditionalAssignment) {
                  _this.throwError('Additional assignment need not found.', 404);
                }

                _context16.next = 11;
                return performanceAppraisalAdditionalAssignment.update({
                  description: description
                });

              case 11:
                updatedPerformanceAppraisalAdditionalAssignment = _context16.sent;

                _this.sendSuccessResponse(res, 'Additional assignment updated successfully.', _dto.default.getPerformanceAppraisalAdditionalAssignmentDto(updatedPerformanceAppraisalAdditionalAssignment));

                _context16.next = 18;
                break;

              case 15:
                _context16.prev = 15;
                _context16.t0 = _context16["catch"](0);

                _this.catchControllerErrors(_context16.t0, next);

              case 18:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16, this, [[0, 15]]);
      }));

      return function (_x46, _x47, _x48) {
        return _ref16.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "deletePerformanceAppraisalAdditionalAssignment",
    /*#__PURE__*/
    function () {
      var _ref17 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee17(req, res, next) {
        var id, performanceAppraisalAdditionalAssignment;
        return _regenerator.default.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                _context17.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context17.next = 5;
                return PerformanceAppraisalAdditionalAssignment.findByPk(id);

              case 5:
                performanceAppraisalAdditionalAssignment = _context17.sent;

                if (!performanceAppraisalAdditionalAssignment) {
                  _this.throwError('Additional assignment not found', 404);
                }

                _context17.next = 9;
                return performanceAppraisalAdditionalAssignment.destroy();

              case 9:
                _this.sendSuccessResponse(res, 'Additional assignment deleted successfully.', {
                  message: 'Additional assignment deleted successfully.'
                });

                _context17.next = 15;
                break;

              case 12:
                _context17.prev = 12;
                _context17.t0 = _context17["catch"](0);

                _this.catchControllerErrors(_context17.t0, next);

              case 15:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17, this, [[0, 12]]);
      }));

      return function (_x49, _x50, _x51) {
        return _ref17.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalAdditionalAssignment",
    /*#__PURE__*/
    function () {
      var _ref18 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee18(req, res, next) {
        var id, appraisaAdditionalAssignment, appraisaAdditionalAssignmentDto;
        return _regenerator.default.wrap(function _callee18$(_context18) {
          while (1) {
            switch (_context18.prev = _context18.next) {
              case 0:
                _context18.prev = 0;

                _this.validateRequest(req);

                id = req.body.id;
                _context18.next = 5;
                return PerformanceAppraisalAdditionalAssignment.findByPk(id);

              case 5:
                appraisaAdditionalAssignment = _context18.sent;

                if (!appraisaAdditionalAssignment) {
                  _this.throwError('Additional assignment not found.', 404);
                }

                appraisaAdditionalAssignmentDto = _dto.default.getPerformanceAppraisalAdditionalAssignmentDto(appraisaAdditionalAssignment);

                _this.sendSuccessResponse(res, 'Success', appraisaAdditionalAssignmentDto);

                _context18.next = 14;
                break;

              case 11:
                _context18.prev = 11;
                _context18.t0 = _context18["catch"](0);

                _this.catchControllerErrors(_context18.t0, next);

              case 14:
              case "end":
                return _context18.stop();
            }
          }
        }, _callee18, this, [[0, 11]]);
      }));

      return function (_x52, _x53, _x54) {
        return _ref18.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getPerformanceAppraisalAdditionalAssignments",
    /*#__PURE__*/
    function () {
      var _ref19 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee19(req, res, next) {
        var appraisalId, appraisalAdditionalAssignments, appraisalAdditionalAssignmentsDto;
        return _regenerator.default.wrap(function _callee19$(_context19) {
          while (1) {
            switch (_context19.prev = _context19.next) {
              case 0:
                _context19.prev = 0;

                _this.validateRequest(req);

                appraisalId = req.body.appraisalId;
                _context19.next = 5;
                return PerformanceAppraisalAdditionalAssignment.findAll({
                  order: [['createdAt', 'ASC']],
                  where: {
                    performanceAppraisalId: appraisalId
                  }
                });

              case 5:
                appraisalAdditionalAssignments = _context19.sent;
                appraisalAdditionalAssignmentsDto = appraisalAdditionalAssignments.map(function (appraisalAdditionalAssignment) {
                  return _dto.default.getPerformanceAppraisalAdditionalAssignmentDto(appraisalAdditionalAssignment);
                });

                _this.sendSuccessResponse(res, 'Success', appraisalAdditionalAssignmentsDto);

                _context19.next = 13;
                break;

              case 10:
                _context19.prev = 10;
                _context19.t0 = _context19["catch"](0);

                _this.catchControllerErrors(_context19.t0, next);

              case 13:
              case "end":
                return _context19.stop();
            }
          }
        }, _callee19, this, [[0, 10]]);
      }));

      return function (_x55, _x56, _x57) {
        return _ref19.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), _validateAppraisal,
    /*#__PURE__*/
    function () {
      var _ref20 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee20(employeeId, startDate, endDate) {
        var employee, performanceAppraisal;
        return _regenerator.default.wrap(function _callee20$(_context20) {
          while (1) {
            switch (_context20.prev = _context20.next) {
              case 0:
                _context20.next = 2;
                return User.findOne({
                  where: {
                    id: employeeId
                  }
                });

              case 2:
                employee = _context20.sent;

                if (!employee) {
                  _this.throwError('Employee not found.', 404);
                } // Ensure there's no appraisal for this employee in this period


                _context20.next = 6;
                return PerformanceAppraisal.findOne({
                  where: (0, _defineProperty2.default)({
                    employeeId: employeeId
                  }, Op.or, [{
                    startDate: (0, _defineProperty2.default)({}, Op.between, [startDate, endDate])
                  }, {
                    endDate: (0, _defineProperty2.default)({}, Op.between, [startDate, endDate])
                  }, (0, _defineProperty2.default)({}, Op.and, {
                    startDate: (0, _defineProperty2.default)({}, Op.lte, startDate),
                    endDate: (0, _defineProperty2.default)({}, Op.gt, startDate)
                  })])
                });

              case 6:
                performanceAppraisal = _context20.sent;

                if (performanceAppraisal) {
                  _this.throwError('The employee already has an appraisal for the period specified.', 422);
                }

              case 8:
              case "end":
                return _context20.stop();
            }
          }
        }, _callee20, this);
      }));

      return function (_x58, _x59, _x60) {
        return _ref20.apply(this, arguments);
      };
    }());
    return _this;
  }

  return PerformanceController;
}(_baseController.default);

var _default = new PerformanceController();

exports.default = _default;