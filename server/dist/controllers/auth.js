"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _baseController = _interopRequireDefault(require("./baseController"));

var _appConstants = _interopRequireDefault(require("../utils/appConstants"));

var _index = _interopRequireDefault(require("../models/index"));

var _roles = _interopRequireDefault(require("../utils/enums/roles"));

var _dto = _interopRequireDefault(require("../factories/dto"));

var Op = _index.default.Sequelize.Op;
var User = _index.default['User'];
var Role = _index.default['Role'];
var Gender = _index.default['Gender'];
var JobGroup = _index.default['JobGroup'];
var EmploymentType = _index.default['EmploymentType'];
var Designation = _index.default['Designation'];

var _getUser = Symbol('_getUser');

var AuthController =
/*#__PURE__*/
function (_BaseController) {
  (0, _inherits2.default)(AuthController, _BaseController);

  function AuthController() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, AuthController);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(AuthController)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "login",
    /*#__PURE__*/
    function () {
      var _ref = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(req, res, next) {
        var username, password, user, error, passwordsMatch, _error, token, refreshToken;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                _this.validateRequest(req);

                username = req.body.username;
                password = req.body.password;
                _context.next = 6;
                return User.findOne({
                  include: [Role],
                  where: (0, _defineProperty2.default)({}, Op.or, [{
                    username: username
                  }, {
                    email: username
                  }])
                });

              case 6:
                user = _context.sent;

                if (user) {
                  _context.next = 11;
                  break;
                }

                error = new Error('Invalid username or password.');
                error.statusCode = 422;
                throw error;

              case 11:
                _context.next = 13;
                return _bcryptjs.default.compare(password, user.password);

              case 13:
                passwordsMatch = _context.sent;

                if (passwordsMatch) {
                  _context.next = 18;
                  break;
                }

                _error = new Error('Invalid username or password.');
                _error.statusCode = 422;
                throw _error;

              case 18:
                token = _jsonwebtoken.default.sign({
                  userId: user.id
                }, _appConstants.default.tokenSecret, {
                  expiresIn: _appConstants.default.tokenLife
                });
                refreshToken = _jsonwebtoken.default.sign({
                  userId: user.id
                }, _appConstants.default.refreshTokenSecret, {
                  expiresIn: _appConstants.default.refreshTokenLife
                });

                _this.sendSuccessResponse(res, "Login success.", {
                  token: token,
                  refreshToken: refreshToken,
                  user: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    roles: user.Roles.map(function (role) {
                      return role.name;
                    })
                  }
                });

                _context.next = 26;
                break;

              case 23:
                _context.prev = 23;
                _context.t0 = _context["catch"](0);

                _this.catchControllerErrors(_context.t0, next);

              case 26:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 23]]);
      }));

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "refreshToken",
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(req, res, next) {
        var refreshToken, decodedRefreshToken, newToken;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                try {
                  _this.validateRequest(req);

                  refreshToken = req.body.refreshToken; // verify refresh token

                  decodedRefreshToken = _jsonwebtoken.default.verify(refreshToken, _appConstants.default.refreshTokenSecret);
                  newToken = _jsonwebtoken.default.sign({
                    userId: decodedRefreshToken.userId
                  }, _appConstants.default.tokenSecret, {
                    expiresIn: _appConstants.default.tokenLife
                  });

                  _this.sendSuccessResponse(res, 'Refresh token success.', {
                    token: newToken
                  });
                } catch (error) {
                  error.statusCode = 401;

                  _this.catchControllerErrors(error, next);
                }

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function (_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "createUser",
    /*#__PURE__*/
    function () {
      var _ref3 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(req, res, next) {
        var body, hashedPassword, isManager, isAdmin, gender, createdUser, roles, userRoles, employeeRole, adminRole, managerRole, userDto;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                _this.validateRequest(req);

                body = req.body;
                _context3.next = 5;
                return _bcryptjs.default.hash(body.password, _appConstants.default.passwordHashSalt);

              case 5:
                hashedPassword = _context3.sent;
                isManager = req.body.isManager || false;
                isAdmin = req.body.isAdmin || false;
                _context3.next = 10;
                return Gender.findOne({
                  where: {
                    code: body.gender.toUpperCase()
                  }
                });

              case 10:
                gender = _context3.sent;
                _context3.next = 13;
                return User.create({
                  username: body.username,
                  password: hashedPassword,
                  firstName: body.firstName,
                  middleName: body.middleName,
                  lastName: body.lastName,
                  email: body.email,
                  phoneNumber: body.phoneNumber,
                  jobGroupId: body.jobGroupId,
                  designationId: body.designationId,
                  ministryId: body.ministryId,
                  departmentId: body.departmentId,
                  sectionId: body.sectionId,
                  dutyStationId: body.dutyStationId,
                  managerId: body.managerId,
                  employmentTypeId: body.employmentTypeId,
                  genderId: gender.id
                });

              case 13:
                createdUser = _context3.sent;
                _context3.next = 16;
                return Role.findAll();

              case 16:
                roles = _context3.sent;
                userRoles = [];
                employeeRole = roles.find(function (role) {
                  return role.name === _roles.default.Employee;
                });
                adminRole = roles.find(function (role) {
                  return role.name === _roles.default.Administrator;
                });
                managerRole = roles.find(function (role) {
                  return role.name === _roles.default.Manager;
                });
                userRoles.push(employeeRole);

                if (isManager) {
                  userRoles.push(managerRole);
                }

                if (isAdmin) {
                  userRoles.push(adminRole);
                }

                _context3.next = 26;
                return createdUser.addRoles(userRoles);

              case 26:
                _context3.next = 28;
                return _this[_getUser](createdUser.id);

              case 28:
                userDto = _context3.sent;

                _this.sendSuccessResponse(res, 'User created successfully.', userDto);

                _context3.next = 35;
                break;

              case 32:
                _context3.prev = 32;
                _context3.t0 = _context3["catch"](0);

                _this.catchControllerErrors(_context3.t0, next);

              case 35:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 32]]);
      }));

      return function (_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "updateUser",
    /*#__PURE__*/
    function () {
      var _ref4 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(req, res, next) {
        var body, isManager, isAdmin, gender, user, updatedUser, roles, userRoles, employeeRole, adminRole, managerRole, userDto;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;

                _this.validateRequest(req);

                body = req.body;
                isManager = req.body.isManager || false;
                isAdmin = req.body.isAdmin || false;
                _context4.next = 7;
                return Gender.findOne({
                  where: {
                    code: body.gender.toUpperCase()
                  }
                });

              case 7:
                gender = _context4.sent;
                _context4.next = 10;
                return User.findByPk(body.id, {
                  include: Gender
                });

              case 10:
                user = _context4.sent;
                _context4.next = 13;
                return user.update({
                  username: body.username,
                  firstName: body.firstName,
                  middleName: body.middleName,
                  lastName: body.lastName,
                  email: body.email,
                  phoneNumber: body.phoneNumber,
                  jobGroupId: body.jobGroupId,
                  designationId: body.designationId,
                  ministryId: body.ministryId,
                  departmentId: body.departmentId,
                  sectionId: body.sectionId,
                  dutyStationId: body.dutyStationId,
                  managerId: body.managerId,
                  employmentTypeId: body.employmentTypeId,
                  genderId: gender.id
                });

              case 13:
                updatedUser = _context4.sent;
                _context4.next = 16;
                return Role.findAll();

              case 16:
                roles = _context4.sent;
                userRoles = [];
                employeeRole = roles.find(function (role) {
                  return role.name === _roles.default.Employee;
                });
                adminRole = roles.find(function (role) {
                  return role.name === _roles.default.Administrator;
                });
                managerRole = roles.find(function (role) {
                  return role.name === _roles.default.Manager;
                });
                userRoles.push(employeeRole);

                if (isManager) {
                  userRoles.push(managerRole);
                }

                if (isAdmin) {
                  userRoles.push(adminRole);
                }

                _context4.next = 26;
                return updatedUser.setRoles(userRoles);

              case 26:
                _context4.next = 28;
                return _this[_getUser](updatedUser.id);

              case 28:
                userDto = _context4.sent;

                _this.sendSuccessResponse(res, 'User updated successfully.', userDto);

                _context4.next = 35;
                break;

              case 32:
                _context4.prev = 32;
                _context4.t0 = _context4["catch"](0);

                _this.catchControllerErrors(_context4.t0, next);

              case 35:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 32]]);
      }));

      return function (_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getUser",
    /*#__PURE__*/
    function () {
      var _ref5 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(req, res, next) {
        var user;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;

                _this.validateRequest(req);

                _context5.next = 4;
                return _this[_getUser](req.body.id);

              case 4:
                user = _context5.sent;

                _this.sendSuccessResponse(res, 'Success', user);

                _context5.next = 11;
                break;

              case 8:
                _context5.prev = 8;
                _context5.t0 = _context5["catch"](0);

                _this.catchControllerErrors(_context5.t0, next);

              case 11:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 8]]);
      }));

      return function (_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), "getUsers",
    /*#__PURE__*/
    function () {
      var _ref6 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(req, res, next) {
        var users, userDtos;
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return User.findAll({
                  include: [Designation, EmploymentType, Gender, JobGroup, Role]
                });

              case 3:
                users = _context6.sent;
                userDtos = users.map(function (user) {
                  return _dto.default.getUserDto(user);
                });

                _this.sendSuccessResponse(res, 'Success', userDtos);

                _context6.next = 11;
                break;

              case 8:
                _context6.prev = 8;
                _context6.t0 = _context6["catch"](0);

                _this.catchControllerErrors(_context6.t0, next);

              case 11:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 8]]);
      }));

      return function (_x16, _x17, _x18) {
        return _ref6.apply(this, arguments);
      };
    }());
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)), _getUser,
    /*#__PURE__*/
    function () {
      var _ref7 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee7(userId) {
        var user, userDto;
        return _regenerator.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return User.findByPk(userId, {
                  include: [Gender, Role, EmploymentType]
                });

              case 2:
                user = _context7.sent;

                if (!user) {
                  _this.throwError('User not found', 404);
                }

                userDto = _dto.default.getUserDto(user);
                return _context7.abrupt("return", userDto);

              case 6:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      return function (_x19) {
        return _ref7.apply(this, arguments);
      };
    }());
    return _this;
  }

  return AuthController;
}(_baseController.default);

var _default = new AuthController();

exports.default = _default;