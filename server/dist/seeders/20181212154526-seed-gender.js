'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var uuidV4 = require('uuid/v4');

module.exports = {
  up: function () {
    var _up = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(queryInterface, Sequelize) {
      var genderCodes, sequelize, currentGenders, genders;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              genderCodes = [['M', 'Male'], ['F', 'Female']];
              sequelize = queryInterface.sequelize;
              _context.next = 4;
              return sequelize.query('SELECT * FROM genders', {
                type: sequelize.QueryTypes.SELECT
              });

            case 4:
              currentGenders = _context.sent;
              genders = [];
              genderCodes.forEach(function (genderCode) {
                if (!currentGenders.find(function (gender) {
                  return gender.code === genderCode[0];
                })) {
                  genders.push({
                    id: uuidV4(),
                    code: genderCode[0],
                    description: genderCode[1],
                    createdAt: new Date(),
                    updatedAt: new Date()
                  });
                }
              });

              if (!(genders.length > 0)) {
                _context.next = 9;
                break;
              }

              return _context.abrupt("return", queryInterface.bulkInsert('Genders', genders, {}));

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Genders', null, {});
  }
};