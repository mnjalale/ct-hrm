'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var uuidV4 = require('uuid/v4');

module.exports = {
  up: function () {
    var _up = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(queryInterface, Sequelize) {
      var sequelize, currentRatingScales, ratings, ratingScales;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              sequelize = queryInterface.sequelize;
              _context.next = 3;
              return sequelize.query('SELECT * FROM RatingScales', {
                type: sequelize.QueryTypes.SELECT
              });

            case 3:
              currentRatingScales = _context.sent;
              ratings = [];
              ratingScales = [{
                rating: 'Excellent',
                description: 'Achievement higher than 100% of the agreen performance targets.',
                fromScore: 101,
                toScore: 1000
              }, {
                rating: 'Very Good',
                description: 'Achievement up to 100% of the agreen performance targets.',
                fromScore: 100,
                toScore: 100
              }, {
                rating: 'Good',
                description: 'Achievement between 80% and 99% of the agreen performance targets.',
                fromScore: 80,
                toScore: 99
              }, {
                rating: 'Fair',
                description: 'Achievement between 60% and 79% of the agreen performance targets.',
                fromScore: 60,
                toScore: 79
              }, {
                rating: 'Poor',
                description: 'Achievement up to 59% of the agreen performance targets.',
                fromScore: 59,
                toScore: 0
              }];
              ratingScales.forEach(function (ratingScale) {
                if (!currentRatingScales.find(function (currentRating) {
                  return currentRating.rating === ratingScale.rating;
                })) {
                  ratings.push({
                    id: uuidV4(),
                    rating: ratingScale.rating,
                    description: ratingScale.description,
                    fromScore: ratingScale.fromScore,
                    toScore: ratingScale.toScore,
                    createdAt: new Date(),
                    updatedAt: new Date()
                  });
                }
              });

              if (!(ratings.length > 0)) {
                _context.next = 9;
                break;
              }

              return _context.abrupt("return", queryInterface.bulkInsert('RatingScales', ratings, {}));

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('RatingScales', null, {});
  }
};