'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var uuidV4 = require('uuid/v4');

var bcrypt = require('bcryptjs');

module.exports = {
  up: function () {
    var _up = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(queryInterface, Sequelize) {
      var sequelize, currentUsers, currentRoles, adminRoleId, hashedPassword, userId, user;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              sequelize = queryInterface.sequelize;
              _context.next = 3;
              return sequelize.query("SELECT * FROM Users where username='admin'", {
                type: sequelize.QueryTypes.SELECT
              });

            case 3:
              currentUsers = _context.sent;

              if (!(currentUsers.length > 0)) {
                _context.next = 6;
                break;
              }

              return _context.abrupt("return");

            case 6:
              _context.next = 8;
              return sequelize.query("SELECT * FROM Roles where name='Administrator'", {
                type: sequelize.QueryTypes.SELECT
              });

            case 8:
              currentRoles = _context.sent;

              if (!(currentRoles.length === 0)) {
                _context.next = 11;
                break;
              }

              return _context.abrupt("return");

            case 11:
              adminRoleId = currentRoles[0].id;
              _context.next = 14;
              return bcrypt.hash('12345678', 12);

            case 14:
              hashedPassword = _context.sent;
              userId = uuidV4();
              user = {
                id: userId,
                username: 'admin',
                password: hashedPassword,
                firstName: 'System',
                lastName: 'Administrator',
                createdAt: new Date(),
                updatedAt: new Date()
              };
              return _context.abrupt("return", queryInterface.bulkInsert('Users', [user]).then(function (result) {
                var userRole = {
                  id: uuidV4(),
                  userId: userId,
                  roleId: adminRoleId,
                  createdAt: new Date(),
                  updatedAt: new Date()
                };
                return queryInterface.bulkInsert('UserRoles', [userRole]);
              }));

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(queryInterface, Sequelize) {
      var sequelize, currentUsers, adminUserId;
      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              sequelize = queryInterface.sequelize;
              _context2.next = 3;
              return sequelize.query("SELECT * FROM Users where username='admin'", {
                type: sequelize.QueryTypes.SELECT
              });

            case 3:
              currentUsers = _context2.sent;

              if (!(currentUsers.length === 0)) {
                _context2.next = 6;
                break;
              }

              return _context2.abrupt("return");

            case 6:
              adminUserId = currentUsers[0].id;
              return _context2.abrupt("return", queryInterface.bulkDelete('UserRoles', {
                userId: adminUserId
              }, {}).then(function (result) {
                return queryInterface.bulkDelete('Users', {
                  id: adminUserId
                }, {});
              }));

            case 8:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};