'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var uuidV4 = require('uuid/v4');

module.exports = {
  up: function () {
    var _up = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(queryInterface, Sequelize) {
      var sequelize, currentJobGroups, oldJobGroupCodes, newJobGroupCodes, jobGroups;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              sequelize = queryInterface.sequelize;
              _context.next = 3;
              return sequelize.query('SELECT * FROM JobGroups', {
                type: sequelize.QueryTypes.SELECT
              });

            case 3:
              currentJobGroups = _context.sent;
              oldJobGroupCodes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T'];
              newJobGroupCodes = ['B1', 'B2', 'B3', 'B4', 'B5', 'C1', 'C2', 'C3', 'C4', 'C5', 'D1', 'D2', 'D3', 'D4', 'D5', 'E1', 'E2', 'E3'];
              jobGroups = [];
              newJobGroupCodes.forEach(function (jobGroupCode, index) {
                if (!currentJobGroups.find(function (jobGroup) {
                  return jobGroup.code === jobGroupCode;
                })) {
                  jobGroups.push({
                    id: uuidV4(),
                    code: jobGroupCode,
                    alternativeCode: oldJobGroupCodes[index],
                    description: 'Job Group ' + jobGroupCode,
                    alternativeDescription: 'Job Group ' + oldJobGroupCodes[index],
                    createdAt: new Date(),
                    updatedAt: new Date()
                  });
                }
              });

              if (!(jobGroups.length > 0)) {
                _context.next = 10;
                break;
              }

              return _context.abrupt("return", queryInterface.bulkInsert('JobGroups', jobGroups, {}));

            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('JobGroups', null, {});
  }
};