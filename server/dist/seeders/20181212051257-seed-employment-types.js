'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var uuidV4 = require('uuid/v4');

module.exports = {
  up: function () {
    var _up = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(queryInterface, Sequelize) {
      var sequelize, currentEmploymentTypes, employmentTypeNames, employmentTypes;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              sequelize = queryInterface.sequelize;
              _context.next = 3;
              return sequelize.query('SELECT * FROM EmploymentTypes', {
                type: sequelize.QueryTypes.SELECT
              });

            case 3:
              currentEmploymentTypes = _context.sent;
              employmentTypeNames = ['Contract', 'Permanent'];
              employmentTypes = [];
              employmentTypeNames.forEach(function (name) {
                if (!currentEmploymentTypes.find(function (employmentType) {
                  return employmentType.name === name;
                })) {
                  employmentTypes.push({
                    id: uuidV4(),
                    name: name,
                    createdAt: new Date(),
                    updatedAt: new Date()
                  });
                }
              });

              if (!(employmentTypes.length > 0)) {
                _context.next = 9;
                break;
              }

              return _context.abrupt("return", queryInterface.bulkInsert('EmploymentTypes', employmentTypes, {}));

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('EmploymentTypes', null, {});
  }
};