"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var bodyParser = _interopRequireWildcard(require("body-parser"));

var _auth = _interopRequireDefault(require("./routes/auth"));

var _lookup = _interopRequireDefault(require("./routes/lookup"));

var _employees = _interopRequireDefault(require("./routes/employees"));

var _performance = _interopRequireDefault(require("./routes/performance"));

var _departments = _interopRequireDefault(require("./routes/departments"));

var _designations = _interopRequireDefault(require("./routes/designations"));

var _dutyStations = _interopRequireDefault(require("./routes/dutyStations"));

var _employmentTypes = _interopRequireDefault(require("./routes/employmentTypes"));

var _ministries = _interopRequireDefault(require("./routes/ministries"));

var _sections = _interopRequireDefault(require("./routes/sections"));

var _config = Symbol('config'); // Use symbols to define private methods


var App =
/*#__PURE__*/
function () {
  function App() {
    (0, _classCallCheck2.default)(this, App);
    this.app = (0, _express.default)();

    this[_config]();
  }

  (0, _createClass2.default)(App, [{
    key: _config,
    value: function value() {
      // Enable CORS
      this.app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        next();
      }); // Body Parser

      this.app.use(bodyParser.json());
      this.app.use(bodyParser.urlencoded({
        extended: false
      })); // Routes

      this.app.use('/api/auth', _auth.default);
      this.app.use('/api/lookup', _lookup.default);
      this.app.use('/api/employees', _employees.default);
      this.app.use('/api/performance', _performance.default);
      this.app.use('/api/employmentTypes', _employmentTypes.default);
      this.app.use('/api/designations', _designations.default);
      this.app.use('/api/departments', _departments.default);
      this.app.use('/api/dutyStations', _dutyStations.default);
      this.app.use('/api/ministries', _ministries.default);
      this.app.use('/api/sections', _sections.default); // Error Handling

      this.app.use(function (error, req, res, next) {
        var status = error.statusCode || 500;
        var message = error.message;
        var errors = error.errors;
        res.status(status).json({
          message: message,
          statusCode: status,
          errors: errors
        });
      });
    }
  }]);
  return App;
}();

var _default = new App().app;
exports.default = _default;