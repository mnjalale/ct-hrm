'use strict';

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.createTable('PerformanceAppraisalTargets', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      performanceTarget: {
        type: Sequelize.STRING,
        allowNull: false
      },
      performanceIndicator: {
        type: Sequelize.STRING,
        allowNull: false
      },
      midYearReviewRemarks: {
        type: Sequelize.STRING
      },
      updatedPerformanceTarget: {
        type: Sequelize.STRING
      },
      achievedResult: {
        type: Sequelize.STRING
      },
      performanceAppraisalScore: {
        type: Sequelize.DECIMAL
      },
      performanceAppraisalId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'PerformanceAppraisals',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.dropTable('PerformanceAppraisalTargets');
  }
};