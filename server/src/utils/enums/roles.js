var Roles = {
  Administrator: 'Administrator',
  Support: 'Support',
  Employee: 'Employee',
  Manager: 'Manager'
};

module.exports = Roles;

