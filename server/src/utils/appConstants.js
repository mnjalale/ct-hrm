class AppConstants {
  static passwordHashSalt = 12;
  static tokenSecret = 'apptokensecret';
  static tokenLife =(10 * 60); //10 minutes
  static refreshTokenSecret = 'apprefreshtokensecret';
  static refreshTokenLife = (24 * 60 * 60); // 24 hours
}

export default AppConstants;