import BaseController from './baseController'
import db from '../models/index';
import Roles from '../utils/enums/roles';

const User = db['User'];
const Role = db['Role'];
const JobGroup = db['JobGroup'];
const EmploymentType = db['EmploymentType'];

class LookupController extends BaseController {


  getJobGroups = async (req, res, next) => {
    try {
      const jobGroups = await JobGroup.findAll({
        order: [['code', 'ASC']]
      });

      this.sendSuccessResponse(res, "Success", jobGroups);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Managers
  getManagers = async (req, res, next) => {
    try {
      const managers = await User.findAll({
        include: {
          model: Role,
          required: true,
          where: {
            name: Roles.Manager
          }
        },
        order: [['firstName', 'ASC']]
      });

      const returnValue = managers.map(manager => {
        return {
          id: manager.id,
          firstName: manager.firstName,
          middleName: manager.middleName,
          lastName: manager.lastName,
          fullName: manager.firstName + ' ' + manager.lastName
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }


}

export default new LookupController();
