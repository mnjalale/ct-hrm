import BaseController from './baseController'
import db from '../models/index';

const Section = db['Section'];
const Op = db.Sequelize.Op;

class SectionsController extends BaseController {

  createSection = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const departmentId = req.body.departmentId;
      const name = req.body.name;

      // Ensure a similar section hasn't been entered for the same department
      const existingSection = await Section.findOne({
        where: {
          departmentId: departmentId,
          name: name
        }
      });

      if (existingSection) {
        this.throwError('A section with a similar name already exists within the department.', 422);
      }

      const section = await Section.create({
        departmentId: departmentId,
        name: name
      });

      const returnValue = {
        id: section.id,
        departmentId: section.departmentId,
        name: section.name
      };

      this.sendSuccessResponse(res, "Section created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateSection = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;
      const departmentId = req.body.departmentId;

      // Ensure a similar section hasn't been entered
      const existingSection = await Section.findOne({
        where: {
          id: {
            [Op.ne]: id
          },
          departmentId: departmentId,
          name: name
        }
      });

      if (existingSection) {
        this.throwError('A section with a similar name already exists within the department.', 422);
      }

      const section = await Section.findByPk(id);
      const updatedSection = await section.update({
        name: name,
        departmentId: departmentId
      });

      const returnValue = {
        id: updatedSection.id,
        name: updatedSection.name,
        departmentId: updatedSection.departmentId
      };

      this.sendSuccessResponse(res, "Section updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteSection= async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const section = await Section.findByPk(id);
      await section.destroy();
      this.sendSuccessResponse(res, "Section deleted successfully", {
        message: 'Section deleted successfully"'
      });
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getSection = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const sectionId = req.body.id;

      const section = await Section.findOne({
        where: {
          id: sectionId
        }
      });

      const returnValue = {
        id: section.id,
        name: section.name,
        departmentId: section.departmentId
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getSections = async (req, res, next) => {
    try {

      const departmentId = req.body.departmentId;
      let sections;

      if (departmentId) {
        sections = await Section.findAll({
          where: {
            departmentId: departmentId
          },
          order: [['name', 'ASC']]
        });
      } else {
        sections = await Section.findAll({
          order: [['name', 'ASC']]
        });
      }

      const returnValue = sections.map(section => {
        return {
          id: section.id,
          name: section.name,
          departmentId: section.departmentId
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }
}

export default new SectionsController();
