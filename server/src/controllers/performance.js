import BaseController from "./baseController";
import db from '../models/index';
import dtoFactory from '../factories/dto';
import modelFactory from '../factories/model'

const Op = db.Sequelize.Op;

const User = db['User'];
const Gender = db['Gender'];
const JobGroup = db['JobGroup'];
const EmploymentType = db['EmploymentType'];
const Designation = db['Designation'];
const PerformanceAppraisal = db['PerformanceAppraisal'];
const PerformanceAppraisalTarget = db['PerformanceAppraisalTarget'];
const PerformanceAppraisalTrainingNeed = db['PerformanceAppraisalTrainingNeed'];
const PerformanceAppraisalAdditionalAssignment = db['PerformanceAppraisalAdditionalAssignment'];

const _validateAppraisal = Symbol('_validateAppraisal');

class PerformanceController extends BaseController {

  createAppraisal = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const employeeId = req.body.employeeId;
      const startDate = new Date(req.body.startDate);
      const midYearReviewDate = new Date(req.body.midYearReviewDate);
      const endDate = new Date(req.body.endDate);

      await this[_validateAppraisal](employeeId, startDate, endDate);

      const appraisal = await PerformanceAppraisal.create({
        employeeId: employeeId,
        startDate: startDate,
        midYearReviewDate: midYearReviewDate,
        endDate: endDate
      });

      this.sendSuccessResponse(res, 'Performance appraisal created successfully.', {
        employeeId: appraisal.employeeId,
        startDate: appraisal.startDate,
        midYearReviewDate: appraisal.midYearReviewDate,
        endDate: appraisal.endDate
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateAppraisal = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const model = modelFactory.getPerformanceAppraisalModel(req.body);
      const appraisalToUpdate = await PerformanceAppraisal.findOne({
        include: [PerformanceAppraisalAdditionalAssignment, PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, User],
        where: {
          id: req.body.id
        }
      });

      const updatedAppraisal = await appraisalToUpdate.update(model);

      this.sendSuccessResponse(res, 'Performance appraisal updated successfully.', dtoFactory.getPerformanceAppraisalDto(updatedAppraisal));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisal = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const appraisalId = req.body.appraisalId;

      const appraisal = await PerformanceAppraisal.findByPk(appraisalId, {
        include: [PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, PerformanceAppraisalAdditionalAssignment]
      });
      const appraisalDto = dtoFactory.getPerformanceAppraisalDto(appraisal);
      const employee = await User.findByPk(appraisal.employeeId, {
        include: [JobGroup, Designation, EmploymentType, Gender]
      });
      const employeeDto = dtoFactory.getUserDto(employee);
      const manager = await User.findByPk(employee.managerId, {
        include: [JobGroup, Designation, EmploymentType, Gender]
      });
      const managerDto = dtoFactory.getUserDto(manager);
      employeeDto.manager = managerDto;

      appraisalDto.employee = employeeDto;

      this.sendSuccessResponse(res, 'Success', appraisalDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getEmployeeAppraisals = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const employeeId = req.body.employeeId;

      const appraisals = await PerformanceAppraisal.findAll({
        include: [PerformanceAppraisalTarget, PerformanceAppraisalTrainingNeed, PerformanceAppraisalAdditionalAssignment],
        order: [['startDate', 'DESC']],
        where: {
          employeeId: employeeId
        }
      });

      this.sendSuccessResponse(res, 'Success', appraisals);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Performance appraisal targets
  createPerformanceAppraisalTarget = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const performanceTarget = req.body.performanceTarget;
      const performanceIndicator = req.body.performanceIndicator;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      const performanceAppraisalTarget = await PerformanceAppraisalTarget.create({
        performanceTarget: performanceTarget,
        performanceIndicator: performanceIndicator,
        performanceAppraisalId: performanceAppraisalId
      });

      this.sendSuccessResponse(res, 'Performance target created successfully.', dtoFactory.getPerformanceAppraisalTargetDto(performanceAppraisalTarget));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updatePerformanceAppraisalTarget = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const performanceTarget = req.body.performanceTarget;
      const performanceIndicator = req.body.performanceIndicator;
      const midYearReviewRemarks = req.body.midYearReviewRemarks;
      const updatedPerformanceTarget = req.body.updatedPerformanceTarget;
      const achievedResult = req.body.achievedResult;
      const performanceAppraisalScore = req.body.performanceAppraisalScore;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      let performanceAppraisalTarget = await PerformanceAppraisalTarget.findOne({
        where: {
          id: id,
          performanceAppraisalId: performanceAppraisalId
        }
      });

      if (!performanceAppraisalTarget) {
        this.throwError('Performance Target not found.', 404);
      }

      const updatedPerformanceAppraisalTarget = await performanceAppraisalTarget.update({
        performanceTarget: performanceTarget,
        performanceIndicator: performanceIndicator,
        midYearReviewRemarks: midYearReviewRemarks,
        updatedPerformanceTarget: updatedPerformanceTarget,
        achievedResult: achievedResult,
        performanceAppraisalScore: performanceAppraisalScore
      });

      this.sendSuccessResponse(res, 'Performance target updated successfully.', dtoFactory.getPerformanceAppraisalTargetDto(updatedPerformanceAppraisalTarget));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deletePerformanceAppraisalTarget = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const performanceAppraisalTarget = await PerformanceAppraisalTarget.findByPk(id);

      if (!performanceAppraisalTarget) {
        this.throwError('Performance Appraisal Target not found', 404);
      }

      await performanceAppraisalTarget.destroy();

      this.sendSuccessResponse(res, 'Performance target deleted successfully.', {
        message: 'Performance target deleted successfully.'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalTarget = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const id = req.body.id;

      const appraisalTarget = await PerformanceAppraisalTarget.findByPk(id);

      if (!appraisalTarget) {
        this.throwError('Performance Target not found', 404);
      }

      const appraisalTargetDto = dtoFactory.getPerformanceAppraisalTargetDto(appraisalTarget);

      this.sendSuccessResponse(res, 'Success', appraisalTargetDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalTargets = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const appraisalId = req.body.appraisalId;

      const appraisalTargets = await PerformanceAppraisalTarget.findAll({
        order: [['createdAt', 'ASC']],
        where: {
          performanceAppraisalId: appraisalId
        }
      });

      const appraisalTargetsDto = appraisalTargets.map(appraisalTarget => dtoFactory.getPerformanceAppraisalTargetDto(appraisalTarget));

      this.sendSuccessResponse(res, 'Success', appraisalTargetsDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Performance appraisal training needs
  createPerformanceAppraisalTrainingNeed = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const description = req.body.description;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      const performanceAppraisalTrainingNeed = await PerformanceAppraisalTrainingNeed.create({
        description: description,
        performanceAppraisalId: performanceAppraisalId
      });

      this.sendSuccessResponse(res, 'Performance training need created successfully.', dtoFactory.getPerformanceAppraisalTrainingNeedDto(performanceAppraisalTrainingNeed));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updatePerformanceAppraisalTrainingNeed = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const description = req.body.description;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      let performanceAppraisalTrainingNeed = await PerformanceAppraisalTrainingNeed.findOne({
        where: {
          id: id,
          performanceAppraisalId: performanceAppraisalId
        }
      });

      if (!performanceAppraisalTrainingNeed) {
        this.throwError('Performance training need not found.', 404);
      }

      const updatedPerformanceAppraisalTrainingNeed = await performanceAppraisalTrainingNeed.update({
        description: description,
      });

      this.sendSuccessResponse(res, 'Performance training need updated successfully.', dtoFactory.getPerformanceAppraisalTrainingNeedDto(updatedPerformanceAppraisalTrainingNeed));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deletePerformanceAppraisalTrainingNeed = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const performanceAppraisalTrainingNeed = await PerformanceAppraisalTrainingNeed.findByPk(id);

      if (!performanceAppraisalTrainingNeed) {
        this.throwError('Performance Training Need not found', 404);
      }

      await performanceAppraisalTrainingNeed.destroy();

      this.sendSuccessResponse(res, 'Performance Training Need deleted successfully.', {
        message: 'Performance Training Need deleted successfully.'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalTrainingNeed = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const id = req.body.id;

      const appraisalTrainingNeed = await PerformanceAppraisalTrainingNeed.findByPk(id);

      if (!appraisalTrainingNeed) {
        this.throwError('Performance Training Need not found.', 404);
      }

      const appraisalTrainingNeedDto = dtoFactory.getPerformanceAppraisalTrainingNeedDto(appraisalTrainingNeed);

      this.sendSuccessResponse(res, 'Success', appraisalTrainingNeedDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalTrainingNeeds = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const appraisalId = req.body.appraisalId;

      const appraisalTrainingNeeds = await PerformanceAppraisalTrainingNeed.findAll({
        order: [['createdAt', 'ASC']],
        where: {
          performanceAppraisalId: appraisalId
        }
      });

      const appraisalTrainingNeedsDto = appraisalTrainingNeeds.map(appraisalTrainingNeed => dtoFactory.getPerformanceAppraisalTrainingNeedDto(appraisalTrainingNeed));

      this.sendSuccessResponse(res, 'Success', appraisalTrainingNeedsDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Performance appraisal additional assignments
  createPerformanceAppraisalAdditionalAssignment = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const description = req.body.description;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      const performanceAdditionalAssignment = await PerformanceAppraisalAdditionalAssignment.create({
        description: description,
        performanceAppraisalId: performanceAppraisalId
      });

      this.sendSuccessResponse(res, 'Additional assignment created successfully.', dtoFactory.getPerformanceAppraisalAdditionalAssignmentDto(performanceAdditionalAssignment));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updatePerformanceAppraisalAdditionalAssignment = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const description = req.body.description;
      const performanceAppraisalId = req.body.performanceAppraisalId;

      let performanceAppraisalAdditionalAssignment = await PerformanceAppraisalAdditionalAssignment.findOne({
        where: {
          id: id,
          performanceAppraisalId: performanceAppraisalId
        }
      });

      if (!performanceAppraisalAdditionalAssignment) {
        this.throwError('Additional assignment need not found.', 404);
      }

      const updatedPerformanceAppraisalAdditionalAssignment = await performanceAppraisalAdditionalAssignment.update({
        description: description,
      });

      this.sendSuccessResponse(res, 'Additional assignment updated successfully.', dtoFactory.getPerformanceAppraisalAdditionalAssignmentDto(updatedPerformanceAppraisalAdditionalAssignment));

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deletePerformanceAppraisalAdditionalAssignment = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const performanceAppraisalAdditionalAssignment = await PerformanceAppraisalAdditionalAssignment.findByPk(id);

      if (!performanceAppraisalAdditionalAssignment) {
        this.throwError('Additional assignment not found', 404);
      }

      await performanceAppraisalAdditionalAssignment.destroy();

      this.sendSuccessResponse(res, 'Additional assignment deleted successfully.', {
        message: 'Additional assignment deleted successfully.'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalAdditionalAssignment = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const id = req.body.id;

      const appraisaAdditionalAssignment = await PerformanceAppraisalAdditionalAssignment.findByPk(id);

      if (!appraisaAdditionalAssignment) {
        this.throwError('Additional assignment not found.', 404);
      }

      const appraisaAdditionalAssignmentDto = dtoFactory.getPerformanceAppraisalAdditionalAssignmentDto(appraisaAdditionalAssignment);

      this.sendSuccessResponse(res, 'Success', appraisaAdditionalAssignmentDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getPerformanceAppraisalAdditionalAssignments = async(req, res, next) => {
    try {
      this.validateRequest(req);
      const appraisalId = req.body.appraisalId;

      const appraisalAdditionalAssignments = await PerformanceAppraisalAdditionalAssignment.findAll({
        order: [['createdAt', 'ASC']],
        where: {
          performanceAppraisalId: appraisalId
        }
      });

      const appraisalAdditionalAssignmentsDto = appraisalAdditionalAssignments.map(appraisalAdditionalAssignment => dtoFactory.getPerformanceAppraisalAdditionalAssignmentDto(appraisalAdditionalAssignment));

      this.sendSuccessResponse(res, 'Success', appraisalAdditionalAssignmentsDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // private methods
  [_validateAppraisal] = async (employeeId, startDate, endDate) => {

    // Check if the employee exists
    const employee = await User.findOne({
      where: {
        id: employeeId
      }
    });

    if (!employee) {
      this.throwError('Employee not found.', 404);
    }

    // Ensure there's no appraisal for this employee in this period
    const performanceAppraisal = await PerformanceAppraisal.findOne({
      where: {
        employeeId: employeeId,
        [Op.or]: [
          {
            startDate: {
              [Op.between]: [startDate, endDate]
            }
          },
          {
            endDate: {
              [Op.between]: [startDate, endDate]
            }
          },
          {
            [Op.and]: {
              startDate: {
                [Op.lte]: startDate
              },
              endDate: {
                [Op.gt]: startDate
              }
            }
          }
        ]
      }
    });

    if (performanceAppraisal) {
      this.throwError('The employee already has an appraisal for the period specified.', 422);
    }

  }
}

export default new PerformanceController();