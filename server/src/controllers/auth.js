import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import BaseController from './baseController'
import AppConstants from '../utils/appConstants';
import db from '../models/index';
import Roles from '../utils/enums/roles';
import dtoFactory from '../factories/dto'

const Op = db.Sequelize.Op;
const User = db['User'];
const Role = db['Role'];
const Gender = db['Gender'];
const JobGroup = db['JobGroup'];
const EmploymentType = db['EmploymentType'];
const Designation = db['Designation'];

const _getUser = Symbol('_getUser');

class AuthController extends BaseController {

  login = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const username = req.body.username;
      const password = req.body.password;

      const user = await User.findOne({
        include: [Role],
        where: {
          [Op.or]: [{
            username: username
          }, {
            email: username
          }]
        }
      });

      if (!user) {
        const error = new Error('Invalid username or password.');
        error.statusCode = 422;
        throw error;
      }

      const passwordsMatch = await bcrypt.compare(password, user.password);

      if (!passwordsMatch) {
        const error = new Error('Invalid username or password.');
        error.statusCode = 422;
        throw error;
      }

      const token = jwt.sign(
        {
          userId: user.id
        },
        AppConstants.tokenSecret,
        {
          expiresIn: AppConstants.tokenLife
        });

      const refreshToken = jwt.sign(
        {
          userId: user.id
        },
        AppConstants.refreshTokenSecret,
        {
          expiresIn: AppConstants.refreshTokenLife
        }
      );

      this.sendSuccessResponse(res, "Login success.", {
        token: token,
        refreshToken: refreshToken,
        user: {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          roles: user.Roles.map(role => role.name)
        }
      });


    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  refreshToken = async(req, res, next) => {
    try {

      this.validateRequest(req);
      const refreshToken = req.body.refreshToken;

      // verify refresh token
      const decodedRefreshToken = jwt.verify(refreshToken, AppConstants.refreshTokenSecret);

      const newToken = jwt.sign(
        {
          userId: decodedRefreshToken.userId
        },
        AppConstants.tokenSecret,
        {
          expiresIn: AppConstants.tokenLife
        }
      );

      this.sendSuccessResponse(res, 'Refresh token success.', {
        token: newToken
      });

    } catch (error) {
      error.statusCode = 401;
      this.catchControllerErrors(error, next);
    }
  }

  createUser = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const body = req.body;
      const hashedPassword = await bcrypt.hash(body.password, AppConstants.passwordHashSalt);
      const isManager = req.body.isManager || false;
      const isAdmin = req.body.isAdmin || false;

      const gender = await Gender.findOne({
        where: {
          code: body.gender.toUpperCase()
        }
      });

      const createdUser = await User
        .create({
          username: body.username,
          password: hashedPassword,
          firstName: body.firstName,
          middleName: body.middleName,
          lastName: body.lastName,
          email: body.email,
          phoneNumber: body.phoneNumber,
          jobGroupId: body.jobGroupId,
          designationId: body.designationId,
          ministryId: body.ministryId,
          departmentId: body.departmentId,
          sectionId: body.sectionId,
          dutyStationId: body.dutyStationId,
          managerId: body.managerId,
          employmentTypeId: body.employmentTypeId,
          genderId: gender.id
        });

      //  Add roles
      const roles = await Role.findAll();
      const userRoles = [];

      const employeeRole = roles.find(role => role.name === Roles.Employee);
      const adminRole = roles.find(role => role.name === Roles.Administrator);
      const managerRole = roles.find(role => role.name === Roles.Manager);

      userRoles.push(employeeRole);

      if (isManager) {
        userRoles.push(managerRole);
      }

      if (isAdmin) {
        userRoles.push(adminRole);
      }

      await createdUser.addRoles(userRoles);

      const userDto = await this[_getUser](createdUser.id);

      this.sendSuccessResponse(res, 'User created successfully.', userDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateUser = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const body = req.body;
      const isManager = req.body.isManager || false;
      const isAdmin = req.body.isAdmin || false;

      const gender = await Gender.findOne({
        where: {
          code: body.gender.toUpperCase()
        }
      });

      const user = await User.findByPk(body.id, {
        include: Gender
      });

      let updatedUser = await user
        .update({
          username: body.username,
          firstName: body.firstName,
          middleName: body.middleName,
          lastName: body.lastName,
          email: body.email,
          phoneNumber: body.phoneNumber,
          jobGroupId: body.jobGroupId,
          designationId: body.designationId,
          ministryId: body.ministryId,
          departmentId: body.departmentId,
          sectionId: body.sectionId,
          dutyStationId: body.dutyStationId,
          managerId: body.managerId,
          employmentTypeId: body.employmentTypeId,
          genderId: gender.id
        });

      // Set Roles
      const roles = await Role.findAll();
      const userRoles = [];

      const employeeRole = roles.find(role => role.name === Roles.Employee);
      const adminRole = roles.find(role => role.name === Roles.Administrator);
      const managerRole = roles.find(role => role.name === Roles.Manager);

      userRoles.push(employeeRole);

      if (isManager) {
        userRoles.push(managerRole);
      }

      if (isAdmin) {
        userRoles.push(adminRole);
      }

      await updatedUser.setRoles(userRoles);

      const userDto = await this[_getUser](updatedUser.id);
      this.sendSuccessResponse(res, 'User updated successfully.', userDto);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getUser = async(req, res, next) => {
    try {
      this.validateRequest(req);

      const user = await this[_getUser](req.body.id);
      this.sendSuccessResponse(res, 'Success', user);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getUsers = async(req, res, next) => {
    try {
      const users = await User.findAll({
        include: [Designation, EmploymentType, Gender, JobGroup, Role]
      });

      const userDtos = users.map(user => dtoFactory.getUserDto(user));

      this.sendSuccessResponse(res, 'Success', userDtos);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Private methods
  [ _getUser] = async (userId) => {

    const user = await User.findByPk(userId, {
      include: [Gender, Role, EmploymentType]
    });

    if (!user) {
      this.throwError('User not found', 404);
    }

    const userDto = dtoFactory.getUserDto(user);

    return userDto;
  }

}

export default new AuthController();
