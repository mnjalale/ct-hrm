import BaseController from './baseController'
import db from '../models/index';

const Department = db['Department'];
const Op = db.Sequelize.Op;

class DepartmentsController extends BaseController {

  createDepartment = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const ministryId = req.body.ministryId;
      const name = req.body.name;

      // Ensure a similar department hasn't been entered for the same ministry
      const existingDepartment = await Department.findOne({
        where: {
          ministryId: ministryId,
          name: name
        }
      });

      if (existingDepartment) {
        this.throwError('A department with a similar name already exists within the ministry.', 422);
      }

      const department = await Department.create({
        ministryId: ministryId,
        name: name
      });

      const returnValue = {
        id: department.id,
        ministryId: department.ministryId,
        name: department.name
      };

      this.sendSuccessResponse(res, "Department created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateDepartment = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;
      const ministryId = req.body.ministryId;

      // Ensure a similar department hasn't been entered
      const existingDepartment = await Department.findOne({
        where: {
          id: {
            [Op.ne]: id
          },
          ministryId: ministryId,
          name: name
        }
      });

      if (existingDepartment) {
        this.throwError('A department with a similar name already exists within the ministry.', 422);
      }

      const department = await Department.findByPk(id);
      const updatedDepartment = await department.update({
        name: name,
        ministryId: ministryId
      });

      const returnValue = {
        id: updatedDepartment.id,
        name: updatedDepartment.name,
        ministryId: updatedDepartment.ministryId
      };

      this.sendSuccessResponse(res, "Department updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteDepartment= async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const department = await Department.findByPk(id);
      await department.destroy();
      this.sendSuccessResponse(res, "Department deleted successfully", {
        message: 'Department deleted successfully"'
      });
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDepartment = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const departmentId = req.body.id;

      const department = await Department.findOne({
        where: {
          id: departmentId
        }
      });

      const returnValue = {
        id: department.id,
        name: department.name,
        ministryId: department.ministryId
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDepartments = async (req, res, next) => {
    try {

      const ministryId = req.body.ministryId;
      let departments;

      if (ministryId) {
        departments = await Department.findAll({
          where: {
            ministryId: ministryId
          },
          order: [['name', 'ASC']]
        });
      } else {
        departments = await Department.findAll({
          order: [['name', 'ASC']]
        });
      }

      const returnValue = departments.map(department => {
        return {
          id: department.id,
          name: department.name,
          ministryId: department.ministryId
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }
}

export default new DepartmentsController();
