import BaseController from './baseController'
import db from '../models/index';

const Ministry = db['Ministry'];
const Op = db.Sequelize.Op;

class MinistriesController extends BaseController {

  createMinistry = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const name = req.body.name;

      // Ensure a similar ministry hasn't been entered
      const existingMinistry = await Ministry.findOne({
        where: {
          name: name
        }
      });

      if (existingMinistry) {
        this.throwError('A ministry with a similar name already exists.', 422);
      }

      const ministry = await Ministry.create({
        name: name
      });

      const returnValue = {
        id: ministry.id,
        name: ministry.name
      };

      this.sendSuccessResponse(res, "Ministry created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateMinistry = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;

      // Ensure a similar ministry hasn't been entered
      const existingMinistry = await Ministry.findOne({
        where: {
          name: name,
          id: {
            [Op.ne]: id
          }
        }
      });

      if (existingMinistry) {
        this.throwError('A ministry with a similar name already exists.', 422);
      }

      const ministry = await Ministry.findByPk(id);
      const updatedMinistry = await ministry.update({
        name: name
      });

      const returnValue = {
        id: updatedMinistry.id,
        name: updatedMinistry.name
      };

      this.sendSuccessResponse(res, "Ministry updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteMinistry = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const ministry = await Ministry.findByPk(id);
      await ministry.destroy();

      this.sendSuccessResponse(res, "Ministry deleted successfully", {
        message: 'Ministry deleted successfully'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getMinistry = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const ministryId = req.body.id;

      const ministry = await Ministry.findOne({
        where: {
          id: ministryId
        }
      });

      const returnValue = {
        id: ministry.id,
        name: ministry.name
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getMinistries = async (req, res, next) => {
    try {

      const ministries = await Ministry.findAll({
        order: [['name', 'ASC']]
      });

      const returnValue = ministries.map(ministry => {
        return {
          id: ministry.id,
          name: ministry.name
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }
}

export default new MinistriesController();
