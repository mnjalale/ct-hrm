import BaseController from './baseController'
import db from '../models/index';

const Designation = db['Designation'];

const Op = db.Sequelize.Op;

class DesignationsController extends BaseController {

  createDesignation = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const name = req.body.name;

      // Ensure a similar designation hasn't been entered
      const existingDesignation = await Designation.findOne({
        where: {
          name: name
        }
      });

      if (existingDesignation) {
        this.throwError('A designation with a similar name already exists.', 422);
      }

      const designation = await Designation.create({
        name: name
      });

      const returnValue = {
        id: designation.id,
        name: designation.name
      };

      this.sendSuccessResponse(res, "Designation created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateDesignation = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;

      // Ensure a similar designation hasn't been entered
      const existingDesignation = await Designation.findOne({
        where: {
          name: name,
          id: {
            [Op.ne]: id
          }
        }
      });

      if (existingDesignation) {
        this.throwError('A designation with a similar name already exists.', 422);
      }

      const designation = await Designation.findByPk(id);
      const updatedDesignation = await designation.update({
        name: name
      });

      const returnValue = {
        id: updatedDesignation.id,
        name: updatedDesignation.name
      };

      this.sendSuccessResponse(res, "Designation updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteDesignation = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const designation = await Designation.findByPk(id);
      await designation.destroy();

      this.sendSuccessResponse(res, "Designation deleted successfully", {
        message: 'Designation deleted successfully'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDesignation = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const designationId = req.body.id;

      const designation = await Designation.findOne({
        where: {
          id: designationId
        }
      });

      const returnValue = {
        id: designation.id,
        name: designation.name
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDesignations = async (req, res, next) => {
    try {
      const designations = await Designation.findAll({
        order: [['name', 'ASC']]
      });

      const returnValue = designations.map(designation => {
        return {
          id: designation.id,
          name: designation.name
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }
}

export default new DesignationsController();
