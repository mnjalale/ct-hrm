import BaseController from './baseController'
import db from '../models/index';

const EmploymentType = db['EmploymentType'];
const Op = db.Sequelize.Op;

class EmploymentTypesController extends BaseController {

  createEmploymentType = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const name = req.body.name;

      // Ensure a similar employment type hasn't been entered
      const existingEmploymentType = await EmploymentType.findOne({
        where: {
          name: name
        }
      });

      if (existingEmploymentType) {
        this.throwError('An employment type with a similar name already exists.', 422);
      }

      const employmentType = await EmploymentType.create({
        name: name
      });

      const returnValue = {
        id: employmentType.id,
        name: employmentType.name
      };

      this.sendSuccessResponse(res, "Employment type created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateEmploymentType = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;

      // Ensure a similar employment type hasn't been entered
      const existingEmploymentType = await EmploymentType.findOne({
        where: {
          name: name,
          id: {
            [Op.ne]: id
          }
        }
      });

      if (existingEmploymentType) {
        this.throwError('An employment type with a similar name already exists.', 422);
      }

      const employmentType = await EmploymentType.findByPk(id);
      const updatedEmploymentType = await employmentType.update({
        name: name
      });

      const returnValue = {
        id: updatedEmploymentType.id,
        name: updatedEmploymentType.name
      };

      this.sendSuccessResponse(res, "Employment type updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteEmploymentType = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const employmentType = await EmploymentType.findByPk(id);
      await employmentType.destroy();

      this.sendSuccessResponse(res, "Employment Type deleted successfully", {
        message: 'Employment Type deleted successfully'
      });

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getEmploymentType = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const employmentTypeId = req.body.id;

      const employmentType = await EmploymentType.findOne({
        where: {
          id: employmentTypeId
        }
      });

      const returnValue = {
        id: employmentType.id,
        name: employmentType.name
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getEmploymentTypes = async (req, res, next) => {
    try {
      const employmentTypes = await EmploymentType.findAll({
        order: [['name', 'ASC']]
      });

      const returnValue = employmentTypes.map(employmentType => {
        return {
          id: employmentType.id,
          name: employmentType.name
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

}

export default new EmploymentTypesController();
