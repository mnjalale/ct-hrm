import BaseController from "./baseController";
import db from '../models/index';
import dtoFactory from '../factories/dto';
import Roles from '../utils/enums/roles';


const User = db['User'];
const Role = db['Role'];
const Gender = db['Gender'];
const JobGroup = db['JobGroup'];
const EmploymentType = db['EmploymentType'];
const Designation = db['Designation'];
const PerformanceAppraisal = db['PerformanceAppraisal'];
const _getWhereClause = Symbol('_getWhereClause');

class EmployeesController extends BaseController {

  getManagerTeamMembers = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const managerId = req.userId;
      const users = await User.findAll({
        order: [['firstName', 'ASC']],
        include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal],
        where: {
          managerId: managerId
        }
      });

      const userDtos = users.map(user => dtoFactory.getUserDto(user));

      this.sendSuccessResponse(res, 'Success', userDtos);

    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getEmployees = async(req, res, next) => {
    try {

      let users;
      let whereClause = this[_getWhereClause](req.body.ministryId, req.body.departmentId, req.body.sectionId, req.body.dutyStationId);

      if (whereClause) {
        users = await User.findAll({
          order: [['firstName', 'ASC']],
          include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal],
          where: whereClause
        });
      } else {
        users = await User.findAll({
          order: [['firstName', 'ASC']],
          include: [Designation, EmploymentType, Gender, JobGroup, Role, PerformanceAppraisal]
        });
      }

      // Get only users who are employees (have employee role)
      // todo: THIS IS VERY INEFFICIENT (the filtering should happen in the database). 
      // I'LL BE COMING BACK TO THIS.
      users = users.filter(user => {
        return !!user.Roles.find(role => role.name === Roles.Employee);
      });

      const userDtos = users.map(user => dtoFactory.getUserDto(user));

      this.sendSuccessResponse(res, 'Success', userDtos);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  // Helpers

  [ _getWhereClause]  = (ministryId, departmentId, sectionId, dutyStationId) => {
    let whereClause;

    if (ministryId && departmentId && sectionId && dutyStationId) {
      whereClause = {
        ministryId: ministryId,
        departmentId: departmentId,
        sectionId: sectionId,
        dutyStationId: dutyStationId
      }
    } else if (ministryId && departmentId && sectionId) {
      whereClause = {
        ministryId: ministryId,
        departmentId: departmentId,
        sectionId: sectionId
      }
    } else if (ministryId && departmentId) {
      whereClause = {
        ministryId: ministryId,
        departmentId: departmentId
      }
    } else if (ministryId) {
      whereClause = {
        ministryId: ministryId
      }
    }

    return whereClause;
  }
}


export default new EmployeesController();