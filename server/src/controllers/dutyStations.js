import BaseController from './baseController'
import db from '../models/index';

const DutyStation = db['DutyStation'];
const Op = db.Sequelize.Op;

class DutyStationsController extends BaseController {

  createDutyStation = async (req, res, next) => {
    try {
      this.validateRequest(req);
      const sectionId = req.body.sectionId;
      const name = req.body.name;

      // Ensure a similar duty station hasn't been entered for the same section
      const existingDutyStation = await DutyStation.findOne({
        where: {
          sectionId: sectionId,
          name: name
        }
      });

      if (existingDutyStation) {
        this.throwError('A duty station with a similar name already exists within the section.', 422);
      }

      const dutyStation = await DutyStation.create({
        sectionId: sectionId,
        name: name
      });

      const returnValue = {
        id: dutyStation.id,
        sectionId: dutyStation.sectionId,
        name: dutyStation.name
      };

      this.sendSuccessResponse(res, "Duty Station created successfully.", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  updateDutyStation = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;
      const name = req.body.name;
      const sectionId = req.body.sectionId;

      // Ensure a similar duty station hasn't been entered
      const existingDutyStation = await DutyStation.findOne({
        where: {
          id: {
            [Op.ne]: id
          },
          sectionId: sectionId,
          name: name
        }
      });

      if (existingDutyStation) {
        this.throwError('A duty station with a similar name already exists within the section.', 422);
      }

      const dutyStation = await DutyStation.findByPk(id);
      const updatedDutyStation = await dutyStation.update({
        name: name,
        sectionId: sectionId
      });

      const returnValue = {
        id: updatedDutyStation.id,
        name: updatedDutyStation.name,
        sectionId: updatedDutyStation.sectionId
      };

      this.sendSuccessResponse(res, "Duty Station updated successfully", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  deleteDutyStation= async (req, res, next) => {
    try {
      this.validateRequest(req);

      const id = req.body.id;

      const dutyStation = await DutyStation.findByPk(id);
      await dutyStation.destroy();
      this.sendSuccessResponse(res, "Duty Station deleted successfully", {
        message: 'Duty Station deleted successfully"'
      });
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDutyStation = async (req, res, next) => {
    try {
      this.validateRequest(req);

      const dutyStationId = req.body.id;

      const dutyStation = await DutyStation.findOne({
        where: {
          id: dutyStationId
        }
      });

      const returnValue = {
        id: dutyStation.id,
        name: dutyStation.name,
        sectionId: dutyStation.sectionId
      };

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }

  getDutyStations = async (req, res, next) => {
    try {

      const sectionId = req.body.sectionId;
      let dutyStations;

      if (sectionId) {
        dutyStations = await DutyStation.findAll({
          where: {
            sectionId: sectionId
          },
          order: [['name', 'ASC']]
        });
      } else {
        dutyStations = await DutyStation.findAll({
          order: [['name', 'ASC']]
        });
      }

      const returnValue = dutyStations.map(dutyStation => {
        return {
          id: dutyStation.id,
          name: dutyStation.name,
          sectionId: dutyStation.sectionId
        }
      });

      this.sendSuccessResponse(res, "Success", returnValue);
    } catch (error) {
      this.catchControllerErrors(error, next);
    }
  }
}

export default new DutyStationsController();
