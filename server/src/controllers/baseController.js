import { validationResult } from 'express-validator/check';

class BaseController {

  sendSuccessResponse(res, message, data) {
    res
      .status(200)
      .json({
        message: message,
        statusCode: 200,
        data: data
      });
  }

  validateRequest(req) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error('Validation Errors Occurred.');
      error.statusCode = 422;
      error.errors = errors.array().map(error => error.msg);
      throw error;
    }
  }

  throwError(message, statusCode) {
    const error = new Error(message);
    error.statusCode = statusCode;
    error.errors = [message];
    throw error;
  }

  catchControllerErrors(error, next) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }

}

export default BaseController;