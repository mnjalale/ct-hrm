'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      personalNo: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      middleName: {
        type: Sequelize.STRING,
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        unique: true
      },
      phoneNumber: {
        type: Sequelize.STRING,
      },
      jobGroupId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'JobGroups',
          key: 'id'
        }
      },
      designationId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Designations',
          key: 'id'
        }
      },
      ministryId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Ministries',
          key: 'id'
        }
      },
      departmentId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Departments',
          key: 'id'
        }
      },
      sectionId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Sections',
          key: 'id'
        }
      },
      dutyStationId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'DutyStations',
          key: 'id'
        }
      },
      managerId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      employmentTypeId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'EmploymentTypes',
          key: 'id'
        }
      },
      genderId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Genders',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};