'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PerformanceAppraisals', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false
      },
      startDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      midYearReviewDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      endDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      appraiseePeriodStartSignature: {
        type: Sequelize.STRING,
      },
      appraiseePeriodStartSignatureDate: {
        type: Sequelize.DATE,
      },
      supervisorPeriodStartName: {
        type: Sequelize.STRING,
      },
      supervisorPeriodStartSignature: {
        type: Sequelize.STRING,
      },
      supervisorPeriodStartSignatureDate: {
        type: Sequelize.DATE,
      },
      supervisorMidYearReviewName: {
        type: Sequelize.STRING,
      },
      supervisorMidYearReviewSignature: {
        type: Sequelize.STRING,
      },
      supervisorMidYearReviewSignatureDate: {
        type: Sequelize.DATE,
      },
      trainingAppraiseeSignature: {
        type: Sequelize.STRING
      },
      trainingAppraiseeSignatureDate: {
        type: Sequelize.DATE
      },
      trainingSupervisorName: {
        type: Sequelize.STRING
      },
      trainingSupervisorSignature: {
        type: Sequelize.STRING
      },
      trainingSupervisorSignatureDate: {
        type: Sequelize.DATE
      },
      appraiseePeriodEndComments: {
        type: Sequelize.STRING
      },
      supervisorPeriodEndComments: {
        type: Sequelize.STRING,
      },
      supervisorPeriodEndName: {
        type: Sequelize.STRING,
      },
      supervisorPeriodEndSignature: {
        type: Sequelize.STRING,
      },
      supervisorPeriodEndSignatureDate: {
        type: Sequelize.DATE,
      },
      recommendedRewardType: {
        type: Sequelize.STRING
      },
      recommendedOtherInterventions: {
        type: Sequelize.STRING
      },
      recommendedSanction: {
        type: Sequelize.STRING
      },
      recommendationMinuteNo: {
        type: Sequelize.STRING
      },
      recommendationMeetingDate: {
        type: Sequelize.DATE
      },
      recommendationChairPersonName: {
        type: Sequelize.STRING
      },
      recommendationChairPersonSignature: {
        type: Sequelize.STRING
      },
      recommendationChairPersonSignatureDate: {
        type: Sequelize.DATE
      },
      recommendationSecretaryName: {
        type: Sequelize.STRING
      },
      recommendationSecretarySignature: {
        type: Sequelize.STRING
      },
      recommendationSecretarySignatureDate: {
        type: Sequelize.DATE
      },
      recommendationAuthorizedOfficerApproved: {
        type: Sequelize.BOOLEAN
      },
      recommendationAuthorizedOfficerName: {
        type: Sequelize.STRING
      },
      recommendationAuthorizedOfficerSignature: {
        type: Sequelize.STRING
      },
      recommendationAuthorizedOfficerSignatureDate: {
        type: Sequelize.DATE
      },
      employeeId: {
        type: Sequelize.UUID,
        onDelete: 'RESTRICT',
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PerformanceAppraisals');
  }
};