import express from 'express';
import lookupController from '../controllers/lookup';
import designationsController from '../controllers/designations';
import employmentTypesController from '../controllers/employmentTypes';
import ministriesController from '../controllers/ministries';
import departmentsController from '../controllers/departments';
import sectionsController from '../controllers/sections';
import dutyStationsController from '../controllers/dutyStations';
import security from '../middleware/security';
import departmentsValidator from '../middleware/route-validators/departments';
import designationsValidator from '../middleware/route-validators/designations';
import dutyStationsValidator from '../middleware/route-validators/dutyStations';
import employmentTypesValidator from '../middleware/route-validators/employmentTypes';
import ministriesValidator from '../middleware/route-validators/ministries';
import sectionsValidator from '../middleware/route-validators/sections';

const _config = Symbol('config');

class LookupRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {
    this.router.get('/jobGroups', security.checkAuthentication, lookupController.getJobGroups);
    this.router.get('/managers', security.checkAuthentication, lookupController.getManagers);

  }
}

export default new LookupRoutes().router;


