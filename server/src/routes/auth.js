import express from 'express';
import authController from '../controllers/auth';
import authValidator from '../middleware/route-validators/auth';
import security from '../middleware/security';
const _config = Symbol('config');

class AuthRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {
    this.router.post('/login', authValidator.validateLogin(), authController.login);
    this.router.post('/refreshToken', authValidator.validateRefreshToken(), authController.refreshToken);
    this.router.post('/createUser', security.checkAdminAuthorization, authValidator.validateCreateUser(), authController.createUser);
    this.router.post('/updateUser', security.checkAuthentication, authValidator.validateUpdateUser(), authController.updateUser);
    this.router.post('/getUser', security.checkAuthentication, authValidator.validateGetUser(), authController.getUser);
    this.router.post('/getUsers', security.checkAdminAuthorization, authController.getUsers);
  }
}

export default new AuthRoutes().router;


