import express from 'express';
import employmentTypesController from '../controllers/employmentTypes';
import security from '../middleware/security';
import employmentTypesValidator from '../middleware/route-validators/employmentTypes';

const _config = Symbol('config');

class EmploymentTypeRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {
    this.router.post('/get', security.checkAuthentication, employmentTypesValidator.validateGetEmploymentType(), employmentTypesController.getEmploymentType);
    this.router.post('/getAll', security.checkAuthentication, employmentTypesController.getEmploymentTypes);
    this.router.post('/create', security.checkAuthentication, employmentTypesValidator.validateCreateEmploymentType(), employmentTypesController.createEmploymentType);
    this.router.post('/update', security.checkAuthentication, employmentTypesValidator.validateUpdateEmploymentType(), employmentTypesController.updateEmploymentType);
    this.router.post('/delete', security.checkAuthentication, employmentTypesValidator.validateDeleteEmploymentType(), employmentTypesController.deleteEmploymentType);

  }
}

export default new EmploymentTypeRoutes().router;


