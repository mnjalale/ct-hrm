import express from 'express';
import ministriesController from '../controllers/ministries';
import security from '../middleware/security';
import ministriesValidator from '../middleware/route-validators/ministries';

const _config = Symbol('config');

class MinistryRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {

    this.router.post('/get', security.checkAuthentication, ministriesValidator.validateGetMinistry(), ministriesController.getMinistry);
    this.router.post('/getAll', security.checkAuthentication, ministriesController.getMinistries);
    this.router.post('/create', security.checkAuthentication, ministriesValidator.validateCreateMinistry(), ministriesController.createMinistry);
    this.router.post('/update', security.checkAuthentication, ministriesValidator.validateUpdateMinistry(), ministriesController.updateMinistry);
    this.router.post('/delete', security.checkAuthentication, ministriesValidator.validateDeleteMinistry(), ministriesController.deleteMinistry);

  }
}

export default new MinistryRoutes().router;


