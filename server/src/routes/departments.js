import express from 'express';
import departmentsController from '../controllers/departments';
import security from '../middleware/security';
import departmentsValidator from '../middleware/route-validators/departments';

const _config = Symbol('config');

class DepartmentRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {

    this.router.post('/get', security.checkAuthentication, departmentsValidator.validateGetDepartment(), departmentsController.getDepartment);
    this.router.post('/getAll', security.checkAuthentication, departmentsController.getDepartments);
    this.router.post('/create', security.checkAuthentication, departmentsValidator.validateCreateDepartment(), departmentsController.createDepartment);
    this.router.post('/update', security.checkAuthentication, departmentsValidator.validateUpdateDepartment(), departmentsController.updateDepartment);
    this.router.post('/delete', security.checkAuthentication, departmentsValidator.validateDeleteDepartment(), departmentsController.deleteDepartment);

  }
}

export default new DepartmentRoutes().router;


