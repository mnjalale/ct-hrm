import express from 'express';
import designationsController from '../controllers/designations';
import security from '../middleware/security';
import designationsValidator from '../middleware/route-validators/designations';

const _config = Symbol('config');

class DesignationRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {

    this.router.post('/get', security.checkAuthentication, designationsValidator.validateGetDesignation(), designationsController.getDesignation);
    this.router.post('/getAll', security.checkAuthentication, designationsController.getDesignations);
    this.router.post('/create', security.checkAuthentication, designationsValidator.validateCreateDesignation(), designationsController.createDesignation);
    this.router.post('/update', security.checkAuthentication, designationsValidator.validateUpdateDesignation(), designationsController.updateDesignation);
    this.router.post('/delete', security.checkAuthentication, designationsValidator.validateDeleteDesignation(), designationsController.deleteDesignation);

  }
}

export default new DesignationRoutes().router;


