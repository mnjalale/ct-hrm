import express from 'express';
import employeesController from '../controllers/employees';
import security from '../middleware/security';
const _config = Symbol('config');

class EmployeesRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {
    this.router.post('/get', security.checkAdminAuthorization, employeesController.getEmployees);
    this.router.post('/getManagerTeamMembers', security.checkAuthentication, employeesController.getManagerTeamMembers);
  }
}

export default new EmployeesRoutes().router;


