import express from 'express';
import sectionsController from '../controllers/sections';
import security from '../middleware/security';
import sectionsValidator from '../middleware/route-validators/sections';

const _config = Symbol('config');

class SectionRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {

    this.router.post('/get', security.checkAuthentication, sectionsValidator.validateGetSection(), sectionsController.getSection);
    this.router.post('/getAll', security.checkAuthentication, sectionsController.getSections);
    this.router.post('/create', security.checkAuthentication, sectionsValidator.validateCreateSection(), sectionsController.createSection);
    this.router.post('/update', security.checkAuthentication, sectionsValidator.validateUpdateSection(), sectionsController.updateSection);
    this.router.post('/delete', security.checkAuthentication, sectionsValidator.validateDeleteSection(), sectionsController.deleteSection);

  }
}

export default new SectionRoutes().router;


