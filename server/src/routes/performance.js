import express from 'express';
import performanceValidator from '../middleware/route-validators/performance';
import performanceController from '../controllers/performance';
import security from '../middleware/security';
const _config = Symbol('config');

class PerformanceRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {
    this.router.post('/createAppraisal', security.checkAuthentication, performanceValidator.validateCreateAppraisal(), performanceController.createAppraisal);
    this.router.post('/updateAppraisal', security.checkAuthentication, performanceValidator.validateUpdateAppraisal(), performanceController.updateAppraisal);
    this.router.post('/getAppraisal', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisal(), performanceController.getPerformanceAppraisal);
    this.router.post('/getEmployeeAppraisals', security.checkAuthentication, performanceValidator.validateGetEmployeeAppraisals(), performanceController.getEmployeeAppraisals);

    // Appraisal targets
    this.router.post('/createAppraisalTarget', security.checkAuthentication, performanceValidator.validateCreatePerformanceAppraisalTarget(), performanceController.createPerformanceAppraisalTarget);
    this.router.post('/updateAppraisalTarget', security.checkAuthentication, performanceValidator.validateUpdateAppraisalTarget(), performanceController.updatePerformanceAppraisalTarget);
    this.router.post('/deleteAppraisalTarget', security.checkAuthentication, performanceValidator.validateDeleteAppraisalTarget(), performanceController.deletePerformanceAppraisalTarget);
    this.router.post('/getAppraisalTarget', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalTarget(), performanceController.getPerformanceAppraisalTarget);
    this.router.post('/getAppraisalTargets', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalTargets(), performanceController.getPerformanceAppraisalTargets);

    // Appraisal training needs
    this.router.post('/createAppraisalTrainingNeed', security.checkAuthentication, performanceValidator.validateCreatePerformanceAppraisalTrainingNeed(), performanceController.createPerformanceAppraisalTrainingNeed);
    this.router.post('/updateAppraisalTrainingNeed', security.checkAuthentication, performanceValidator.validateUpdatePerformanceAppraisalTrainingNeed(), performanceController.updatePerformanceAppraisalTrainingNeed);
    this.router.post('/deleteAppraisalTrainingNeed', security.checkAuthentication, performanceValidator.validateDeletePerformanceAppraisalTrainingNeed(), performanceController.deletePerformanceAppraisalTrainingNeed);
    this.router.post('/getAppraisalTrainingNeed', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalTrainingNeed(), performanceController.getPerformanceAppraisalTrainingNeed);
    this.router.post('/getAppraisalTrainingNeeds', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalTrainingNeeds(), performanceController.getPerformanceAppraisalTrainingNeeds);

    // Appraisal additional assignment
    this.router.post('/createAppraisalAdditionalAssignment', security.checkAuthentication, performanceValidator.validateCreatePerformanceAppraisalAdditionalAssignment(), performanceController.createPerformanceAppraisalAdditionalAssignment);
    this.router.post('/updateAppraisalAdditionalAssignment', security.checkAuthentication, performanceValidator.validateUpdatePerformanceAppraisalAdditionalAssignment(), performanceController.updatePerformanceAppraisalAdditionalAssignment);
    this.router.post('/deleteAppraisalAdditionalAssignment', security.checkAuthentication, performanceValidator.validateDeletePerformanceAppraisalAdditionalAssignment(), performanceController.deletePerformanceAppraisalAdditionalAssignment);
    this.router.post('/getAppraisalAdditionalAssignment', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalAdditionalAssignment(), performanceController.getPerformanceAppraisalAdditionalAssignment);
    this.router.post('/getAppraisalAdditionalAssignments', security.checkAuthentication, performanceValidator.validateGetPerformanceAppraisalAdditionalAssignments(), performanceController.getPerformanceAppraisalAdditionalAssignments);
  }
}

export default new PerformanceRoutes().router;


