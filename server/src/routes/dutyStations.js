import express from 'express';
import dutyStationsController from '../controllers/dutyStations';
import security from '../middleware/security';
import dutyStationsValidator from '../middleware/route-validators/dutyStations';

const _config = Symbol('config');

class DutyStationRoutes {
  constructor() {
    this.router = express.Router();
    this[_config]();
  }

  [ _config]() {

    this.router.post('/get', security.checkAuthentication, dutyStationsValidator.validateGetDutyStation(), dutyStationsController.getDutyStation);
    this.router.post('/getAll', security.checkAuthentication, dutyStationsController.getDutyStations);
    this.router.post('/create', security.checkAuthentication, dutyStationsValidator.validateCreateDutyStation(), dutyStationsController.createDutyStation);
    this.router.post('/update', security.checkAuthentication, dutyStationsValidator.validateUpdateDutyStation(), dutyStationsController.updateDutyStation);
    this.router.post('/delete', security.checkAuthentication, dutyStationsValidator.validateDeleteDutyStation(), dutyStationsController.deleteDutyStation);
  }
}

export default new DutyStationRoutes().router;


