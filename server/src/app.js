import express from 'express';
import * as bodyParser from 'body-parser';
import AuthRoutes from './routes/auth'
import LookupRoutes from './routes/lookup';
import EmployeeRoutes from './routes/employees';
import PerformanceRoutes from './routes/performance';
import DepartmentRoutes from './routes/departments';
import DesignationRoutes from './routes/designations';
import DutyStationRoutes from './routes/dutyStations';
import EmploymentTypeRoutes from './routes/employmentTypes';
import MinistryRoutes from './routes/ministries';
import SectionRoutes from './routes/sections';

const _config = Symbol('config'); // Use symbols to define private methods

class App {
  constructor() {
    this.app = express();
    this[_config]();
  }

  [ _config]() {

    // Enable CORS
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      next();
    });

    // Body Parser
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
      extended: false
    }));

    // Routes
    this.app.use('/api/auth', AuthRoutes);
    this.app.use('/api/lookup', LookupRoutes);
    this.app.use('/api/employees', EmployeeRoutes);
    this.app.use('/api/performance', PerformanceRoutes);
    this.app.use('/api/employmentTypes', EmploymentTypeRoutes);
    this.app.use('/api/designations', DesignationRoutes);
    this.app.use('/api/departments', DepartmentRoutes);
    this.app.use('/api/dutyStations', DutyStationRoutes);
    this.app.use('/api/ministries', MinistryRoutes);
    this.app.use('/api/sections', SectionRoutes);

    // Error Handling
    this.app.use((error, req, res, next) => {
      const status = error.statusCode || 500;
      const message = error.message;
      const errors = error.errors;
      res
        .status(status)
        .json({
          message: message,
          statusCode: status,
          errors: errors
        });
    })
  }

}

export default new App().app;