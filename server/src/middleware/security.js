import jwt from 'jsonwebtoken';
import AppConstants from '../utils/appConstants';
import db from '../models/index';
import Roles from '../utils/enums/roles';

const User = db['User'];
const Role = db['Role'];
const _throwNotAuthenticatedError = Symbol('throwNotAuthenticatedError');
const _throwNotAuthorizedError = Symbol('throwNotAuthorizedError');
const _getDecodedToken = Symbol('getDecodedToken');

class Security {
  checkAuthentication = (req, res, next) => {
    try {
      const decodedToken = this[_getDecodedToken](req);
      req.userId = decodedToken.userId;
      next();
    } catch (error) {
      if (!error.statusCode) {
        error.statusCode = 401;
      }
      next(error);
    }
  }

  checkAdminAuthorization = async (req, res, next) => {
    try {
      const decodedToken = this[_getDecodedToken](req);
      req.userId = decodedToken.userId;

      const user = await User.findByPk(decodedToken.userId, {
        include: Role
      });

      // Confirm that the user is admin
      if (!user.Roles.find(role => role.name === Roles.Administrator)) {
        this[_throwNotAuthorizedError]();
      }

      next();
    } catch (error) {
      if (!error.statusCode) {
        error.statusCode = 403;
      }
      next(error);
    }
  }

  // Private methods
  [ _getDecodedToken](req) {
    try {
      const authHeader = req.get('Authorization');
      if (!authHeader) {
        this[_throwNotAuthenticatedError]();
      }

      // Expecting 'Bearer [token]'
      const token = authHeader.split(' ')[1];

      if (!token) {
        this[_throwNotAuthenticatedError]();
      }

      const decodedToken = jwt.verify(token, AppConstants.tokenSecret);
      if (!decodedToken) {
        this[_throwNotAuthenticatedError]();
      }

      return decodedToken;
    } catch (error) {
      this[_throwNotAuthenticatedError](error.message);
    }
  }

  [ _throwNotAuthenticatedError] (errorDescription) {
    const error = new Error('Not authenticated.');
    error.statusCode = 401;
    if (errorDescription) {
      error.errors = [errorDescription];
    }
    throw error;
  }

  [ _throwNotAuthorizedError] () {
    const error = new Error('Not authorized.');
    error.statusCode = 403;
    throw error;
  }
}

export default new Security();