import { body } from 'express-validator/check';
import moment from 'moment';

const _getDate = Symbol('_getDate');
const _validateAppraisal = Symbol('_validateAppraisal');


class PerformanceValidator {

  validateCreateAppraisal() {
    return [
      ...this[_validateAppraisal]()
    ];
  }

  validateUpdateAppraisal() {
    return [
      ...this[_validateAppraisal](),
      body('id')
        .not().isEmpty().withMessage('Id is required.')
    ];
  }

  validateGetEmployeeAppraisals() {
    return [
      body('employeeId')
        .not().isEmpty().withMessage('Employee Id is required.')
    ]
  }

  validateGetPerformanceAppraisal() {
    return [
      body('appraisalId')
        .not().isEmpty().withMessage('Appraisal Id is required.')
    ]
  }

  // Performance Appraisal Targets
  validateCreatePerformanceAppraisalTarget() {
    return [
      body('performanceTarget')
        .not().isEmpty().withMessage('Performance target is required.'),
      body('performanceIndicator')
        .not().isEmpty().withMessage('Performance indicator is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateUpdateAppraisalTarget() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.'),
      body('performanceTarget')
        .not().isEmpty().withMessage('Performance target is required.'),
      body('performanceIndicator')
        .not().isEmpty().withMessage('Performance indicator is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateDeleteAppraisalTarget() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.')
    ];
  }

  validateGetPerformanceAppraisalTarget() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.')
    ]
  }

  validateGetPerformanceAppraisalTargets() {
    return [
      body('appraisalId')
        .not().isEmpty().withMessage('Appraisal Id is required.')
    ]
  }

  // Performance Appraisal Training Needs
  validateCreatePerformanceAppraisalTrainingNeed() {
    return [
      body('description')
        .not().isEmpty().withMessage('Description is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateUpdatePerformanceAppraisalTrainingNeed() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.'),
      body('description')
        .not().isEmpty().withMessage('Description is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateDeletePerformanceAppraisalTrainingNeed() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.')
    ];
  }

  validateGetPerformanceAppraisalTrainingNeed() {
    return [
      body('id')
        .not().isEmpty().withMessage('Appraisal Training Need Id is required.')
    ]
  }

  validateGetPerformanceAppraisalTrainingNeeds() {
    return [
      body('appraisalId')
        .not().isEmpty().withMessage('Appraisal Id is required.')
    ]
  }

  // Performance Appraisal Additional Assignments
  validateCreatePerformanceAppraisalAdditionalAssignment() {
    return [
      body('description')
        .not().isEmpty().withMessage('Description is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateUpdatePerformanceAppraisalAdditionalAssignment() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.'),
      body('description')
        .not().isEmpty().withMessage('Description is required.'),
      body('performanceAppraisalId')
        .not().isEmpty().withMessage('Performance Appraisal Id is required.')
    ];
  }

  validateDeletePerformanceAppraisalAdditionalAssignment() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required.')
    ];
  }

  validateGetPerformanceAppraisalAdditionalAssignment() {
    return [
      body('id')
        .not().isEmpty().withMessage('Additional Assignment Id is required.')
    ]
  }

  validateGetPerformanceAppraisalAdditionalAssignments() {
    return [
      body('appraisalId')
        .not().isEmpty().withMessage('Appraisal Id is required.')
    ]
  }

  [ _getDate](stringValue, format) {
    if (format) {
      const date = moment(stringValue, moment);
      return date;
    } else {
      const dateFormat = 'YYYY-MM-DD';
      const date = moment(stringValue, dateFormat);
      return date;
    }
  }

  [ _validateAppraisal]() {
    return [
      body('employeeId')
        .not().isEmpty().withMessage('Employee Id is required.'),
      body('startDate')
        .not().isEmpty().withMessage('Start date is required.')
        .custom((value, {req}) => {
          if (!value || value.trim().length === 0) {
            return true;
          }

          const startDate = this[_getDate](value);
          if (!startDate.isValid()) {
            throw new Error("Invalid start date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
          }
          return true;
        }),
      body('midYearReviewDate')
        .not().isEmpty().withMessage('Mid year review date is required.')
        .custom((value, {req}) => {
          if (!value || value.trim().length === 0) {
            return true;
          }

          const midYearReviewDate = this[_getDate](value);
          if (!midYearReviewDate.isValid()) {
            throw new Error("Invalid mid year review date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
          }

          const startDate = this[_getDate](req.body.startDate);
          if (startDate.isValid() && !startDate.isBefore(midYearReviewDate)) {
            throw new Error('Mid year review date must be after the start date.');
          }

          return true;
        }),
      body('endDate')
        .not().isEmpty().withMessage('End date is required.')
        .custom((value, {req}) => {
          if (!value || value.trim().length === 0) {
            return true;
          }

          const endDate = this[_getDate](value);
          if (!endDate.isValid()) {
            throw new Error("Invalid end date. Date format must be 'YYYY-MM-DD' e.g 2018-12-25");
          }

          const midYearReviewDate = this[_getDate](req.body.midYearReviewDate)
          if (midYearReviewDate.isValid() && !endDate.isAfter(midYearReviewDate)) {
            throw new Error('End date must be after the mid year review date.');
          }
          return true;
        })

    ];
  }

}

export default new PerformanceValidator();