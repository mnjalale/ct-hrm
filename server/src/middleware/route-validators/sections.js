import { body } from 'express-validator/check';
const _validateSection = Symbol('_validateSection');

class SectionsValidator {

  validateGetSection() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateSection() {
    return [
      ...this[_validateSection]()
    ];
  }

  validateUpdateSection() {
    return [
      ...this[_validateSection](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteSection() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateSection]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.'),
      body('departmentId')
        .not().isEmpty().withMessage('Department is required.')
    ]
  }

}

export default new SectionsValidator();