import { body } from 'express-validator/check';
const _validateMinistry = Symbol('_validateMinistry');

class MinistriesValidator {

  validateGetMinistry() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateMinistry() {
    return [
      ...this[_validateMinistry]()
    ];
  }

  validateUpdateMinistry() {
    return [
      ...this[_validateMinistry](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteMinistry() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateMinistry]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.')
    ]
  }

}

export default new MinistriesValidator();