import { body } from 'express-validator/check';
const _validateDesignation = Symbol('_validateDesignation');

class DesignationsValidator {

  validateGetDesignation() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateDesignation() {
    return [
      ...this[_validateDesignation]()
    ];
  }

  validateUpdateDesignation() {
    return [
      ...this[_validateDesignation](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteDesignation() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateDesignation]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.')
    ]
  }

}

export default new DesignationsValidator();