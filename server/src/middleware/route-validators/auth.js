import { body } from 'express-validator/check';
import db from '../../models/index';
const _validateUser = Symbol('validateUser');

const User = db['User'];
const Op = db.Sequelize.Op;

class AuthValidator {

  validateLogin() {
    return [
      body('username')
        .not().isEmpty().withMessage('Username is required'),
      body('password')
        .not().isEmpty().withMessage('Password is required.')
    ];
  }

  validateRefreshToken() {
    return [
      body('refreshToken')
        .not().isEmpty().withMessage('Refresh token is required.')
    ];
  }

  validateCreateUser() {
    return [
      ...this[_validateUser](),
      body('username')
        .not().isEmpty().withMessage('Username is required')
        .custom(async (value, {req}) => {
          const user = await User.findOne({
            where: {
              username: value
            }
          });

          if (user) {
            const error = new Error('Username is already in use.');
            throw error;
          } else {
            return true;
          }
        }),
      body('password')
        .not().isEmpty().withMessage('Password is required.'),
      body('email')
        .optional({
          nullable: true
        })
        .isEmail().withMessage('Enter valid email address.')
        .normalizeEmail()
        .custom(async (value, {req}) => {
          if (!value) {
            return true;
          }
          const user = await User.findOne({
            where: {
              email: value
            }
          });

          if (user) {
            const error = new Error('Email address is already in use.');
            throw error;
          } else {
            return true;
          }
        }),

    ];
  }

  validateUpdateUser() {
    return [
      ...this[_validateUser](),
      body('id')
        .not().isEmpty().withMessage('Id is required'),
      body('username')
        .not().isEmpty().withMessage('Username is required')
        .custom(async (value, {req}) => {
          const user = await User.findOne({
            where: {
              username: value,
              id: {
                [Op.ne]: req.body.id
              }
            }
          });

          if (user) {
            const error = new Error('Username is already in use.');
            throw error;
          } else {
            return true;
          }
        }),
      body('email')
        .optional({
          nullable: true
        })
        .isEmail().withMessage('Enter valid email address.')
        .normalizeEmail()
        .custom(async (value, {req}) => {
          if (!value) {
            return true;
          }
          const user = await User.findOne({
            where: {
              email: value,
              id: {
                [Op.ne]: req.body.id
              }
            }
          });

          if (user) {
            const error = new Error('Email address is already in use.');
            throw error;
          } else {
            return true;
          }
        })
    ];
  }

  validateGetUser() {
    return [
      body('id')
        .not().isEmpty().withMessage("Id is required")
    ];
  }

  [ _validateUser]() {
    return [
      body('firstName')
        .not().isEmpty().withMessage('First name is required.'),
      body('lastName')
        .not().isEmpty().withMessage('Last name is required.'),
      body('phoneNumber')
        .not().isEmpty().withMessage('Phone number is required.'),
      body('gender')
        .not().isEmpty().withMessage('Gender is required.')
        .custom((value, {req}) => {
          if (!value) {
            return true;
          }
          const gender = value.toUpperCase();
          if (gender !== 'M' && gender !== 'F') {
            const error = new Error("Gender value must be either 'M' or 'F'");
            throw error;
          } else {
            return true;
          }
        }),
      body('isManager')
        .optional()
        .isBoolean().withMessage("'Is Manager' must be a boolean value."),
      body('isAdmin')
        .optional()
        .isBoolean().withMessage("'Is Admin' must be a boolean value.")
    ]
  }

}

export default new AuthValidator();