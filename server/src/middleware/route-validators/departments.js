import { body } from 'express-validator/check';
const _validateDepartment = Symbol('_validateDepartment');

class DepartmentsValidator {

  validateGetDepartment() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateDepartment() {
    return [
      ...this[_validateDepartment]()
    ];
  }

  validateUpdateDepartment() {
    return [
      ...this[_validateDepartment](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteDepartment() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateDepartment]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.'),
      body('ministryId')
        .not().isEmpty().withMessage('Ministry is required.')
    ]
  }

}

export default new DepartmentsValidator();