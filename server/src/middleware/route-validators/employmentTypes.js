import { body } from 'express-validator/check';
const _validateEmploymentType = Symbol('_validateEmploymentType');

class EmploymentTypesValidator {

  validateGetEmploymentType() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateEmploymentType() {
    return [
      ...this[_validateEmploymentType]()
    ];
  }

  validateUpdateEmploymentType() {
    return [
      ...this[_validateEmploymentType](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteEmploymentType() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateEmploymentType]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.')
    ]
  }

}

export default new EmploymentTypesValidator();