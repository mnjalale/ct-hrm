import { body } from 'express-validator/check';
const _validateDutyStation = Symbol('_validateDutyStation');

class DutyStationsValidator {

  validateGetDutyStation() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateCreateDutyStation() {
    return [
      ...this[_validateDutyStation]()
    ];
  }

  validateUpdateDutyStation() {
    return [
      ...this[_validateDutyStation](),
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  validateDeleteDutyStation() {
    return [
      body('id')
        .not().isEmpty().withMessage('Id is required')
    ];
  }

  [ _validateDutyStation]() {
    return [
      body('name')
        .not().isEmpty().withMessage('Name is required.'),
      body('sectionId')
        .not().isEmpty().withMessage('Section is required.')
    ]
  }

}

export default new DutyStationsValidator();