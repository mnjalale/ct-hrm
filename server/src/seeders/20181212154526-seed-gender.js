'use strict';

const uuidV4 = require('uuid/v4');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const genderCodes = [['M', 'Male'], ['F', 'Female']];
    const sequelize = queryInterface.sequelize;
    const currentGenders = await sequelize.query('SELECT * FROM genders', {
      type: sequelize.QueryTypes.SELECT
    });
    const genders = [];

    genderCodes.forEach(genderCode => {
      if (!currentGenders.find(gender => gender.code === genderCode[0])) {
        genders.push({
          id: uuidV4(),
          code: genderCode[0],
          description: genderCode[1],
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    })

    if (genders.length > 0) {
      return queryInterface.bulkInsert('Genders', genders, {});
    }

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Genders', null, {});
  }
};
