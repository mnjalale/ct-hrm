'use strict';

const uuidV4 = require('uuid/v4');
const bcrypt = require('bcryptjs');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const sequelize = queryInterface.sequelize;
    const currentUsers = await sequelize.query("SELECT * FROM Users where username='admin'", {
      type: sequelize.QueryTypes.SELECT
    });

    if (currentUsers.length > 0) {
      return;
    }

    const currentRoles = await sequelize.query("SELECT * FROM Roles where name='Administrator'", {
      type: sequelize.QueryTypes.SELECT
    });

    if (currentRoles.length === 0) {
      return;
    }

    const adminRoleId = currentRoles[0].id;

    const hashedPassword = await bcrypt.hash('12345678', 12);
    const userId = uuidV4();

    const user = {
      id: userId,
      username: 'admin',
      password: hashedPassword,
      firstName: 'System',
      lastName: 'Administrator',
      createdAt: new Date(),
      updatedAt: new Date()
    };

    return queryInterface
      .bulkInsert('Users', [user])
      .then(result => {

        const userRole = {
          id: uuidV4(),
          userId: userId,
          roleId: adminRoleId,
          createdAt: new Date(),
          updatedAt: new Date()
        };

        return queryInterface.bulkInsert('UserRoles', [userRole]);
      });
  },

  down: async (queryInterface, Sequelize) => {

    const sequelize = queryInterface.sequelize;
    const currentUsers = await sequelize.query("SELECT * FROM Users where username='admin'", {
      type: sequelize.QueryTypes.SELECT
    });

    if (currentUsers.length === 0) {
      return;
    }

    const adminUserId = currentUsers[0].id;

    return queryInterface
      .bulkDelete('UserRoles', {
        userId: adminUserId
      }, {})
      .then(result => {
        return queryInterface.bulkDelete('Users', {
          id: adminUserId
        }, {});
      });
  }
};
