'use strict';

const uuidV4 = require('uuid/v4');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const sequelize = queryInterface.sequelize;
    const currentJobGroups = await sequelize.query('SELECT * FROM JobGroups', {
      type: sequelize.QueryTypes.SELECT
    });

    const oldJobGroupCodes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T'];
    const newJobGroupCodes = ['B1', 'B2', 'B3', 'B4', 'B5', 'C1', 'C2', 'C3', 'C4', 'C5', 'D1', 'D2', 'D3', 'D4', 'D5', 'E1', 'E2', 'E3'];
    const jobGroups = [];

    newJobGroupCodes.forEach((jobGroupCode, index) => {
      if (!currentJobGroups.find(jobGroup => jobGroup.code === jobGroupCode)) {
        jobGroups.push({
          id: uuidV4(),
          code: jobGroupCode,
          alternativeCode: oldJobGroupCodes[index],
          description: 'Job Group ' + jobGroupCode,
          alternativeDescription: 'Job Group ' + oldJobGroupCodes[index],
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    });

    if (jobGroups.length > 0) {
      return queryInterface.bulkInsert('JobGroups', jobGroups, {});
    }
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('JobGroups', null, {});
  }
};
