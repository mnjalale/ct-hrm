'use strict';

const uuidV4 = require('uuid/v4');
const Roles = require('../utils/enums/roles');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const sequelize = queryInterface.sequelize;
    const currentRoles = await sequelize.query('SELECT * FROM Roles', {
      type: sequelize.QueryTypes.SELECT
    });

    const roleNames = [Roles.Administrator, Roles.Support, Roles.Employee, Roles.Manager];
    const roles = [];

    roleNames.forEach(roleName => {
      if (!currentRoles.find(role => role.name === roleName)) {
        roles.push({
          id: uuidV4(),
          name: roleName,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    });

    if (roles.length > 0) {
      return queryInterface.bulkInsert('Roles', roles, {});
    }
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
