'use strict';

const uuidV4 = require('uuid/v4');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const sequelize = queryInterface.sequelize;
    const currentEmploymentTypes = await sequelize.query('SELECT * FROM EmploymentTypes', {
      type: sequelize.QueryTypes.SELECT
    });

    const employmentTypeNames = ['Contract', 'Permanent'];
    const employmentTypes = [];

    employmentTypeNames.forEach(name => {
      if (!currentEmploymentTypes.find(employmentType => employmentType.name === name)) {
        employmentTypes.push({
          id: uuidV4(),
          name: name,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    });

    if (employmentTypes.length > 0) {
      return queryInterface.bulkInsert('EmploymentTypes', employmentTypes, {});
    }
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmploymentTypes', null, {});
  }
};
