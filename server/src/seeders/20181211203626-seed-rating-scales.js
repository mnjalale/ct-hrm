'use strict';

const uuidV4 = require('uuid/v4');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const sequelize = queryInterface.sequelize;
    const currentRatingScales = await sequelize.query('SELECT * FROM RatingScales', {
      type: sequelize.QueryTypes.SELECT
    });

    const ratings = [];
    const ratingScales = [
      {
        rating: 'Excellent',
        description: 'Achievement higher than 100% of the agreen performance targets.',
        fromScore: 101,
        toScore: 1000
      },
      {
        rating: 'Very Good',
        description: 'Achievement up to 100% of the agreen performance targets.',
        fromScore: 100,
        toScore: 100
      },
      {
        rating: 'Good',
        description: 'Achievement between 80% and 99% of the agreen performance targets.',
        fromScore: 80,
        toScore: 99
      },
      {
        rating: 'Fair',
        description: 'Achievement between 60% and 79% of the agreen performance targets.',
        fromScore: 60,
        toScore: 79
      },
      {
        rating: 'Poor',
        description: 'Achievement up to 59% of the agreen performance targets.',
        fromScore: 59,
        toScore: 0
      }
    ];

    ratingScales.forEach(ratingScale => {
      if (!currentRatingScales.find(currentRating => currentRating.rating === ratingScale.rating)) {
        ratings.push({
          id: uuidV4(),
          rating: ratingScale.rating,
          description: ratingScale.description,
          fromScore: ratingScale.fromScore,
          toScore: ratingScale.toScore,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    });

    if (ratings.length > 0) {
      return queryInterface.bulkInsert('RatingScales', ratings, {});
    }
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('RatingScales', null, {});
  }
};
