import Roles from '../utils/enums/roles';

class DtoFactory {

  getUserDto =  (user) => {
    if (!user) {
      return null;
    }

    const isManager = !!user.Roles ? !!user.Roles.find(role => role.name === Roles.Manager) : false;
    const isAdmin = !!user.Roles ? !!user.Roles.find(role => role.name === Roles.Administrator) : false;
    const hasActiveAppraisal = !!user.PerformanceAppraisals ? !!user.PerformanceAppraisals.find(performanceAppraisal => {
      const today = new Date();
      return performanceAppraisal.startDate <= today && today <= performanceAppraisal.endDate;
    }) : false;

    const userDto = {
      id: user.id,
      firstName: user.firstName,
      middleName: user.middleName,
      lastName: user.lastName,
      gender: !!user.Gender ? user.Gender.code : '',
      email: user.email,
      phoneNumber: user.phoneNumber,
      username: user.username,
      jobGroupId: user.jobGroupId,
      jobGroup: !!user.JobGroup ? this.getJobGroupDto(user.JobGroup) : null,
      designationId: user.designationId,
      ministryId: user.ministryId,
      departmentId: user.departmentId,
      sectionId: user.sectionId,
      dutyStationId: user.dutyStationId,
      designation: !!user.Designation ? this.getDesignationDto(user.Designation) : null,
      managerId: user.managerId,
      // manager: !!user.Manager
      employmentTypeId: user.employmentTypeId,
      employmentType: !!user.EmploymentType ? this.getEmploymentTypeDto(user.EmploymentType) : null,
      isManager: isManager,
      isAdmin: isAdmin,
      hasActiveAppraisal: hasActiveAppraisal
    };

    return userDto;
  }

  getJobGroupDto = (jobGroup) => {
    if (!jobGroup) {
      return null;
    }

    const jobGroupDto = {
      id: jobGroup.id,
      code: jobGroup.code,
      alternativeCode: jobGroup.alternativeCode,
      description: jobGroup.description
    };

    return jobGroupDto;
  }

  getDesignationDto = (designation) => {
    if (!designation) {
      return null;
    }

    const designationDto = {
      id: designation.id,
      name: designation.name
    };

    return designationDto;
  }

  getEmploymentTypeDto = (employmentType) => {
    if (!employmentType) {
      return null;
    }

    const employmentTypeDto = {
      id: employmentType.id,
      name: employmentType.name
    };

    return employmentTypeDto;
  }

  getPerformanceAppraisalDto = (appraisal) => {
    if (!appraisal) {
      return null;
    }

    const employee = !!appraisal.User ? this.getUserDto(appraisal.User) : null;
    const appraisalDto = {
      id: appraisal.id,
      startDate: appraisal.startDate,
      midYearReviewDate: appraisal.midYearReviewDate,
      endDate: appraisal.endDate,
      appraiseePeriodStartSignature: appraisal.appraiseePeriodStartSignature,
      appraiseePeriodStartSignatureDate: appraisal.appraiseePeriodStartSignatureDate,
      supervisorPeriodStartName: appraisal.supervisorPeriodStartName,
      supervisorPeriodStartSignature: appraisal.supervisorPeriodStartSignature,
      supervisorPeriodStartSignatureDate: appraisal.supervisorPeriodStartSignatureDate,
      supervisorMidYearReviewName: appraisal.supervisorMidYearReviewName,
      supervisorMidYearReviewSignature: appraisal.supervisorMidYearReviewSignature,
      supervisorMidYearReviewSignatureDate: appraisal.supervisorMidYearReviewSignatureDate,
      trainingAppraiseeSignature: appraisal.trainingAppraiseeSignature,
      trainingAppraiseeSignatureDate: appraisal.trainingAppraiseeSignatureDate,
      trainingSupervisorName: appraisal.trainingSupervisorName,
      trainingSupervisorSignature: appraisal.trainingSupervisorSignature,
      trainingSupervisorSignatureDate: appraisal.trainingSupervisorSignatureDate,
      appraiseePeriodEndComments: appraisal.appraiseePeriodEndComments,
      supervisorPeriodEndComments: appraisal.supervisorPeriodEndComments,
      supervisorPeriodEndName: appraisal.supervisorPeriodEndName,
      supervisorPeriodEndSignature: appraisal.supervisorPeriodEndSignature,
      supervisorPeriodEndSignatureDate: appraisal.supervisorPeriodEndSignatureDate,
      recommendedRewardType: appraisal.recommendedRewardType,
      recommendedOtherInterventions: appraisal.recommendedOtherInterventions,
      recommendedSanction: appraisal.recommendedSanction,
      recommendationMinuteNo: appraisal.recommendationMinuteNo,
      recommendationMeetingDate: appraisal.recommendationMeetingDate,
      recommendationChairPersonName: appraisal.recommendationChairPersonName,
      recommendationChairPersonSignature: appraisal.recommendationChairPersonSignature,
      recommendationChairPersonSignatureDate: appraisal.recommendationChairPersonSignatureDate,
      recommendationSecretaryName: appraisal.recommendationSecretaryName,
      recommendationSecretarySignature: appraisal.recommendationSecretarySignature,
      recommendationSecretarySignatureDate: appraisal.recommendationSecretarySignatureDate,
      recommendationAuthorizedOfficerApproved: appraisal.recommendationAuthorizedOfficerApproved,
      recommendationAuthorizedOfficerName: appraisal.recommendationAuthorizedOfficerName,
      recommendationAuthorizedOfficerSignature: appraisal.recommendationAuthorizedOfficerSignature,
      recommendationAuthorizedOfficerSignatureDate: appraisal.recommendationAuthorizedOfficerSignatureDate,
      employeeId: appraisal.employeeId,
      employee: employee,
      performanceAppraisalTargets: appraisal.PerformanceAppraisalTargets.map(target => this.getPerformanceAppraisalTargetDto(target)),
      performanceAppraisalTrainingNeeds: appraisal.PerformanceAppraisalTrainingNeeds.map(need => this.getPerformanceAppraisalTrainingNeedDto(need)),
      performanceAppraisalAdditionalAssignments: appraisal.PerformanceAppraisalAdditionalAssignments.map(additionalAssignment => this.getPerformanceAppraisalAdditionalAssignmentDto(additionalAssignment))
    };

    return appraisalDto;
  }

  getPerformanceAppraisalTargetDto =(performanceTarget) => {
    const performanceTargetDto = {
      id: performanceTarget.id,
      performanceTarget: performanceTarget.performanceTarget,
      performanceIndicator: performanceTarget.performanceIndicator,
      midYearReviewRemarks: performanceTarget.midYearReviewRemarks,
      updatedPerformanceTarget: performanceTarget.updatedPerformanceTarget,
      achievedResult: performanceTarget.achievedResult,
      performanceAppraisalScore: performanceTarget.performanceAppraisalScore,
      performanceAppraisalId: performanceTarget.performanceAppraisalId
    };

    return performanceTargetDto;
  }

  getPerformanceAppraisalTrainingNeedDto =(performanceAppraisalTrainingNeed) => {
    const performanceAppraisalTrainingNeedDto = {
      id: performanceAppraisalTrainingNeed.id,
      description: performanceAppraisalTrainingNeed.description,
      performanceAppraisalId: performanceAppraisalTrainingNeed.performanceAppraisalId
    };

    return performanceAppraisalTrainingNeedDto;
  }

  getPerformanceAppraisalAdditionalAssignmentDto =(performanceAppraisalAdditionalAssignment) => {
    const performanceAppraisalAdditionalAssignmentDto = {
      id: performanceAppraisalAdditionalAssignment.id,
      description: performanceAppraisalAdditionalAssignment.description,
      performanceAppraisalId: performanceAppraisalAdditionalAssignment.performanceAppraisalId
    };

    return performanceAppraisalAdditionalAssignmentDto;
  }
}

export default new DtoFactory();