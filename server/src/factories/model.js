const _formatDate = Symbol('_formatDate')

class ModelFactory {



  getPerformanceAppraisalModel= (input) => {
    if (!input) {
      return null;
    }

    const appraisal = {
      id: input.id,
      startDate: this[_formatDate](input.startDate),
      midYearReviewDate: this[_formatDate](input.midYearReviewDate),
      endDate: this[_formatDate](input.endDate),
      appraiseePeriodStartSignature: input.appraiseePeriodStartSignature,
      appraiseePeriodStartSignatureDate: this[_formatDate](input.appraiseePeriodStartSignatureDate),
      supervisorPeriodStartName: input.supervisorPeriodStartName,
      supervisorPeriodStartSignature: input.supervisorPeriodStartSignature,
      supervisorPeriodStartSignatureDate: this[_formatDate](input.supervisorPeriodStartSignatureDate),
      supervisorMidYearReviewName: input.supervisorMidYearReviewName,
      supervisorMidYearReviewSignature: input.supervisorMidYearReviewSignature,
      supervisorMidYearReviewSignatureDate: this[_formatDate](input.supervisorMidYearReviewSignatureDate),
      trainingAppraiseeSignature: input.trainingAppraiseeSignature,
      trainingAppraiseeSignatureDate: this[_formatDate](input.trainingAppraiseeSignatureDate),
      trainingSupervisorName: input.trainingSupervisorName,
      trainingSupervisorSignature: input.trainingSupervisorSignature,
      trainingSupervisorSignatureDate: this[_formatDate](input.trainingSupervisorSignatureDate),
      appraiseePeriodEndComments: input.appraiseePeriodEndComments,
      supervisorPeriodEndComments: input.supervisorPeriodEndComments,
      supervisorPeriodEndName: input.supervisorPeriodEndName,
      supervisorPeriodEndSignature: input.supervisorPeriodEndSignature,
      supervisorPeriodEndSignatureDate: this[_formatDate](input.supervisorPeriodEndSignatureDate),
      recommendedRewardType: input.recommendedRewardType,
      recommendedOtherInterventions: input.recommendedOtherInterventions,
      recommendedSanction: input.recommendedSanction,
      recommendationMinuteNo: input.recommendationMinuteNo,
      recommendationMeetingDate: this[_formatDate](input.recommendationMeetingDate),
      recommendationChairPersonName: input.recommendationChairPersonName,
      recommendationChairPersonSignature: input.recommendationChairPersonSignature,
      recommendationChairPersonSignatureDate: this[_formatDate](input.recommendationChairPersonSignatureDate),
      recommendationSecretaryName: input.recommendationSecretaryName,
      recommendationSecretarySignature: input.recommendationSecretarySignature,
      recommendationSecretarySignatureDate: this[_formatDate](input.recommendationSecretarySignatureDate),
      recommendationAuthorizedOfficerApproved: input.recommendationAuthorizedOfficerApproved,
      recommendationAuthorizedOfficerName: input.recommendationAuthorizedOfficerName,
      recommendationAuthorizedOfficerSignature: input.recommendationAuthorizedOfficerSignature,
      recommendationAuthorizedOfficerSignatureDate: this[_formatDate](input.recommendationAuthorizedOfficerSignatureDate),
      employeeId: input.employeeId
    };

    return appraisal;
  }

  [ _formatDate](value) {
    let returnValue = null;
    if (value) {
      returnValue = new Date(value);
    }
    return returnValue;
  }
}


export default new ModelFactory();