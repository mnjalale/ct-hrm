'use strict';
module.exports = (sequelize, DataTypes) => {
  const Designation = sequelize.define('Designation', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  Designation.associate = function(models) {
    // associations can be defined here
    Designation.hasMany(models.User, {
      foreignKey: 'designationId',
      onDelete: 'RESTRICT'
    })
  };
  return Designation;
};