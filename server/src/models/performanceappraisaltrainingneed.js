'use strict';
module.exports = (sequelize, DataTypes) => {
  const PerformanceAppraisalTrainingNeed = sequelize.define('PerformanceAppraisalTrainingNeed', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {});
  PerformanceAppraisalTrainingNeed.associate = function(models) {
    // associations can be defined here
    PerformanceAppraisalTrainingNeed.belongsTo(models.PerformanceAppraisal, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });
  };
  return PerformanceAppraisalTrainingNeed;
};