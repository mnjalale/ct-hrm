'use strict';
module.exports = (sequelize, DataTypes) => {
  const Section = sequelize.define('Section', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  Section.associate = function(models) {
    // associations can be defined here
    Section.hasMany(models.User, {
      foreignKey: 'sectionId',
      onDelete: 'RESTRICT'
    });

    Section.hasMany(models.DutyStation, {
      foreignKey: 'sectionId',
      onDelete: 'RESTRICT'
    });

    Section.belongsTo(models.Department, {
      foreignKey: 'departmentId',
      onDelete: 'RESTRICT'
    });
  };
  return Section;
};