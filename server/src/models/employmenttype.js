'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmploymentType = sequelize.define('EmploymentType', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {});
  EmploymentType.associate = function(models) {
    // associations can be defined here
    EmploymentType.hasMany(models.User, {
      foreignKey: 'employmentTypeId',
      onDelete: 'RESTRICT'
    })
  };
  return EmploymentType;
};