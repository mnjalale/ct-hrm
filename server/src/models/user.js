'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    personalNo: {
      type: DataTypes.STRING
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    middleName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    phoneNumber: {
      type: DataTypes.STRING,
    }
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.belongsTo(models.JobGroup, {
      foreignKey: 'jobGroupId',
      onDelete: 'RESTRICT'
    });

    User.belongsTo(models.Designation, {
      foreignKey: 'designationId',
      onDelete: 'RESTRICT'
    });

    User.belongsTo(models.Ministry, {
      foreignKey: 'ministryId',
      onDelete: 'RESTRICT'
    });

    User.hasMany(models.User, {
      foreignKey: 'managerId',
      onDelete: 'RESTRICT'
    });

    User.belongsTo(models.User, {
      foreignKey: 'managerId',
      onDelete: 'RESTRICT'
    });

    User.belongsToMany(models.Role, {
      through: models.UserRole
    });

    User.belongsTo(models.EmploymentType, {
      foreignKey: 'employmentTypeId',
      onDelete: 'RESTRICT'
    });

    User.belongsTo(models.Gender, {
      foreignKey: 'genderId',
      onDelete: 'RESTRICT'
    });

    User.hasMany(models.PerformanceAppraisal, {
      foreignKey: 'employeeId',
      onDelete: 'RESTRICT'
    });
  };
  return User;
};