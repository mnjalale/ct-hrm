'use strict';
module.exports = (sequelize, DataTypes) => {
  const PerformanceAppraisal = sequelize.define('PerformanceAppraisal', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    midYearReviewDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    appraiseePeriodStartSignature: {
      type: DataTypes.STRING,
    },
    appraiseePeriodStartSignatureDate: {
      type: DataTypes.DATE,
    },
    supervisorPeriodStartName: {
      type: DataTypes.STRING,
    },
    supervisorPeriodStartSignature: {
      type: DataTypes.STRING,
    },
    supervisorPeriodStartSignatureDate: {
      type: DataTypes.DATE,
    },
    supervisorMidYearReviewName: {
      type: DataTypes.STRING,
    },
    supervisorMidYearReviewSignature: {
      type: DataTypes.STRING,
    },
    supervisorMidYearReviewSignatureDate: {
      type: DataTypes.DATE,
    },

    // Staff training and development needs
    trainingAppraiseeSignature: {
      type: DataTypes.STRING
    },
    trainingAppraiseeSignatureDate: {
      type: DataTypes.DATE
    },
    trainingSupervisorName: {
      type: DataTypes.STRING
    },
    trainingSupervisorSignature: {
      type: DataTypes.STRING
    },
    trainingSupervisorSignatureDate: {
      type: DataTypes.DATE
    },

    // End of appraisal period comments
    appraiseePeriodEndComments: {
      type: DataTypes.STRING
    },
    supervisorPeriodEndComments: {
      type: DataTypes.STRING,
    },
    supervisorPeriodEndName: {
      type: DataTypes.STRING,
    },
    supervisorPeriodEndSignature: {
      type: DataTypes.STRING,
    },
    supervisorPeriodEndSignatureDate: {
      type: DataTypes.DATE,
    },

    // Recommendations of rewards or sanctions
    recommendedRewardType: {
      type: DataTypes.STRING
    },
    recommendedOtherInterventions: {
      type: DataTypes.STRING
    },
    recommendedSanction: {
      type: DataTypes.STRING
    },
    recommendationMinuteNo: {
      type: DataTypes.STRING
    },
    recommendationMeetingDate: {
      type: DataTypes.DATE
    },
    recommendationChairPersonName: {
      type: DataTypes.STRING
    },
    recommendationChairPersonSignature: {
      type: DataTypes.STRING
    },
    recommendationChairPersonSignatureDate: {
      type: DataTypes.DATE
    },
    recommendationSecretaryName: {
      type: DataTypes.STRING
    },
    recommendationSecretarySignature: {
      type: DataTypes.STRING
    },
    recommendationSecretarySignatureDate: {
      type: DataTypes.DATE
    },
    recommendationAuthorizedOfficerApproved: {
      type: DataTypes.BOOLEAN
    },
    recommendationAuthorizedOfficerName: {
      type: DataTypes.STRING
    },
    recommendationAuthorizedOfficerSignature: {
      type: DataTypes.STRING
    },
    recommendationAuthorizedOfficerSignatureDate: {
      type: DataTypes.DATE
    }

  }, {});
  PerformanceAppraisal.associate = function(models) {
    // associations can be defined here
    PerformanceAppraisal.hasMany(models.PerformanceAppraisalTarget, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });

    PerformanceAppraisal.hasMany(models.PerformanceAppraisalTrainingNeed, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });

    PerformanceAppraisal.hasMany(models.PerformanceAppraisalAdditionalAssignment, {
      foreignKey: 'performanceAppraisalId',
      onDelete: 'RESTRICT'
    });

    PerformanceAppraisal.belongsTo(models.User, {
      foreignKey: 'employeeId',
      onDelete: 'RESTRICT'
    });
  };
  return PerformanceAppraisal;
};