'use strict';
module.exports = (sequelize, DataTypes) => {
  const RatingScale = sequelize.define('RatingScale', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    fromScore: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    toScore: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {});
  RatingScale.associate = function(models) {
    // associations can be defined here
  };
  return RatingScale;
};