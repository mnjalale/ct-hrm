'use strict';
module.exports = (sequelize, DataTypes) => {
  const Gender = sequelize.define('Gender', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  Gender.associate = function(models) {
    // associations can be defined here
    Gender.hasMany(models.User, {
      foreignKey: 'genderId',
      onDelete: 'RESTRICT'
    });
  };
  return Gender;
};